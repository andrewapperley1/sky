//
//  AFFVector.h
//  AFFramework
//
//  Created by Andrew Apperley on 2013-04-10.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LogVector2(v)     NSLog(@"{%f , %f}",v.x, v.y);
#define LogVector3(v)     NSLog(@"{%f , %f , %f}",v.x,v.y,v.z);


struct Vector2 {
    float x,
    y;
};

struct Vector3 {
    float x,
    y,
    z;
};

typedef struct Vector2 Vector2;
typedef struct Vector3 Vector3;

@interface AFFVector : NSObject

+ (Vector2)vectorWithX:(float)lx andY:(float)ly;
+ (Vector3)vectorWithX:(float)lx andY:(float)ly andZ:(float)lz;
+ (Vector2)addVector2:(Vector2)v1 withVector2:(Vector2)v2;
+ (Vector2)minusVector2:(Vector2)v1 withVector2:(Vector2)v2;
+ (float)magnitudeOfVector2:(Vector2)v1;
+ (float)dotProductofVector2:(Vector2)v1 andVector2:(Vector2)v2;
+ (float)angleBetweenVector2:(Vector2)v1 andVector2:(Vector2)v2;

+ (Vector3)addVector3:(Vector3)v1 withVector3:(Vector3)v2;
+ (Vector3)minusVector3:(Vector3)v1 withVector3:(Vector3)v2;
+ (float)magnitudeOfVector3:(Vector3)v1;
+ (float)dotProductofVector3:(Vector3)v1 andVector3:(Vector3)v2;
+ (float)angleBetweenVector3:(Vector3)v1 andVector3:(Vector3)v2;
@end
