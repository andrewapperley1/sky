//
//  AFFXMLParser.m
//  AFFramework
//
//  Created by Andrew Apperley on 2013-02-07.
//
//

#import "AFFXMLParser.h"

@implementation AFFXMLParser

@synthesize parsedObjects;

-(id)initWithFileName:(NSString *)file
{
    self = [super init];
    if(self)
    {
         _objectStack = [NSMutableArray new];
        [_objectStack addObject:[NSMutableDictionary dictionary]];
        [self loadXMLFile:file];
       
    }
    
    return self;
}

-(void)loadXMLFile:(NSString *)p
{
    NSURL *url = [NSURL fileURLWithPath:p];
    
    NSXMLParser *addressParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    if (addressParser)
    {
        [addressParser setDelegate:self];
        [addressParser setShouldResolveExternalEntities:YES];
        [addressParser parse];
        
        [addressParser setDelegate:nil];
        [addressParser release];
        addressParser = nil;
        
        return;
    }

}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSMutableDictionary *parent = [_objectStack lastObject];
    NSMutableDictionary *child = [NSMutableDictionary dictionaryWithDictionary:attributeDict];
    
    if([parent objectForKey:elementName] != nil){
    
        if([[parent objectForKey:elementName] isKindOfClass:[NSMutableArray class]])
            //element exists but is an array
            [[parent objectForKey:elementName] addObject:child];
        else {
            //Element exists but is a dictionary
            NSMutableArray *childArray = [NSMutableArray array];
            [childArray addObject:[parent objectForKey:elementName]];
            [childArray addObject:child];
            [parent setObject:childArray forKey:elementName];
        }
    
    } else {
        [parent setObject:child forKey:elementName];
    }
    
    [_objectStack addObject:child];
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    [_objectStack removeLastObject];
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    parsedObjects = [[NSDictionary dictionaryWithDictionary:[_objectStack objectAtIndex:0]] retain];
    [_objectStack release];
    _objectStack = nil;
}

-(void)dealloc
{
    [parsedObjects release];
    parsedObjects = nil;
    
    [super dealloc];
}
@end
