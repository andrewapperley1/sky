//
//  AFFXMLParser.h
//  AFFramework
//
//  Created by Andrew Apperley on 2013-02-07.
//
//

#import <UIKit/UIKit.h>

@interface AFFXMLParser : NSObject<NSXMLParserDelegate>
{
    //testing stuff
    NSMutableArray *_objectStack;
}
@property (nonatomic, assign)NSDictionary *parsedObjects;
-(id)initWithFileName:(NSString *)file;
-(void)loadXMLFile:(NSString *)p;
@end
