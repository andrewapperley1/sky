//
//  AFFMiscMacros.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-07.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#define UserDefaults                        [NSUserDefaults standardUserDefaults]
#define SyncDefaults                        [UserDefaults synchronize]
#define NotificationCenter                  [NSNotificationCenter defaultCenter]
#define Bundle                              [NSBundle mainBundle]
#define MainScreen                          [UIScreen mainScreen]
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define StatusBarHeight                     [[UIApplication sharedApplication] statusBarFrame].size.height
#define SystemVersion                       [[UIDevice currentDevice].systemVersion floatValue]
#define DEGREES_TO_RADIANS(x)               (M_PI * x / 180.0) 
#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define HEXCOLOR(c, a)                      [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:a]
#define AFFLogObject(o,m)                     NSLog(@"\nLog Message: %@\nFunction: %s \nLine: %d \nObject:%@",m, __func__, __LINE__, o)
#define AFFLog(m)                            NSLog(@"\nLog Message: %@\nFunction: %s \nLine: %d",m, __func__, __LINE__)

#ifdef UI_USER_INTERFACE_IDIOM()
#define IS_IPAD() (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#else
#define IS_IPAD() (false)
#endif

//Center an object relative to it's container
#define CENTER_OBJECT_X($object, $container)                                                \
    [$object setAffX:($container.bounds.size.width - $object.bounds.size.width) / 2 ]

#define CENTER_OBJECT_Y($object, $container)                                                \
    [$object setAffY:($container.bounds.size.height - $object.bounds.size.height) / 2 ]

#define CENTER_OBJECT($object, $container)                                                  \
    [$object setAffX:($container.bounds.size.width - $object.bounds.size.width) / 2 ];      \
    [$object setAffY:($container.bounds.size.height - $object.bounds.size.height) / 2 ]

