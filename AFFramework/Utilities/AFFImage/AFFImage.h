//
//  AFFImage.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2012-12-02.
//
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@class CAAnimation;

@interface AFFImage : UIView

@property (nonatomic, assign) int rotation;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) CALayer *imageLayer;
@property (nonatomic, retain) NSString *name;

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image;
- (id)initWithOrigin:(CGPoint)frame andImage:(UIImage *)image;
- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)color;

@end
