//
//  AFFString.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-02-20.
//
//

#import <UIKit/UIKit.h>

@interface AFFString : NSObject

+ (NSString *)trimString:(NSString *)string toIndexString:(NSString *)rangeString;

@end
