//
//  AFFSelectorQueue.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-01-28.
//
//

#import <UIKit/UIKit.h>

@interface AFFSelectorQueue : NSObject
{
    @private
    id delegate;
    NSMutableArray *queue;
    NSMutableDictionary *selectorDictionary;
}

- (id)initWithDelegate:(id)ldelegate;

//Queue Methods
- (void)addSelectorToQueue:(SEL)selector;
- (void)addSelectorToQueue:(SEL)selector andArg:(id)arg;
- (void)addSelectorToQueue:(SEL)selector andArg:(id)arg1 andArg:(id)arg2;
- (void)removeSelectorFromQueue:(SEL)selector;
- (void)removeAllSelectorsFromQueue;

- (void)runQueue;

@end
