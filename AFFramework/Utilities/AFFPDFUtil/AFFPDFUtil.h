//
//  AFFPDFUtil.h
//  AFFramework
//
//  Created by Andrew Apperley on 2013-04-08.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIImage;

@interface AFFPDFUtil : NSObject

+ (void)createPDFFileFromImageSet:(NSArray *)limageSet imagesPerPage:(int)split andName:(NSString *)lfileName;

+ (void)createPDFFileFromImageSet:(NSArray *)limageSet imagesPerPage:(int)split andName:(NSString *)lfileName andPadding:(float)padding;

+ (void)createPDFFileFromImage:(UIImage *)limage andName:(NSString *)lfileName;

+ (void)deletePDFByFileName:(NSString *)fileName andImages:(NSArray *)images;


@end