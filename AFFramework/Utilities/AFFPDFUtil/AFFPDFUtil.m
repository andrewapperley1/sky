//
//  AFFPDFUtil.m
//  AFFramework
//
//  Created by Andrew Apperley on 2013-04-08.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFPDFUtil.h"
#import <UIKit/UIKit.h>

@implementation AFFPDFUtil

+ (void)createPDFFileFromImageSet:(NSArray *)limageSet imagesPerPage:(int)split andName:(NSString *)lfileName
{
    [AFFPDFUtil createPDFFileFromImageSet:limageSet imagesPerPage:split andName:lfileName andPadding:0];
}

+ (void)createPDFFileFromImageSet:(NSArray *)limageSet imagesPerPage:(int)split andName:(NSString *)lfileName andPadding:(float)padding
{
    NSArray *imageSet = [[NSArray alloc] initWithArray:limageSet];
    NSString *fileName = [[NSString alloc] initWithString:lfileName];
    NSMutableData *data = [NSMutableData data];
    
    BOOL creating = TRUE;
    
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    int a = 0;
    
    do {
        
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, [[imageSet objectAtIndex:a]size].width, [[imageSet objectAtIndex:a]size].height + [[imageSet objectAtIndex:MIN(a+1, [imageSet count]-1)]size].height + padding), nil);
        
        
        [((UIImage *)[imageSet objectAtIndex:a]) drawAtPoint:CGPointMake(0, 0)];
        a++;
        if(a >= [imageSet count])
            break;
        
        if(a >= [imageSet count])
            creating = FALSE;
    } while (creating);
    
    UIGraphicsEndPDFContext();
    
    NSArray* docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* _docDir = [docDir objectAtIndex:0];
    NSString* _docDirFileN = [_docDir stringByAppendingPathComponent:fileName];
    
    [data writeToFile:_docDirFileN atomically:YES];
    
    [imageSet release];
    [fileName release];
    
}

+ (void)createPDFFileFromImage:(UIImage *)limage andName:(NSString *)lfileName
{
    UIImage *image = [limage retain];
    NSString *fileName = [[NSString alloc] initWithString:lfileName];
    NSMutableData *data = [NSMutableData data];
    
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, image.size.width, image.size.height), nil);
    
    [limage drawAtPoint:CGPointMake(0, 0)];
    
    UIGraphicsEndPDFContext();
    
    NSArray* docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* _docDir = [docDir objectAtIndex:0];
    NSString* _docDirFileN = [_docDir stringByAppendingPathComponent:fileName];
    
    [data writeToFile:_docDirFileN atomically:YES];
    
    [fileName release];
    fileName = nil;
    
    [image release];
    image = nil;
    
}

+ (void)deletePDFByFileName:(NSString *)fileName andImages:(NSArray *)images
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) objectAtIndex:0];
    
    //Delete images
    if(images && images.count > 0)
    {
        for(uint i = 0; i < images.count; i++)
        {
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:(NSString *)[images objectAtIndex:i]];
            
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
    }
    
    //Delete PDF file
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
}

@end
