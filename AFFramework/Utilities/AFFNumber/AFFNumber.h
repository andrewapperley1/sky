//
//  AFFNumber.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-03-10.
//
//

#import <UIKit/UIKit.h>

@interface AFFNumber : NSObject

+ (int)numberOfDigits:(NSNumber *)number;

@end
