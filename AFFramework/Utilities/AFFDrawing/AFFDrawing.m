//
//  AFFDrawing.m
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-07.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFDrawing.h"
#import <QuartzCore/QuartzCore.h>

@implementation AFFDrawing

+ (UIImage *)resizeImage:(UIImage *)limage toSize:(CGSize)size
{
    UIImage *currentImage = limage;
    UIGraphicsBeginImageContext(size);

    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width  = size.width;
    thumbnailRect.size.height = size.height;
    
    [currentImage drawInRect:thumbnailRect];
    
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(resizedImage == nil)
        NSLog(@"\nWARNING : Could not scale image.");
    
    UIGraphicsEndImageContext();
    
    return resizedImage;
}



+ (UIImage *)imageFromView:(UIView *)view
{
    //Create image from view
    UIGraphicsBeginImageContext(view.frame.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    if(image == nil)
        NSLog(@"\nWARNING : Could not create image.");
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageFromView:(UIView *)view andRect:(CGRect)frame
{
    //Create image from view
    UIGraphicsBeginImageContext(view.frame.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    if(image == nil)
        NSLog(@"\nWARNING : Could not create image.");
    
    UIGraphicsEndImageContext();
    
    //Clip image
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, frame);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return returnImage;
}

+ (void)drawLineFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint withColor:(UIColor*)color andLineWidth:(float)lineWidth
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    
    CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    CGContextAddLineToPoint(context, endPoint.x, endPoint.y);

    CGContextStrokePath(context);
}

+ (void)drawLineFromPoint:(CGPoint)startPoint toPoints:(CGPoint[])endPoints withColor:(UIColor*)color andLineWidth:(float)lineWidth
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    
    CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    
    uint i = 1;
    
    while (i < sizeof( *endPoints) + 1)
    {
        CGContextAddLineToPoint(context, endPoints[i].x, endPoints[i].y);
        i ++;
    }
    
    CGContextStrokePath(context);
}

+ (void)drawLineFromPoints:(CGPoint[])points withColor:(UIColor*)color andLineWidth:(float)lineWidth
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor(context, color.CGColor);    
    
    CGContextMoveToPoint(context, points[0].x, points[0].y);
    
    uint i = 1;
    
    while (i < sizeof( *points) + 1)
    {
        CGContextAddLineToPoint(context, points[i].x, points[i].y);
        i ++;
    }
    
    CGContextStrokePath(context);
}

@end