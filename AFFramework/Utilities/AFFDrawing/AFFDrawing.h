//
//  AFFDrawing.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-07.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFFDrawing : NSObject

+ (UIImage *)resizeImage:(UIImage *)limage toSize:(CGSize)size;
+ (UIImage *)imageFromView:(UIView *)view;
+ (UIImage *)imageFromView:(UIView *)view andRect:(CGRect)frame;

/*
 * Use ONLY in drawRect method
 */
+ (void)drawLineFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint withColor:(UIColor*)color andLineWidth:(float)lineWidth;
//+ (void)drawLineFromPoint:(CGPoint)startPoint toPoints:(CGPoint[])endPoints withColor:(UIColor*)color andLineWidth:(float)lineWidth;
//+ (void)drawLineFromPoints:(CGPoint[])points withColor:(UIColor*)color andLineWidth:(float)lineWidth;
@end
