//
//  AFFEvent.m
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-10.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFEvent.h"

@implementation AFFEvent
@synthesize sender = _sender;
@synthesize data = _data;
@synthesize eventName = _eventName;

+ (id)eventWithSender:(id)lsender andData:(id)ldata andEventName:(NSString *)leventName
{
    return [[[self alloc] initWithSender:lsender andData:ldata andEventName:leventName] autorelease];
}

- (id)initWithSender:(id)lsender andData:(id)ldata andEventName:(NSString *)leventName
{
    self = [super init];
    if(self)
    {
        _sender = lsender;
        _data = ldata;
        _eventName = leventName;
    }
    return self;
}

@end
