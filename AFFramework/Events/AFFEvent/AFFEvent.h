//
//  AFFEvent.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-10.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFFEvent : NSObject

@property (nonatomic, readonly) id sender;
@property (nonatomic, readonly) id data;
@property (nonatomic, readonly) NSString *eventName;

+ (id)eventWithSender:(id)lsender andData:(id)ldata andEventName:(NSString *)leventName;
- (id)initWithSender:(id)lsender andData:(id)ldata andEventName:(NSString *)leventName;

@end
