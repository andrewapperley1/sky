//
//  AFFEventBaseHandler.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-10.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFEvent.h"

@class AFFEventArgs;

@interface AFFEventBaseHandler : NSObject
{
    @public
    NSObject *sender;
    NSObject *observer;
    SEL selector;
    NSMutableArray *args;
}

@property (nonatomic, assign) BOOL isOneTimeHandler;
@property (nonatomic, retain) NSString *eventNameWithHash;

- (id)initWithSender:(NSObject *)lsender andObserver:(NSObject *)lobserver andSelector:(SEL)lselector andEventName:(NSString *)leventName andArgs:(NSArray *)largs;
- (void)invokeWithEvent:(AFFEvent *)event;

@end
