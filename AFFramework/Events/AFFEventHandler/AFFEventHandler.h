//
//  AFFEventHandler.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-10.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFEventBaseHandler.h"

@interface AFFEventHandler : AFFEventBaseHandler

+ (id)handlerWithSender:(id)lsender andObserver:(id)lobserver andSelector:(SEL)lselector andEventName:(NSString *)leventName andArgs:(NSArray *)largs;

@end
