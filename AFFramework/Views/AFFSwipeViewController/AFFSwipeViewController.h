//
//  AFFSwipeViewController.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-15.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFViewController.h"

enum SwipeDirection
{
    SwipeNone          = 0,
    SwipeRight         = 1 << 0,
    SwipeLeft          = 1 << 1,
    SwipeUp            = 1 << 2,
    SwipeDown          = 1 << 3,
    SwipeHorizontal    = SwipeLeft | SwipeRight,
    SwipeVertical      = SwipeUp | SwipeDown,
    SwipeAll           = SwipeRight | SwipeLeft | SwipeUp | SwipeDown
};

typedef NSUInteger SwipeDirection;

@interface AFFSwipeViewController : AFFViewController
{
    @protected
    UIView *stage;
}

//Default number of touches = 1

- (id)initWithDirections:(SwipeDirection)directions;
- (id)initWithDirections:(SwipeDirection)directions andNumberOfTouches:(uint)numberOfTouches;

AFFEventCreate(AFFEventInstance, evtSwipedUp);
AFFEventCreate(AFFEventInstance, evtSwipedRight);
AFFEventCreate(AFFEventInstance, evtSwipedDown);
AFFEventCreate(AFFEventInstance, evtSwipedLeft);

@end
