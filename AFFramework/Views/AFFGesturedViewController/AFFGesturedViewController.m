//
//  AFFGesturedViewController.m
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-15.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFGesturedViewController.h"

const short DEFAULT_NUM_SWIPE_TOUCHES = 1;
const short DEFAULT_MIN_VELOCITY = 1.0f;

@implementation AFFGesturedViewController

AFFEventSynthesize(AFFEventInstance, evtPinchedIn);
AFFEventSynthesize(AFFEventInstance, evtPinchedOut);
AFFEventSynthesize(AFFEventInstance, evtTapped);

/*
 * Constructors
 */
- (id)initWithDirections:(SwipeDirection)directions
{
    return [self initWithDirections:directions andNumberOfTouches:DEFAULT_NUM_SWIPE_TOUCHES andMinPinchVelocity:DEFAULT_MIN_VELOCITY];
}

- (id)initWithDirections:(SwipeDirection)directions andMinPinchVelocity:(float)lminVelocity
{
    return [self initWithDirections:directions andNumberOfTouches:DEFAULT_NUM_SWIPE_TOUCHES andMinPinchVelocity:lminVelocity];
}

- (id)initWithDirections:(SwipeDirection)directions andNumberOfTouches:(uint)numberOfTouches
{
    return [self initWithDirections:directions andNumberOfTouches:numberOfTouches andMinPinchVelocity:DEFAULT_MIN_VELOCITY];
}

- (id)initWithDirections:(SwipeDirection)directions andNumberOfTouches:(uint)numberOfTouches andMinPinchVelocity:(float)lminVelocity
{
    self = [super initWithDirections:directions andNumberOfTouches:numberOfTouches];
    if(self)
    {
        minVelocity = lminVelocity;
        [self createPinchGestures];
        [self createTapGesture];
    }
    return self;
}

- (void)createPinchGestures
{
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onPinch:)];
    [stage addGestureRecognizer:pinchGesture];
    
    [pinchGesture release];
    pinchGesture = nil;
}

- (void)createTapGesture
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    [stage addGestureRecognizer:tapGesture];
    
    [tapGesture release];
    tapGesture = nil;
}

- (void)onTap
{
    [[self evtTapped] send];
}

- (void)onPinch:(UIPinchGestureRecognizer *)pinchGesture
{
    if(pinchGesture.velocity > minVelocity)
        [[self evtPinchedOut] send];
    else if(pinchGesture.velocity < - minVelocity)
        [[self evtPinchedIn] send];
}

- (void)dealloc
{
    AFFRemoveAllEvents();
    
    [super dealloc];
}

@end
