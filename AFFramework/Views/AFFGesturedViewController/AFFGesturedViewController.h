//
//  AFFGesturedViewController.h
//  AFFramework
//
//  Created by Jeremy Fuellert on 2013-04-15.
//  Copyright (c) 2013 AFFramework. All rights reserved.
//

#import "AFFSwipeViewController.h"

@interface AFFGesturedViewController : AFFSwipeViewController
{
    @private
    float minVelocity;
}

AFFEventCreate(AFFEventInstance, evtPinchedIn);
AFFEventCreate(AFFEventInstance, evtPinchedOut);
AFFEventCreate(AFFEventInstance, evtTapped);

//Default min velocity = 1.0f

- (id)initWithDirections:(SwipeDirection)directions;
- (id)initWithDirections:(SwipeDirection)directions andMinPinchVelocity:(float)lminVelocity;
- (id)initWithDirections:(SwipeDirection)directions andNumberOfTouches:(uint)numberOfTouches;
- (id)initWithDirections:(SwipeDirection)directions andNumberOfTouches:(uint)numberOfTouches andMinPinchVelocity:(float)lminVelocity;

@end
