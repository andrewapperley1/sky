//
//  main.m
//  Sky : Weather
//
//  //  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
