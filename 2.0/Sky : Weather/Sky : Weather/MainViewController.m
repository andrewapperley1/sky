//
//  MainViewController.m
//  Sky : Weather
//
//  //  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "MainViewController.h"
#import "WeatherViewController.h"
#import "FailedRequestController.h"


@interface MainViewController () {
    WeatherViewController *_weatherView;
}

@end

@implementation MainViewController

- (instancetype)init {
    if (self = [super init]) {
        [self addChildViewController:[FailedRequestController sharedController]];
        [self.view addSubview:[[FailedRequestController sharedController] view]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _weatherView = [[WeatherViewController alloc] init];
    [self addChildViewController:_weatherView];
    [self.view addSubview:_weatherView.view];
    
}

- (void)reloadWeatherView {
    [_weatherView reloadWeatherView];
}

@end