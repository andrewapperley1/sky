//
//  BaseNetworkModel.h
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseNetworkModel : NSObject

+ (instancetype)modelFromNetworkResponse:(NSDictionary *)response;

@property(nonatomic, readonly)NSString *cod;
@property(nonatomic, readonly)NSString *message;
@property(nonatomic, readonly, strong)NSDictionary *data;

@end