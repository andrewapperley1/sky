//
//  NetworkingStatics.m
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "NetworkingStatics.h"

NSString *const kNetworkingBaseURL = @"http://api.openweathermap.org/data/2.5/";
NSString *const kNetworkingAPIKey = @"8442d6229229896c8a473b9b14d44738";
NSString *const kNetworkingBaseNetworkError = @"You don't have an active internet connection";