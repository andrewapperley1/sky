//
//  BaseNetworkModel.m
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "BaseNetworkModel.h"

NSString *const kBaseNetworkModelDataKey = @"data";
NSString *const kBaseNetworkModelListKey = @"list";
NSString *const kBaseNetworkModelCityKey = @"city";
NSString *const kBaseNetworkModelStatusKey = @"cod";
NSString *const kBaseNetworkModelMessageKey = @"message";

@implementation BaseNetworkModel

+ (instancetype)modelFromNetworkResponse:(NSDictionary *)response {
    BaseNetworkModel *model = [[BaseNetworkModel alloc] init];
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:response[kBaseNetworkModelListKey] forKey:kBaseNetworkModelListKey];
    
    if ([response objectForKey:kBaseNetworkModelCityKey]) {
        [data setObject:response[kBaseNetworkModelCityKey] forKey:kBaseNetworkModelCityKey];
    }
    
    [model setValue:data forKey:kBaseNetworkModelDataKey];
    
    [model setValue:response[kBaseNetworkModelStatusKey] forKey:kBaseNetworkModelStatusKey];

    [model setValue:response[kBaseNetworkModelMessageKey] forKey:kBaseNetworkModelMessageKey];
    
    return model;
}

@end