//
//  FailedRequestViewController.m
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "FailedRequestController.h"
#import "FailedRequestView.h"
#import <KeepLayout.h>
#import "FailedRequestQueueModel.h"
#import "NetworkMediator.h"

typedef void(^ShowOutModalBlock)(void);

@interface FailedRequestController () <FailedRequestModalDelegate> {
    NSMutableArray *_currentQueue;
    FailedRequestView *_currentModal;
}

@property(nonatomic, strong)UIVisualEffectView *view;

@end

@implementation FailedRequestController

@dynamic view;

- (instancetype)init {
    if (self = [super init]) {
        _currentQueue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = NO;
    self.view.alpha = 0;
}

- (void)loadView {
    self.view = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    self.view.frame = [[UIScreen mainScreen] bounds];
}

+ (instancetype)sharedController {
    static FailedRequestController *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[self alloc] init];
    });
    return sharedController;
}

- (void)addRequestModalToQueue:(NetworkingRequestBlock)request withError:(NSError *)error {
    BOOL allowAdd = YES;
    for (FailedRequestQueueModel *model in _currentQueue) {
        if ([model.request isEqual:request]) {
            allowAdd = NO;
            break;
        }
    }
    if (allowAdd) {
        [_currentQueue addObject:[[FailedRequestQueueModel alloc] initModelWithRequest:request error:error]];
        [self showInNextModal];
    }
}

- (void)showInNextModal {
    
    if (!_currentModal) {
        FailedRequestQueueModel *model = [_currentQueue firstObject];
        
        _currentModal = [[FailedRequestView alloc] initWithTitle:model.error.localizedDescription];
        _currentModal.delegate = self;
        
        [_currentModal keepSize:CGSizeMake(self.view.bounds.size.width, 60)];
        [self.view.contentView addSubview:_currentModal];
        _currentModal.keepTopInset.equal = self.view.frame.size.height;
        
        [self.view keepAnimatedWithDuration:0.4f layout:^{
            self.view.alpha = 1.0f;
            self.view.userInteractionEnabled = YES;
        } completion:^(BOOL finished) {
            if (!self) {return;}
            [self.view keepAnimatedWithDuration:0.4f layout:^{
                _currentModal.keepTopInset.equal = self.view.frame.size.height - _currentModal.frame.size.height;
            }];
        }];
    }
    
}

- (void)showOutModalForRequest:(NetworkingRequestBlock)request {
    if (_currentModal && _currentQueue.count > 0 && [[_currentQueue.firstObject request] isEqual:request]) {
        [_currentQueue removeObjectAtIndex:0];
        [self.view keepAnimatedWithDuration:0.4f layout:^{
            _currentModal.keepTopInset.equal = self.view.frame.size.height;
        } completion:^(BOOL finished) {
            [self.view keepAnimatedWithDuration:0.4f layout:^{
                if (_currentQueue.count <= 0) {
                    self.view.alpha = 0.0f;
                    self.view.userInteractionEnabled = NO;
                }
            } completion:^(BOOL finished) {
                _currentModal.delegate = nil;
                [_currentModal removeFromSuperview];
                _currentModal = nil;
                if (_currentQueue.count > 0) {
                    [self showInNextModal];
                }
            }];
        }];
    }
}

#pragma mark - FailedRequestModalDelegate

- (void)retryRequest {
    FailedRequestQueueModel *model = [_currentQueue firstObject];
    [[NetworkMediator sharedMediator] addRequest:model.request];
}

@end