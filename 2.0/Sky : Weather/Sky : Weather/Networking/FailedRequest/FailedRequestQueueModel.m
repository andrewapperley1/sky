//
//  FailedRequestQueueModel.m
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "FailedRequestQueueModel.h"

@implementation FailedRequestQueueModel

- (instancetype)initModelWithRequest:(NetworkingRequestBlock)request error:(NSError *)error {
    if (self = [super init]) {
        _request = request;
        _error = error;
    }
    return self;
}

@end