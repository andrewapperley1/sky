//
//  FailedRequestQueueModel.h
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkingStatics.h"

@interface FailedRequestQueueModel : NSObject

@property(nonatomic, readonly)NetworkingRequestBlock request;
@property(nonatomic, readonly)NSError *error;

- (instancetype)initModelWithRequest:(NetworkingRequestBlock)request error:(NSError *)error;

@end