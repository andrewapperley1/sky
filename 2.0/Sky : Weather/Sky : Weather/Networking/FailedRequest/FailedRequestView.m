//
//  FailedRequestView.m
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "FailedRequestView.h"
#import <KeepLayout.h>

@interface FailedRequestView () {
    
}

@end

@implementation FailedRequestView

- (instancetype)initWithTitle:(NSString *)title {
    if (self = [super init]) {
        self.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 60);
        self.backgroundColor = [UIColor yellowColor];
        [self setupTitle:title];
        [self setupRetryButton];
    }
    return self;
}

- (void)setupTitle:(NSString *)title {
    UILabel *_titleLabel = [[UILabel alloc] init];
    _titleLabel.numberOfLines = 2;
    _titleLabel.text = title;
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
    [_titleLabel keepSize:CGSizeMake(self.bounds.size.width/1.3, self.bounds.size.height)];
    [self addSubview:_titleLabel];
    _titleLabel.keepLeftMarginInset.equal = 10;
    [_titleLabel keepVerticallyCentered];
}

- (void)setupRetryButton {
    UIButton *retryButton = [[UIButton alloc] init];
    [retryButton addTarget:self action:@selector(retryRequest) forControlEvents:UIControlEventTouchUpInside];
    [retryButton setImage:[UIImage imageNamed:@"retryButton"] forState:UIControlStateNormal];
    [retryButton keepSize:[UIImage imageNamed:@"retryButton"].size];
    [self addSubview:retryButton];
    
    [retryButton keepVerticallyCentered];
    retryButton.keepRightInset.equal = 10;
}

- (void)retryRequest {
    [_delegate retryRequest];
}

@end
