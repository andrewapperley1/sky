//
//  FailedRequestViewController.h
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkingStatics.h"

@interface FailedRequestController : UIViewController

+ (instancetype)sharedController;

- (void)addRequestModalToQueue:(NetworkingRequestBlock)request withError:(NSError *)error;
- (void)showOutModalForRequest:(NetworkingRequestBlock)request;

@end