//
//  FailedRequestView.h
//  Beacon Scavenger Hunt
//
//  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FailedRequestModalDelegate <NSObject>

- (void)retryRequest;

@end

@interface FailedRequestView : UIView

@property(nonatomic, weak)id<FailedRequestModalDelegate> delegate;

- (instancetype)initWithTitle:(NSString *)title;

@end