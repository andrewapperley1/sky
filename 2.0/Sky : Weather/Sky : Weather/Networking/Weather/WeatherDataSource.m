//
//  WeatherDataSource.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "WeatherDataSource.h"

NSString *const kWeatherSearchEndpoint = @"find";
NSString *const kWeatherDailyEndpoint = @"forecast/daily";
NSString *const kWeatherHourlyEndpoint = @"forecast";

@implementation WeatherDataSource

- (void)searchForCityByTerm:(NSString *)term success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{
                             @"q": term,
                             @"type": @"like",
                             @"sort": @"population",
                             @"units": @"metric", // #TODO Change this to the setting that the user chooses
                             @"cnt": @(30),
                             @"mode": @"json"
                             }];
    [self makeRequestWithEndpoint:kWeatherSearchEndpoint params:params success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success(response, request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        failure(error, request);
    } type:BaseNetworkRequestType_GET];
}

- (void)weatherForCityByID:(NSString *)cityID daily:(BOOL)daily success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{
                             @"id": cityID,
                             @"mode": @"json",
                             @"units": @"metric", // #TODO Change this to the setting that the user chooses
                             @"cnt": @(7)
                             }];
    [self makeRequestWithEndpoint:(daily) ? kWeatherDailyEndpoint : kWeatherHourlyEndpoint params:params success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success(response, request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        failure(error, request);
    } type:BaseNetworkRequestType_GET];
}

- (void)weatherForCurrentLocationAtLong:(CGFloat)loc_long lat:(CGFloat)loc_lat daily:(BOOL)daily success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{
                             @"lat": @(loc_lat),
                             @"lon": @(loc_long),
                             @"type": @"accurate",
                             @"mode": @"json",
                             @"units": @"metric", // #TODO Change this to the setting that the user chooses
                             @"cnt": @(5)
                             }];
    [self makeRequestWithEndpoint:(daily) ? kWeatherDailyEndpoint : kWeatherHourlyEndpoint params:params success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success(response, request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        failure(error, request);
    } type:BaseNetworkRequestType_GET];
}

@end