//
//  WeatherDataSource.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNetworkDataSource.h"

@interface WeatherDataSource : BaseNetworkDataSource

- (void)searchForCityByTerm:(NSString *)term success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure;
- (void)weatherForCityByID:(NSString *)cityID daily:(BOOL)daily success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure;
- (void)weatherForCurrentLocationAtLong:(CGFloat)loc_long lat:(CGFloat)loc_lat daily:(BOOL)daily success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure;

@end