//
//  WeatherModelFactory.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationModel, ForecastModel;

typedef void(^WeatherModelFactoryForecastSuccess)(ForecastModel *forecast, NSURLSessionDataTask *request);
typedef void(^WeatherModelFactoryLocationSuccess)(NSArray *locations, NSURLSessionDataTask *request);
typedef void(^WeatherModelFactoryFailure)(NSError *error, NSURLSessionDataTask *request);

@interface WeatherModelFactory : NSObject

- (void)searchForCityByTerm:(NSString *)term success:(WeatherModelFactoryLocationSuccess)success failure:(WeatherModelFactoryFailure)failure;
- (void)weatherForCityByID:(NSString *)cityID daily:(BOOL)daily success:(WeatherModelFactoryForecastSuccess)success failure:(WeatherModelFactoryFailure)failure;
- (void)weatherForCurrentLocationAtLong:(CGFloat)loc_long lat:(CGFloat)loc_lat daily:(BOOL)daily success:(WeatherModelFactoryForecastSuccess)success failure:(WeatherModelFactoryFailure)failure;

@end