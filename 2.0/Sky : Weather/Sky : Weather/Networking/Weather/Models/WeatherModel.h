//
//  WeatherModel.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-13.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempertureModel.h"

@interface WeatherModel : NSObject

- (instancetype)initWithData:(NSDictionary *)data;

@property(nonatomic, readonly, strong)TempertureModel *temperture;
@property(nonatomic)NSInteger dateStamp;
@property(nonatomic, readonly, strong)NSString *condition;

@end