//
//  TempertureModel.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "TempertureModel.h"

@implementation TempertureModel

- (instancetype)initWithData:(NSDictionary *)data {
    if (self = [super init]) {
        
        _min = ceilf((data[@"min"]) ? [data[@"min"] floatValue] : [data[@"temp_min"] floatValue]);
        _max = ceilf((data[@"max"]) ? [data[@"max"] floatValue] : [data[@"temp_max"] floatValue]);
        _morn = ceilf([data[@"morn"] floatValue]);
        _day = ceilf([data[@"day"] floatValue]);
        _eve = ceilf([data[@"eve"] floatValue]);
        _night = ceilf([data[@"night"] floatValue]);
        if (data[@"morn"]) {
            _current = ceil([data[@"temp"] floatValue]);
        } else {
            _current = ceil([data[timeOfDay()] floatValue]);
        }
        
        
        CGFloat average = 0;
        
        if (data[@"temp"]) {
            average = ceilf([data[@"temp"] floatValue]);
        } else {
            average = ceilf((_morn + _day + _eve + _night) / 4);
        }
        
        _average = average;
        
    }
    return self;
}

static inline NSString* timeOfDay() {
    static NSDateFormatter *_formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _formatter = [[NSDateFormatter alloc] init];
        [_formatter setTimeZone:[NSTimeZone localTimeZone]];
        [_formatter setDateFormat:@"H"];
    });
    NSDate *_date = [NSDate date];
    NSInteger hour = [[_formatter stringFromDate:_date] integerValue];
    
    NSString *timeOfDay = @"";
    switch (hour) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            timeOfDay = @"morn";
            /*Morning*/
            break;
        
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
            timeOfDay = @"day";
            /*Day*/
            break;
        
        case 17:
        case 18:
        case 19:
        case 20:
            timeOfDay = @"eve";
            /*Evening*/
            break;
            
        case 21:
        case 22:
        case 23:
        case 24:
            timeOfDay = @"night";
            /*Night*/
            break;
            
    }
    return timeOfDay;
}

@end
