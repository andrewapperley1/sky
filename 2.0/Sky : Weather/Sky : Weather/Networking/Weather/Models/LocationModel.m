//
//  LocationModel.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "LocationModel.h"

@implementation LocationModel

+ (instancetype)locationFromData:(NSDictionary *)data {
    return [[LocationModel alloc] initWithData:data];
}

- (instancetype)initWithData:(NSDictionary *)data {
    if (self = [super init]) {
        if ([data objectForKey:@"id"]) {
            _cityID = [NSString stringWithFormat:@"%d", [data[@"id"] integerValue]];
        }
        
        if ([data objectForKey:@"name"]) {
            _cityName = data[@"name"];
        }
        
        _countryCode = (!data[@"country"]) ? data[@"sys"][@"country"] : data[@"country"];
        _order = locationOrder(_cityID);
    }
    return self;
}

@end
