//
//  WeatherModel.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-13.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "WeatherModel.h"
#import "TempertureModel.h"

@implementation WeatherModel

- (instancetype)initWithData:(NSDictionary *)data {
    if (self = [super init]) {
        _temperture = [[TempertureModel alloc] initWithData:(data[@"temp"]) ? data[@"temp"] : data[@"main"]];
        _dateStamp = [data[@"dt"] integerValue];
        _condition  = [data[@"weather"][0][@"description"] capitalizedString];
    }
    return self;
}

@end