//
//  ForecastModel.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LocationModel;

@interface ForecastModel : NSObject

+ (instancetype)forecastModelFromData:(NSDictionary *)data;

- (instancetype)initWithData:(NSDictionary *)data;

@property(nonatomic, strong, readonly)LocationModel *location;
@property(nonatomic, strong, readonly)NSArray *weather;

@end