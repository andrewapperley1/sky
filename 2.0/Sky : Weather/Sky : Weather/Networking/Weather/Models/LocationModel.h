//
//  LocationModel.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationModel : NSObject

+ (instancetype)locationFromData:(NSDictionary *)data;

- (instancetype)initWithData:(NSDictionary *)data;

@property(nonatomic, strong, readonly)NSString *cityName;
@property(nonatomic, strong, readonly)NSString *cityID;
@property(nonatomic, strong, readonly)NSString *countryCode;
@property(nonatomic, readwrite)NSInteger order;

@end

static inline NSInteger locationOrder(NSString *_id) {
    return [(NSArray *)[[NSUserDefaults standardUserDefaults] arrayForKey:@"locations"] indexOfObject:_id];
}

static inline NSInteger locationCount() {
    return [[NSUserDefaults standardUserDefaults] arrayForKey:@"locations"].count-1;
}