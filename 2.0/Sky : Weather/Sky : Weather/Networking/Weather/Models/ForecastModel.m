//
//  ForecastModel.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "ForecastModel.h"
#import "LocationModel.h"
#import "WeatherModel.h"

@implementation ForecastModel

+ (instancetype)forecastModelFromData:(NSDictionary *)data {
    return [[ForecastModel alloc] initWithData:data];
}

- (instancetype)initWithData:(NSDictionary *)data {
    if (self = [super init]) {
        _location = [[LocationModel alloc] initWithData:data[@"city"]];
        NSMutableArray *weather = [[NSMutableArray alloc] init];
        for (NSDictionary *_data in data[@"list"]) {
            [weather addObject:[[WeatherModel alloc] initWithData:_data]];
        }
        _weather = [NSArray arrayWithArray:weather];
    }
    return self;
}

@end