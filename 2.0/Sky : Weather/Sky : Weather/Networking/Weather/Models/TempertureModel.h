//
//  TempertureModel.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-14.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TempertureModel : NSObject

- (instancetype)initWithData:(NSDictionary *)data;

@property(nonatomic, readonly)CGFloat average;
@property(nonatomic, readonly)CGFloat min;
@property(nonatomic, readonly)CGFloat max;
@property(nonatomic, readonly)CGFloat day;
@property(nonatomic, readonly)CGFloat night;
@property(nonatomic, readonly)CGFloat eve;
@property(nonatomic, readonly)CGFloat morn;
@property(nonatomic, readonly)CGFloat current;

@end
