//
//  WeatherModelFactory.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "WeatherModelFactory.h"
#import "BaseNetworkModel.h"
#import "WeatherDataSource.h"
#import "ForecastModel.h"
#import "LocationModel.h"

@implementation WeatherModelFactory

- (void)searchForCityByTerm:(NSString *)term success:(WeatherModelFactoryLocationSuccess)success failure:(WeatherModelFactoryFailure)failure {
    WeatherDataSource *datasource = [[WeatherDataSource alloc] init];
    [datasource searchForCityByTerm:term success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success([self.class locationsFromResponse:response.data], request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        
    }];
}

- (void)weatherForCityByID:(NSString *)cityID daily:(BOOL)daily success:(WeatherModelFactoryForecastSuccess)success failure:(WeatherModelFactoryFailure)failure {
    WeatherDataSource *datasource = [[WeatherDataSource alloc] init];
    [datasource weatherForCityByID:cityID daily:daily success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success([self.class forecastFromResponse:response.data], request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        failure(error, request);
    }];
}

- (void)weatherForCurrentLocationAtLong:(CGFloat)loc_long lat:(CGFloat)loc_lat daily:(BOOL)daily success:(WeatherModelFactoryForecastSuccess)success failure:(WeatherModelFactoryFailure)failure {
    WeatherDataSource *datasource = [[WeatherDataSource alloc] init];
    [datasource weatherForCurrentLocationAtLong:loc_long lat:loc_lat daily:daily success:^(BaseNetworkModel *response, NSURLSessionDataTask *request) {
        success([self.class forecastFromResponse:response.data], request);
    } failure:^(NSError *error, NSURLSessionDataTask *request) {
        failure(error, request);
    }];
}



+ (NSArray *)locationsFromResponse:(NSDictionary *)data {
    NSMutableArray *locations = [[NSMutableArray alloc] init];
    for (NSDictionary* locationData in data[@"list"]) {
        [locations addObject:[LocationModel locationFromData:locationData]];
    }
    return locations;
}

+ (ForecastModel *)forecastFromResponse:(NSDictionary *)data {
    return [ForecastModel forecastModelFromData:data];
}

@end