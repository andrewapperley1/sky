//
//  BaseNetworkDataSource.m
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "BaseNetworkDataSource.h"
#import <AFNetworking/AFNetworking.h>
#import "NetworkingStatics.h"
#import "BaseNetworkModel.h"

NSString *const kBaseNetworkAppIDKey = @"APPID";

@implementation BaseNetworkDataSource

- (NSURLSessionDataTask *)makeRequestWithEndpoint:(NSString *)endpoint params:(NSMutableDictionary *)params success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure type:(BaseNetworkRequestType)type {
    
    static AFHTTPSessionManager *sessionManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
    
        NSString *baseURLString = kNetworkingBaseURL;
        
        sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString]];
        
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        responseSerializer.acceptableContentTypes    = [[NSSet alloc] initWithObjects:@"application/json", nil];
        sessionManager.responseSerializer            = responseSerializer;
        
        AFJSONRequestSerializer *requestSerializer   = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        sessionManager.requestSerializer             = requestSerializer;
        
        [sessionManager setResponseSerializer:[AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers]];
    });
    
    params[kBaseNetworkAppIDKey] = kNetworkingAPIKey;
    
    if (type == BaseNetworkRequestType_POST) {
        return [sessionManager POST:endpoint parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            
            BaseNetworkModel *networkModel = [BaseNetworkModel modelFromNetworkResponse:responseObject];
            success(networkModel, task);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            failure(error, task);
        }];
    } else {
        return [sessionManager GET:endpoint parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            
            BaseNetworkModel *networkModel = [BaseNetworkModel modelFromNetworkResponse:responseObject];
            success(networkModel, task);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            failure(error, task);
        }];
    }
}

@end