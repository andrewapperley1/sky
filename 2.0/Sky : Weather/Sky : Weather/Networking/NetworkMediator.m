//
//  NetworkMediator.m
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "NetworkMediator.h"
#import "NetworkingStatics.h"
#import <AFNetworkReachabilityManager.h>
#import "AFBlipAlertView.h"
#import "FailedRequestController.h"

@interface NetworkMediator () {
    NSMutableSet *_currentRequests;
    NSOperationQueue *_currentRequestQueue;
}

@property(nonatomic)BOOL internet;

@end

@implementation NetworkMediator

- (instancetype)init {
    if (self = [super init]) {
        _internet = YES;
        _currentRequests = [[NSMutableSet alloc] init];
        _currentRequestQueue = [[NSOperationQueue alloc] init];
        [self setupReachabilityCallBack];
    }
    return self;
}

+ (instancetype)sharedMediator {
    static NetworkMediator *sharedMediator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMediator = [[self alloc] init];
    });
    return sharedMediator;
}

- (void)setupReachabilityCallBack {
    typeof(self) __weak wself = self;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
            case AFNetworkReachabilityStatusUnknown:
                wself.internet = NO;
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                wself.internet = YES;
                break;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (void)addRequest:(NetworkingRequestBlock)request {
    if (_internet) {
        [_currentRequests addObject:request];
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:request];
        [_currentRequestQueue addOperation:operation];
    } else {
        [self requestFailed:request withError:[NSError errorWithDomain:@"ca.afapps.sky" code:500 userInfo:@{NSLocalizedDescriptionKey: kNetworkingBaseNetworkError}]];
    }
}

- (void)removeRequest:(NetworkingRequestBlock)request {
    [_currentRequests removeObject:request];
}

- (void)retryLastRequest {
    
}

- (void)requestRetrySucceeded:(NetworkingRequestBlock)request {
    [self removeRequest:request];
    [[FailedRequestController sharedController] showOutModalForRequest:request];
}

- (void)requestFailed:(NetworkingRequestBlock)request withError:(NSError *)error {
    [self removeRequest:request];
    [[FailedRequestController sharedController] addRequestModalToQueue:request withError:error];
}

@end