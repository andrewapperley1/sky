//
//  NetworkMediator.h
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkingStatics.h"

@interface NetworkMediator : NSObject

+ (instancetype)sharedMediator;
- (void)addRequest:(NetworkingRequestBlock)request;
- (void)retryLastRequest;
- (void)requestRetrySucceeded:(NetworkingRequestBlock)request;
- (void)requestFailed:(NetworkingRequestBlock)request withError:(NSError *)error;
@end