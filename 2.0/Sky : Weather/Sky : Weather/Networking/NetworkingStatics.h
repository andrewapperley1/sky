//
//  NetworkingStatics.h
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kNetworkingBaseURL;
extern NSString *const kNetworkingAPIKey;
extern NSString *const kNetworkingBaseNetworkError;
typedef void(^NetworkingRequestBlock)(void);