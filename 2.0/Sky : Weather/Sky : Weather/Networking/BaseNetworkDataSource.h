//
//  BaseNetworkDataSource.h
//  Beacon Scavenger Hunt
//
//  //  Created by Andrew Apperley
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseNetworkModel;

typedef void(^BaseNetworkSuccess)(BaseNetworkModel *response, NSURLSessionDataTask *request);
typedef void(^BaseNetworkFailure)(NSError *error, NSURLSessionDataTask *request);
typedef NS_ENUM(NSUInteger, BaseNetworkRequestType) {
    BaseNetworkRequestType_POST,
    BaseNetworkRequestType_GET
};
@interface BaseNetworkDataSource : NSObject

- (NSURLSessionDataTask *)makeRequestWithEndpoint:(NSString *)endpoint params:(NSMutableDictionary *)params success:(BaseNetworkSuccess)success failure:(BaseNetworkFailure)failure type:(BaseNetworkRequestType)type;

@end