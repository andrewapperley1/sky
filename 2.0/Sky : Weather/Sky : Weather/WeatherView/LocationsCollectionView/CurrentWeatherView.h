//
//  CurrentWeatherView.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-21.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForecastModel.h"

@interface CurrentWeatherView : UIView

- (void)updateWithForecastModel:(ForecastModel *)model;

@end