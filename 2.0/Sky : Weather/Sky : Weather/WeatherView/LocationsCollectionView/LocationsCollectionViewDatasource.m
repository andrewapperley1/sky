//
//  LocationsCollectionViewDatasource.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "LocationsCollectionViewDatasource.h"
#import "LocationsCollectionViewCell.h"
#import "LocationsStatics.h"
#import "ForecastModel.h"
#import "LocationModel.h"

@interface LocationsCollectionViewDatasource() {
    NSMutableArray *_locations;
}

@end

@implementation LocationsCollectionViewDatasource

- (instancetype)init {
    if (self = [super init]) {
        [self updateLocations];
    }
    return self;
}

- (void)updateLocations {
    _locations = nil;
    _locations = [[NSMutableArray alloc] init];
    for (NSString *filler in [[NSUserDefaults standardUserDefaults] objectForKey:@"locations"]) {
        ForecastModel *forecast = [ForecastModel forecastModelFromData:@{@"city": @{@"id": filler}}];
        [_locations addObject:forecast];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _locations.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LocationsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:locationsCellKey forIndexPath:indexPath];
    if ([_locations[indexPath.row] weather].count > 0) {
        
        [cell updateLocationWithForecastModel:_locations[indexPath.row]];
        if (![_delegate initiallyLoaded]) {
            [_delegate locationUpdated:[_locations[indexPath.row] weather][0]];
        }
    }
    
    return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    if ([_locations[index] weather].count > 0) {
         [_delegate locationUpdated:[_locations[index] weather][0]];
    }

}

- (void)addUpdateData:(ForecastModel *)data {
    if (_locations.count < locationCount()) {
        [_locations insertObject:data atIndex:data.location.order];
    } else {
        if (locationOrder(data.location.cityID) <= locationCount()) {
            [_locations replaceObjectAtIndex:data.location.order withObject:data];
        }
    }
    
}

@end