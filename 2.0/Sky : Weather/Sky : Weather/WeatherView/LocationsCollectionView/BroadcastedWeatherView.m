//
//  BroadcastedWeatherView.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-21.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "BroadcastedWeatherView.h"
#import <KeepLayout/KeepLayout.h>
#import "BroadcastWeatherCollectionViewCell.h"
#import "LocationsStatics.h"
#import "UIImage+ImageWithColor.h"

@interface BroadcastedWeatherView () <UICollectionViewDataSource> {
    UICollectionView *_weeklyWeatherCollectionView;
    NSArray *_weeklyWeather;
}

@end

@implementation BroadcastedWeatherView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupBackground];
    }
    return self;
}

- (void)setupBackground {
    UIVisualEffectView *background = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    background.frame = self.bounds;
    
    UIButton *tab = [[UIButton alloc] init];
    [tab addTarget:self action:@selector(tabTapped) forControlEvents:UIControlEventTouchUpInside];
    tab.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.7f];
    tab.alpha = 0.7f;
    tab.layer.cornerRadius = 4;
    [background addSubview:tab];
    
    [self addSubview:background];
    [tab keepSize:CGSizeMake(40, 10)];
    [tab keepHorizontallyCentered];
    tab.keepTopInset.equal = 5;
    [self setupCollectionView:background tab:tab];
    
}

- (void)tabTapped {
    [_delegate tabTapped];
}

- (void)setupCollectionView:(UIView *)background tab:(UIView *)tab {
    NSInteger paddingSize = 10;
    NSInteger margin = 20;
    UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    collectionViewLayout.minimumLineSpacing = paddingSize;
    collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width/2 - paddingSize*1.5, self.bounds.size.width/2 - paddingSize);
    _weeklyWeatherCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(paddingSize, CGRectGetMaxY(tab.frame) + margin, self.bounds.size.width - paddingSize*2, self.bounds.size.height - CGRectGetMaxY(tab.frame) - margin) collectionViewLayout:collectionViewLayout];
    _weeklyWeatherCollectionView.contentInset = UIEdgeInsetsMake(0,0,margin,0);
    _weeklyWeatherCollectionView.showsVerticalScrollIndicator = NO;
    _weeklyWeatherCollectionView.backgroundColor = [UIColor clearColor];
    _weeklyWeatherCollectionView.dataSource = self;
    [_weeklyWeatherCollectionView registerClass:[BroadcastWeatherCollectionViewCell class] forCellWithReuseIdentifier:broadcastCellKey];
    [background addSubview:_weeklyWeatherCollectionView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _weeklyWeather.count;
}

- (BroadcastWeatherCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BroadcastWeatherCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:broadcastCellKey forIndexPath:indexPath];
    [cell updateData:_weeklyWeather[indexPath.row]];
    return cell;
}

- (void)updateWithForecastModel:(ForecastModel *)model {
    
    [_weeklyWeatherCollectionView setContentOffset:CGPointZero];
    _weeklyWeather = [model.weather subarrayWithRange:NSMakeRange(1, model.weather.count-1)];
    [_weeklyWeatherCollectionView reloadData];
}

@end