//
//  BroadcastWeatherCollectionViewCell.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-22.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "BroadcastWeatherCollectionViewCell.h"
#import <KeepLayout/KeepLayout.h>
#import "Colours.h"

@interface BroadcastWeatherCollectionViewCell () {
    UILabel *_temp;
    UILabel *_codition;
    UILabel *_date;
    CAGradientLayer *_gradientLayer;
}

@end

@implementation BroadcastWeatherCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor clearColor];
        [self setupBackgroundLayer];
        [self setupLabels];
    }
    return self;
}

- (void)setupBackgroundLayer {
    _gradientLayer = [CAGradientLayer layer];
//    _gradientLayer.opacity = 0.7f;
    _gradientLayer.frame = self.bounds;
    [self.layer insertSublayer:_gradientLayer atIndex:0];
}

- (void)setupLabels {
    _temp = [[UILabel alloc] init];
    _temp.textColor = [UIColor whiteColor];
    _temp.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:30];
    
    _codition = [[UILabel alloc] init];
    _codition.textColor = [UIColor whiteColor];
    _codition.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    _date = [[UILabel alloc] init];
    _date.textColor = [UIColor whiteColor];
    _date.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    
    [self.contentView addSubview:_temp];
    [self.contentView addSubview:_codition];
    [self.contentView addSubview:_date];
    
    _temp.keepTopInset.equal = 5;
    _temp.keepLeftInset.equal = 5;
    
    _codition.keepTopOffsetTo(_temp).equal = 3;
    _codition.keepLeftInset.equal = 5;
    
    _date.keepBottomInset.equal = 5;
    _date.keepLeftInset.equal = 5;
}

- (void)updateData:(WeatherModel *)model {
    
    [_temp setText:[NSString stringWithFormat:@"%.0f°", model.temperture.average]];
    [_temp sizeToFit];
    
    [_codition setText:model.condition];
    [_codition sizeToFit];
    
    [_date setText:[NSString stringWithFormat:@"%@", timeStamptoString(model.dateStamp)]];
    [_date sizeToFit];

    drawBackgroundGradient(model.temperture.min, model.temperture.max, model.temperture.average, _gradientLayer);
}

static inline NSString* timeStamptoString(NSInteger timestamp) {
    
    static NSDateFormatter *_formatter = nil;
    static NSDate *_date = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _formatter = [[NSDateFormatter alloc] init];
        [_formatter setTimeZone:[NSTimeZone localTimeZone]];
        [_formatter setDateFormat:@"EEEE"];
    });
    
    _date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    return [_formatter stringFromDate:_date];
}

@end
