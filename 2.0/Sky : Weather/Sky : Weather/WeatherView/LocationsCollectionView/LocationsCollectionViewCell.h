//
//  LocationsCollectionViewCell.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForecastModel;

@interface LocationsCollectionViewCell : UICollectionViewCell

- (void)updateLocationWithForecastModel:(ForecastModel *)forecast;

@end