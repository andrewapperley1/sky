//
//  BroadcastedWeatherView.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-21.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForecastModel.h"

@protocol BroadcastedWeatherViewDelegate <NSObject>

- (void)tabTapped;

@end

@interface BroadcastedWeatherView : UIView

- (void)updateWithForecastModel:(ForecastModel *)model;

@property(nonatomic, weak)id<BroadcastedWeatherViewDelegate> delegate;

@end