//
//  BroadcastWeatherCollectionViewCell.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-22.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherModel.h"

@interface BroadcastWeatherCollectionViewCell : UICollectionViewCell

- (void)updateData:(WeatherModel *)model;

@end