//
//  LocationsStatics.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-29.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const locationsCellKey;
extern NSString *const broadcastCellKey;