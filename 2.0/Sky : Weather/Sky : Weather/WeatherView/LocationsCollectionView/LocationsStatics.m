//
//  LocationsStatics.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-29.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "LocationsStatics.h"

NSString *const locationsCellKey = @"locationsCellKey";
NSString *const broadcastCellKey = @"broadcastCellKey";