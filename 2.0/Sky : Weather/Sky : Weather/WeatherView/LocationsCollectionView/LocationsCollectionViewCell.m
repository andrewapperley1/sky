//
//  LocationsCollectionViewCell.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "LocationsCollectionViewCell.h"
#import "ForecastModel.h"
#import "CurrentWeatherView.h"
#import "BroadcastedWeatherView.h"

@interface LocationsCollectionViewCell() <UIScrollViewDelegate, BroadcastedWeatherViewDelegate> {
    CurrentWeatherView *_currentWeatherView;
    BroadcastedWeatherView *_broadcastedWeatherView;
    UIScrollView *_scrollView;
    UIVisualEffectView *_backdrop;
}

@end

@implementation LocationsCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupBackdrop];
        [self setupScrollView];
        [self setupCurrentWeatherView];
        [self setupBroadcastedWeatherView];
    }
    return self;
}

- (void)setupBackdrop {
    _backdrop = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    _backdrop.frame = self.bounds;
    _backdrop.alpha = 0.0f;
    [self.contentView addSubview:_backdrop];
}

- (void)setupScrollView {
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.bounces = _scrollView.bouncesZoom = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.directionalLockEnabled = YES;
    _scrollView.delegate = self;
    [self.contentView addSubview:_scrollView];
}

- (void)setupCurrentWeatherView {
    _currentWeatherView = [[CurrentWeatherView alloc] initWithFrame:self.bounds];
    [self.contentView insertSubview:_currentWeatherView belowSubview:_backdrop];
}

- (void)setupBroadcastedWeatherView {
    _broadcastedWeatherView = [[BroadcastedWeatherView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height, self.bounds.size.width, self.bounds.size.height*0.75)];
    _broadcastedWeatherView.delegate = self;
    [_scrollView addSubview:_broadcastedWeatherView];
    _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _broadcastedWeatherView.bounds.size.height + _currentWeatherView.bounds.size.height );
}

- (void)updateLocationWithForecastModel:(ForecastModel *)forecast {
    if (forecast.weather.count > 0) {
        [_currentWeatherView updateWithForecastModel:forecast];
        [_broadcastedWeatherView updateWithForecastModel:forecast];
        [self resetScrollViewAnimated:NO];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y <= 0) {
        [self resetScrollViewAnimated:YES];
    } else {
        _backdrop.alpha = (scrollView.contentOffset.y / self.bounds.size.height) + 0.25f;
    }
}

- (void)resetScrollViewAnimated:(BOOL)animated {
    [_scrollView setContentOffset:CGPointZero animated:animated];
    [UIView animateWithDuration:(animated) ? 0.4f : 0.0f animations:^{
       _backdrop.alpha = 0.0f;
    }];
}

- (void)dealloc {
    _broadcastedWeatherView.delegate = nil;
}

#pragma mark - BroadcastedWeatherViewDelegate

- (void)tabTapped {
    [self resetScrollViewAnimated:YES];
}

@end