//
//  LocationsCollectionViewDatasource.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForecastModel, WeatherModel;

@protocol LocationsCollectionViewDelegate <NSObject>

- (void)locationUpdated:(WeatherModel *)weather;
- (BOOL)initiallyLoaded;

@end

@interface LocationsCollectionViewDatasource : NSObject <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property(nonatomic, weak)id<LocationsCollectionViewDelegate>delegate;

- (void)addUpdateData:(ForecastModel *)data;

@end