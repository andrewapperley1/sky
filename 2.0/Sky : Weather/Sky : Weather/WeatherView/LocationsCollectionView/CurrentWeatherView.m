//
//  CurrentWeatherView.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-21.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "CurrentWeatherView.h"
#import "LocationModel.h"
#import "WeatherModel.h"
#import "TempertureModel.h"
#import <KeepLayout/KeepLayout.h>

@interface CurrentWeatherView () {
    UILabel *_locationTitle;
    UILabel *_dayTitle;
    UILabel *_tempTitle;
    UILabel *_conditionTitle;
}

@end

@implementation CurrentWeatherView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupTitles];
    }
    return self;
}

- (void)setupTitles {
    _tempTitle = [[UILabel alloc] init];
    _tempTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:70];
    [_tempTitle setTextColor:[UIColor whiteColor]];
    [self addSubview:_tempTitle];
    
    _conditionTitle = [[UILabel alloc] init];
    _conditionTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:25];
    [_conditionTitle setTextColor:[UIColor whiteColor]];
    [self addSubview:_conditionTitle];
    
    _locationTitle = [[UILabel alloc] init];
    _locationTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:40];
    [_locationTitle setTextColor:[UIColor whiteColor]];
    [self addSubview:_locationTitle];
    
    _dayTitle = [[UILabel alloc] init];
    _dayTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:25];
    [_dayTitle setTextColor:[UIColor whiteColor]];
    [self addSubview:_dayTitle];
    
    _tempTitle.keepLeftInset.equal = 10;
    _tempTitle.keepTopInset.equal = 10;
    _conditionTitle.keepTopOffsetTo(_tempTitle).equal = 0;
    _conditionTitle.keepLeftAlignTo(_tempTitle).equal = 0;
    
    _dayTitle.keepBottomAlignTo(_locationTitle).equal = 40;
    _dayTitle.keepLeftAlignTo(_locationTitle).equal = 0;
}

- (void)updateWithForecastModel:(ForecastModel *)model {
    [_tempTitle setText:[NSString stringWithFormat:@"%.0f°", ((WeatherModel *)model.weather.firstObject).temperture.average]];
    [_conditionTitle setText:((WeatherModel *)model.weather.firstObject).condition];
    [_locationTitle setText:model.location.cityName];
    
    [_dayTitle setText:timeStamptoString(((WeatherModel *)model.weather.firstObject).dateStamp)];
    [_tempTitle sizeToFit];
    [_conditionTitle sizeToFit];
    [_locationTitle sizeToFit];
    [_dayTitle sizeToFit];
    
    _locationTitle.keepTopInset.equal = self.bounds.size.height - _locationTitle.bounds.size.height - 10;
    _locationTitle.keepLeftInset.equal = 10;
}

static inline NSString* timeStamptoString(NSInteger timestamp) {
    
    static NSDateFormatter *_formatter = nil;
    static NSDate *_date = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _formatter = [[NSDateFormatter alloc] init];
        [_formatter setTimeZone:[NSTimeZone localTimeZone]];
        [_formatter setDateFormat:@"EEEE, MMMM dd"];
    });
    
    _date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    return [_formatter stringFromDate:_date];
}

@end
