//
//  WeatherView.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForecastModel;

@protocol WeatherViewDelegate <NSObject>


@end

@interface WeatherView : UIView

@property(nonatomic)BOOL loaded;

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<WeatherViewDelegate>)delegate;
- (void)updateWithData:(ForecastModel *)data;

@end