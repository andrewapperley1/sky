//
//  WeatherViewController.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherView.h"

@interface WeatherViewController : UIViewController

@property(nonatomic, strong)WeatherView *view;

- (void)reloadWeatherView;

@end