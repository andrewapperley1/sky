//
//  WeatherViewController.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "WeatherViewController.h"
#import "WeatherModelFactory.h"
#import "NetworkMediator.h"

@interface WeatherViewController ()<WeatherViewDelegate>

@end

@implementation WeatherViewController

@dynamic view;

- (instancetype)init {
    if (self = [super init]) {
        //    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"locations"]) {
        NSArray *locations = @[
                               @"6167865",
                               @"6089426",
                               @"5894171",
                               @"3652462"
                               ];
        [[NSUserDefaults standardUserDefaults] setObject:locations forKey:@"locations"];
        [[NSUserDefaults standardUserDefaults] setObject:@"C" forKey:@"tempFormat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //    }
    }
    return self;
}

- (void)loadView {
    self.view = [[WeatherView alloc] initWithFrame:[[UIScreen mainScreen] bounds] delegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadWeatherView];
}

- (void)reloadWeatherView {
    self.view.loaded = NO;
    WeatherModelFactory *factory = [[WeatherModelFactory alloc] init];
    typeof(self) __weak wself = self;
    
    for (NSString *loc in [[NSUserDefaults standardUserDefaults] arrayForKey:@"locations"]) {
        __block NetworkingRequestBlock block = ^{
            [factory weatherForCityByID:loc daily:YES success:^(ForecastModel *forecast, NSURLSessionDataTask *request) {
                [[NetworkMediator sharedMediator] requestRetrySucceeded:block];
                [wself.view updateWithData:forecast];
            } failure:^(NSError *error, NSURLSessionDataTask *request) {
                [[NetworkMediator sharedMediator] requestFailed:block withError:error];
            }];
        };
        [[NetworkMediator sharedMediator] addRequest:block];
    }
}

@end