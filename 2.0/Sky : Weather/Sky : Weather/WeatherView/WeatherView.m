//
//  WeatherView.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-04-27.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "WeatherView.h"
#import "LocationsCollectionViewDatasource.h"
#import "LocationsStatics.h"
#import "LocationsCollectionViewCell.h"
#import "ForecastModel.h"
#import "LocationModel.h"
#import "WeatherModel.h"
#import "TempertureModel.h"
#import "Colours.h"

@interface WeatherView() <UICollectionViewDelegate, LocationsCollectionViewDelegate> {
    LocationsCollectionViewDatasource *_locationsCollectionViewDataSource;
    UICollectionView * _locationsCollectionView;
    CAGradientLayer *_gradientLayer;
}

@end

@implementation WeatherView

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<WeatherViewDelegate>)delegate {
    if (self = [super initWithFrame:frame]) {
        _loaded = NO;
        [self setupBackgroundLayer];
        [self createLocationsCollectionView];
    }
    return self;
}

- (void)createLocationsCollectionView {
    _locationsCollectionViewDataSource = [[LocationsCollectionViewDatasource alloc] init];
    _locationsCollectionViewDataSource.delegate = self;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = layout.minimumInteritemSpacing = 0;
    layout.itemSize = self.bounds.size;
    _locationsCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    _locationsCollectionView.backgroundColor = [UIColor clearColor];
    [_locationsCollectionView setBounces:NO];
    _locationsCollectionView.pagingEnabled = YES;
    [_locationsCollectionView registerClass:[LocationsCollectionViewCell class] forCellWithReuseIdentifier:locationsCellKey];
    _locationsCollectionView.dataSource = _locationsCollectionViewDataSource;
    _locationsCollectionView.delegate = _locationsCollectionViewDataSource;
    _locationsCollectionView.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:_locationsCollectionView];
}

- (void)setupBackgroundLayer {
    _gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.bounds;
    [self.layer insertSublayer:_gradientLayer atIndex:0];
}

- (void)updateWithData:(ForecastModel *)data {
    [_locationsCollectionViewDataSource addUpdateData:data];
    [_locationsCollectionView reloadData];
}

- (BOOL)initiallyLoaded {
    return _loaded;
}

// Drawing Gradient

- (void)locationUpdated:(WeatherModel *)weather {
    drawBackgroundGradient(weather.temperture.min, weather.temperture.max, weather.temperture.average, _gradientLayer);
}

@end