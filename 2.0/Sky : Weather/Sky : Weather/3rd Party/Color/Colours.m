//
//  Colours.m
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-29.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import "Colours.h"
#import <Foundation/Foundation.h>

inline UIColor* colourForTemperture(NSInteger temp) {
    
    UIColor *colour = nil;
    SEL method;
    if (temp <= -20) {
        method = NSSelectorFromString(darkBlue());
    } else if (temp <= -10) {
        method = NSSelectorFromString(blue());
    } else if (temp <= 0) {
        method = NSSelectorFromString(lightBlue());
    } else if (temp <= 5 && temp >= 1) {
        method = NSSelectorFromString(lightGreen());
    } else if (temp <= 8 && temp >= 6) {
        method = NSSelectorFromString(green());
    } else if (temp <= 10 && temp >= 8) {
        method = NSSelectorFromString(darkGreen());
    } else if (temp <= 13 && temp >= 11) {
        method = NSSelectorFromString(lightOrange());
    } else if (temp <= 17 && temp >= 14) {
        method = NSSelectorFromString(orange());
    } else if (temp <= 20 && temp >= 18) {
        method = NSSelectorFromString(darkOrange());
    } else if (temp <= 23 && temp >= 21) {
        method = NSSelectorFromString(lightRed());
    } else if (temp <= 25 && temp >= 24) {
        method = NSSelectorFromString(red());
    } else if (temp >= 26) {
        method = NSSelectorFromString(darkRed());
    }
    
    colour = [UIColor performSelector:method];
    
    return colour;
}

inline void drawBackgroundGradient(NSInteger min, NSInteger max, NSInteger average, CAGradientLayer *layer) {
    
    UIColor *maxColour = colourForTemperture(max);
    UIColor *minColour = colourForTemperture(min);
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2];
    layer.colors = @[(id)maxColour.CGColor, (id)minColour.CGColor];
    [CATransaction commit];
}