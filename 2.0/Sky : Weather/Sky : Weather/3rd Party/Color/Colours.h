//
//  Colours.h
//  Sky : Weather
//
//  Created by Andrew Apperley on 2015-05-29.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#ifndef Sky___Weather_Colours_h
#define Sky___Weather_Colours_h

#import <UIKit/UIKit.h>

// Cold colours

static inline NSString* darkBlue() {
    return @"indigoColor";
}

static inline NSString* lightBlue() {
    return @"pastelBlueColor";
}

static inline NSString* blue() {
    return @"blueberryColor";
}

static inline NSString* green() {
    return @"moneyGreenColor";
}

static inline NSString* lightGreen() {
    return @"seafoamColor";
}

static inline NSString* darkGreen() {
    return @"emeraldColor";
}

// Hot colours

static inline NSString* lightOrange() {
    return @"pastelOrangeColor";
}

static inline NSString* darkOrange() {
    return @"carrotColor";
}

static inline NSString* orange() {
    return @"cantaloupeColor";
}

static inline NSString* darkRed() {
    return @"crimsonColor";
}

static inline NSString* lightRed() {
    return @"salmonColor";
}

static inline NSString* red() {
    return @"grapefruitColor";
}

extern inline UIColor* colourForTemperture(NSInteger temp);
extern inline void drawBackgroundGradient(NSInteger min, NSInteger max, NSInteger average, CAGradientLayer *layer);

#endif
