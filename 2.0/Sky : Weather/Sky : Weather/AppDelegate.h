//
//  AppDelegate.h
//  Sky : Weather
//
//  //  Created by Andrew Apperley on 2015-04-12.
//  Copyright (c) 2015 AF-Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

