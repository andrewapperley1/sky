//
//  CalendarService.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-21.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "CalendarService.h"

@implementation CalendarService

+ (BOOL)compareWeatherDate:(NSDate *)date1 andDate:(NSDate *)date2
{
    NSDate *_date1 = [self recreateNewDate:date1 withFlags:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit];
    NSDate *_date2 = [self recreateNewDate:date2 withFlags:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit];
    NSComparisonResult result = [_date1 compare:_date2];
    
    switch (result) {
        case NSOrderedAscending:
            return TRUE;
            break;
        case NSOrderedDescending:
            return FALSE;
            break;
        case NSOrderedSame:
            return TRUE;
            break;
    }
    
    return FALSE;
}

+ (NSInteger)extractDateComponent:(NSUInteger)lflag FromDate:(NSDate *)ldate andReturnValue:(int)lcomp
{
    NSCalendar *dateCal = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    switch (lcomp) {
        case 0:
            return [dateCal components:lflag fromDate:ldate].hour;
            break;
        case 1:
            return [dateCal components:lflag fromDate:ldate].day;
            break;
        case 2:
            return [dateCal components:lflag fromDate:ldate].month;
            break;
        case 3:
            return [dateCal components:lflag fromDate:ldate].year;
            break;
        case 4:
            return [dateCal components:lflag fromDate:ldate].minute;
            break;
    }
    
    return 0;
}

+ (NSDate *)recreateNewDate:(NSDate *)date withFlags:(NSUInteger)lflags
{
    //setup to extract the YEAR/DAY/MONTH from each date
    NSUInteger dateFlags = lflags;
    NSCalendar *dateCal = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents *dateCom = [dateCal components:dateFlags fromDate:date];
    
    //recreate the date objects
    NSDate *newDate = [dateCal dateFromComponents:dateCom];
    
    return newDate;
    
}

+ (NSDate *)getLocaleDateFromDate:(NSDate *)ldate
{
    NSDate *objectDate = ldate;
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:objectDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:objectDate];
    NSTimeInterval interval = 0;
    if(![destinationTimeZone isDaylightSavingTimeForDate:objectDate]){
        interval = destinationGMTOffset - sourceGMTOffset;
    } else {
        interval = (destinationGMTOffset + [destinationTimeZone daylightSavingTimeOffset]) - sourceGMTOffset;
    }
    
    return [[[NSDate alloc] initWithTimeInterval:interval sinceDate:objectDate] autorelease];
}


@end
