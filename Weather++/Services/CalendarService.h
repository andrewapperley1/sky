//
//  CalendarService.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-21.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarService : NSObject


+ (BOOL)compareWeatherDate:(NSDate *)date1 andDate:(NSDate *)date2;
+ (NSInteger)extractDateComponent:(NSUInteger)lflag FromDate:(NSDate *)ldate andReturnValue:(int)lcomp;
+ (NSDate *)recreateNewDate:(NSDate *)date withFlags:(NSUInteger)lflags;
+ (NSDate *)getLocaleDateFromDate:(NSDate *)ldate;
@end
