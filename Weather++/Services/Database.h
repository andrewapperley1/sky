//
//  Database.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2012-11-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

@class WeatherDatabase;

#define weatherDatabase [[Database database] _weatherDatabase]

@interface Database : NSObject

@property(nonatomic, retain)WeatherDatabase *_weatherDatabase;


+ (Database *)database;

@end