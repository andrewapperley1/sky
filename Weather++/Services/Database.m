//
//  Database.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2012-11-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Database.h"
#import "WeatherDatabase.h"
@implementation Database
@synthesize _weatherDatabase;


static Database *_database;
+ (Database *)database
{
    return _database;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        _database = self;
        [self createDatabases];
    }
    return self;
}

- (void)createDatabases
{
    _weatherDatabase = [WeatherDatabase new];
}

- (void)dealloc
{
    destroy(_weatherDatabase);
    [super dealloc];
}

@end
