//
//  SimpleWeatherObject.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "SimpleWeatherObject.h"
#import "AFFCleanup.h"
@implementation SimpleWeatherObject

@synthesize time = _time, temp = _temp, image_url = _image_url, desc = _desc;

- (id)initWithTime:(NSString *)ltime andTemp:(NSString *)ltemp andImage:(NSString *)limage andDesc:(NSString *)ldesc
{
    self = [super init];
    if(self)
    {
        _time = [ltime retain];
        _temp = [ltemp retain];
        _image_url = [limage retain];
        //special case for grammar
        _desc = [ldesc isEqualToString:@"sky is clear"] ? @"clear skies" : [ldesc retain];
    }
    
    return self;
}


- (void)dealloc
{
    destroy(_time);
    destroy(_temp);
    destroy(_image_url);
    destroy(_desc);
    
    [super dealloc];
}
@end
