//
//  SimpleWeatherService.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-03.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "SimpleWeatherService.h"


@implementation SimpleWeatherService

#define BASE_URL @"http://api.openweathermap.org/data/2.5/forecast?"

- (id)init
{
    self = [super init];
    if(self)
    {
        [self queryWeather];
    }
    return self;
}

- (void)queryWeather
{
    double lLat =  [UserDefaults doubleForKey:@"locationLat"];
    double lLong = [UserDefaults doubleForKey:@"locationLong"];
    
    NSURL *queryURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@lat=%f&lon=%f&APPID=%@&units=%@&lang=%@",BASE_URL,lLat,lLong,API_KEY,UNIT,[LANGUAGES objectAtIndex:CURRENT_LANGUAGE]]];
    
    NSURLRequest *queryRequest = [NSURLRequest requestWithURL:queryURL];
    
    queryConnection = [[NSURLConnection alloc] initWithRequest:queryRequest delegate:self];
    
    if(queryConnection)
    {
        if(responseData)
            destroy(responseData);
        
        responseData = [NSMutableData new];
        
        [[Shell shell].errorLabel setText:LOADING];
        
    } else {
        [[Shell shell].errorLabel setText:[NSString stringWithFormat:@"%@\n%@",CONNECTION_ERROR,DISMISS]];
        [[self evtQueryFailed] send];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    destroy(json);
    json = [[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil] retain];
    
    if(!json){
        if(responseData)
            destroy(responseData);
        destroy(json);
        destroy(connection);
        [self queryWeather];
        return;
    }
    
    destroy(connection);
    
    [[self evtQueryCompleted] send:json];
}

- (void)dealloc
{
    destroy(json);
    if(responseData)
        destroy(responseData);
    
    [super dealloc];
}

@end