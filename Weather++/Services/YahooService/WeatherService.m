//
//  WeatherService.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "WeatherService.h"
#import "WeatherObject.h"
#import "AFFCleanup.h"
#import "AFFEventAPI.h"
#import "Constants.h"
#import "WeatherDatabase.h"
#import "Database.h"
#import "SimpleWeatherService.h"
#import "Reachability.h"
#import "CalendarService.h"
#import "AppDelegate.h"

#define BASE_URL @"http://api.openweathermap.org/data/2.5/forecast/daily?"

#define RETINA_5 ScreenHeight == 568 ? @"-568h" : @""

#define WEATHER_PATH(o,key) [[[o objectForKey:@"weather"] objectAtIndex:0] objectForKey:key]
#define TEMP_PATH(o,key) [[o objectForKey:@"temp"] objectForKey:key]
#define TEMP_EXTRA_PATH(o,key) [[o objectForKey:@"main"] objectForKey:key]
#define PRECIPITATION_PATH(o) ![o objectForKey:@"rain"] ? (NSNumber *)[o objectForKey:@"snow"] : (NSNumber *)[o objectForKey:@"rain"]

@implementation WeatherService

const float _updateTime = 600.0f;
static NSTimer *updateTimer;

@synthesize notReady;

AFFEventSynthesize(AFFEventInstance, evtWeatherUpdated);
AFFEventSynthesize(AFFEventInstance, evtQueryStarted);
AFFEventSynthesize(AFFEventInstance, evtQueryFailed);
AFFEventSynthesize(AFFEventInstance, evtQueryCompleted);

- (void)initalStart
{
    //Attach event handlers for when the application needs to override the update timer and when
    //the service has started the querying process so it can send an event to shell which then
    //pops up an activity spinner
    [[[Shell shell] evtLocationChanged] addHandler:AFFHandler(@selector(overrideReady))];
    [[self evtQueryStarted] addHandler:AFFHandler(@selector(queryingApi:))];
    updateTimer = [[NSTimer timerWithTimeInterval:_updateTime target:self selector:@selector(readyToUpdateAgain) userInfo:nil repeats:FALSE] retain];
    [[AppDelegate evtCheckTimer] addHandler:AFFHandler(@selector(checkTimer))];
    //starts querying
    [self queryWeather];
}

- (void)checkTimer
{
    
    NSTimeInterval difference = [[NSDate date] timeIntervalSinceDate:[UserDefaults objectForKey:CURRENT_DATE]];
    
    NSTimeInterval _difference = [[NSDate date] timeIntervalSinceDate:updateTimer.fireDate];
    
    [updateTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:-difference - _difference]];
    
    if([updateTimer.fireDate compare:[NSDate date]] == NSOrderedAscending || [updateTimer.fireDate compare:[NSDate date]] == NSOrderedSame)
    {
         [self readyToUpdateAgain];
        destroy(updateTimer);
        updateTimer = [[NSTimer timerWithTimeInterval:_updateTime target:self selector:@selector(readyToUpdateAgain) userInfo:nil repeats:FALSE] retain];
    }
    
}

- (void)queryingApi:(AFFEvent *)event
{
    //tells shell to show loading spinner
    [Shell.shell queryingApi];
}

- (void)queryWeather
{
    
    if(notReady){
        Shell.shell._error = TRUE;
        if(([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable))
            Shell.shell.offline = FALSE;
        [[self evtQueryStarted] send];
        [[Shell shell].errorLabel setText:DATA_UP_TO_DATE];
        DISMISS_MESSAGE();
        return;
    }
    //grabs long and lat from userDefaults
    double lLat =  [UserDefaults doubleForKey:@"locationLat"];
    double lLong = [UserDefaults doubleForKey:@"locationLong"];
    
    //constructs api URL
    NSURL *queryURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@lat=%f&lon=%f&cnt=7&mode=json&APPID=%@&units=%@&lang=%@",BASE_URL,lLat,lLong,API_KEY,UNIT,[LANGUAGES objectAtIndex:CURRENT_LANGUAGE]]];

    NSURLRequest *queryRequest = [NSURLRequest requestWithURL:queryURL];
    
    if(queryConnection)
        destroy(queryConnection);
    
    queryConnection = [[NSURLConnection alloc] initWithRequest:queryRequest delegate:self];
    
    //if query connection is successful then it creates a new data object that will hold the incomming json data
    //else it pops an error and doesnt continue.
    
    if(([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable) && !notReady && queryConnection)
    {
        if(responseData)
            destroy(responseData);
        
        responseData = [NSMutableData new];
        
        notReady = TRUE;
        
        [[Shell shell].errorLabel setText:LOADING];
        [[self evtQueryStarted] send];
        
    } else {
        [[self evtQueryStarted] send];
        if(notReady) {
            [[Shell shell].errorLabel setText:DATA_UP_TO_DATE];
            DISMISS_MESSAGE();
        } else
            [[Shell shell].errorLabel setText:[NSString stringWithFormat:@"%@\n%@",CONNECTION_ERROR,DISMISS]];
        
        destroy(queryConnection);
        [[self evtQueryFailed] send];
    }
}


- (void)readyToUpdateAgain
{
    notReady = FALSE;
}

- (void)overrideReady
{
    [self readyToUpdateAgain];
    [self queryWeather];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //reset data to nothing as its a re-query
    if(responseData)
        destroy(responseData);
    
    responseData = [NSMutableData new];

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //make data object as the data comes in
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //take care of any popups that need to show up when there is an error
    if(responseData)
        destroy(responseData);
    
    destroy(queryConnection);
    Shell.shell._error = TRUE;
    if(!notReady)
        [[Shell shell].errorLabel setText:[NSString stringWithFormat:@"%@\n%@",TIMEOUT_ERROR,DISMISS]];
    [[self evtQueryFailed] send:error];
    if(!Shell.shell._firstBoot)
        [Shell.shell finishBoot];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    Shell.shell._error = FALSE;
    json = nil;
    json = [[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil] retain];
    
    simpleService = [SimpleWeatherService new];
        
    [[simpleService evtQueryCompleted] addHandlerOneTime:AFFHandler(@selector(createWeatherObjectWithExtraInfo:))];
    
    destroy(queryConnection);
}

- (void)createWeatherObjectWithExtraInfo:(AFFEvent *)event
{
    NSJSONSerialization *extraJson = nil;
    extraJson = event.data;
    
    NSMutableArray *weather = [[NSMutableArray array] retain];
    
    NSDate *weatherDate = [NSDate date];
    
    NSDateFormatter *objectFormatter = [[NSDateFormatter alloc] init];
    
    
    BOOL _currentDate = TRUE;
    int count = 0;
    int times;
    NSString *cityName = [[(NSDictionary *)json objectForKey:@"city"] objectForKey:CITY_NAME];
    NSString *objectDate = @"";
    NSString *objectTime = @"";
    NSString *timeOfDay = @"";
    
    for (NSDictionary *object in [(NSDictionary *)json objectForKey:@"list"]) {
        if(([CalendarService compareWeatherDate:weatherDate andDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]]] || _currentDate) && count < 7)
        {
            
            times = 0;
            weatherDate = [CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]];
            
            [objectFormatter setDateFormat:@"MMMM d"];
            if(count == 0) {
                objectDate = [TODAY capitalizedString];
                
                NSInteger currentTime = [CalendarService extractDateComponent:NSHourCalendarUnit FromDate:[CalendarService recreateNewDate:[NSDate date] withFlags:NSHourCalendarUnit] andReturnValue:0];
                
                
                if(currentTime >= 0 && currentTime < 12)
                    timeOfDay = @"morn";
                if(currentTime >= 12 && currentTime < 16)
                    timeOfDay = @"day";
                if(currentTime >= 16 && currentTime < 19)
                    timeOfDay = @"eve";
                if(currentTime >= 19 && currentTime <= 24)
                    timeOfDay = @"night";
                
            } else if (count == 1) {
                objectDate = [TOMORROW capitalizedString];
                timeOfDay = @"day";
            } else {
                objectDate = [objectFormatter stringFromDate:weatherDate];
                timeOfDay = @"day";
            }
            
            [weather addObject:[weatherDatabase createWeatherObjectWithData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                             //City Name
                                                                             cityName, CITY_NAME,
                                                                             //Weather Object
                                                                             [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLong:((NSNumber *)WEATHER_PATH(object,@"id")).longValue],CONDITION_CODE, WEATHER_PATH(object,@"main"),MAIN_CONDITION, WEATHER_PATH(object,@"description"), DESCRIPTION,[NSNumber numberWithFloat:![UserDefaults boolForKey:WEATHER_MODE] ? ((NSNumber *)PRECIPITATION_PATH(object)).floatValue : ((NSNumber *)PRECIPITATION_PATH(object)).floatValue * 0.04],@"precipitation",[object objectForKey:@"humidity"],@"humidity", nil], @"weather",
                                                                             //Temperature Object
                                                                             [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:ceil([TEMP_PATH(object, timeOfDay) doubleValue])],TEMPERATURE,
                                                                              [NSNumber numberWithInt:ceil([TEMP_PATH(object, @"min") doubleValue])],TEMP_MIN,
                                                                              [NSNumber numberWithInt:ceil([TEMP_PATH(object, @"max") doubleValue])],TEMP_MAX,nil], @"temp",
                                                                             //Image Object
                                                                             [NSString stringWithFormat:@"%@_large%@.png", WEATHER_PATH(object,@"icon"),RETINA_5],@"image",
                                                                             //Date
                                                                             objectDate,@"date",
                                                                             nil]]];
            ++count;
            for (NSDictionary *object in [(NSDictionary *)extraJson objectForKey:@"list"]) {
                
                
                if(([CalendarService compareWeatherDate:weatherDate andDate:[CalendarService getLocaleDateFromDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]]]]) && count < 7)
                {
                    
                    if(([CalendarService extractDateComponent:NSHourCalendarUnit FromDate:[CalendarService recreateNewDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]] withFlags:NSHourCalendarUnit] andReturnValue:0] != 0) || (times > 1 && ([CalendarService extractDateComponent:NSHourCalendarUnit FromDate:[CalendarService recreateNewDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]] withFlags:NSHourCalendarUnit] andReturnValue:0] == 0)))
                    {
                        
                    ++times;
      
                    if(( [CalendarService extractDateComponent:NSDayCalendarUnit FromDate:[CalendarService recreateNewDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]] withFlags:NSDayCalendarUnit] andReturnValue:1] == [CalendarService extractDateComponent:NSDayCalendarUnit FromDate:[CalendarService recreateNewDate:weatherDate withFlags:NSDayCalendarUnit] andReturnValue:1] + 1 ))
                        break;

                        
                    [objectFormatter setDateFormat:@"h:mm a"];
                    objectTime = [objectFormatter stringFromDate:[CalendarService getLocaleDateFromDate:[NSDate dateWithTimeIntervalSince1970:[[object objectForKey:@"dt"] longValue]]]];

                    [((WeatherObject *)[weather objectAtIndex:MAX(0,(count-1))]).times addObject:[weatherDatabase createSimpleWeatherObjectWithData:[NSDictionary dictionaryWithObjectsAndKeys:objectTime,@"time",[NSNumber numberWithInt:ceil([TEMP_EXTRA_PATH(object, TEMPERATURE) doubleValue])],TEMPERATURE, [NSString stringWithFormat:@"%@_small.png", WEATHER_PATH(object,@"icon")],@"image", WEATHER_PATH(object , @"description"),@"desc", nil]]];
                    }
                
                }
            }

        }
    }
    
    
    [weatherDatabase updateWeatherDatabase:weather];
    
    destroy(simpleService);
    destroy(json);
    destroy(objectFormatter);
    destroy(responseData);
    destroy(weather);
    
    [[self evtQueryStarted] send];
    [[self evtWeatherUpdated] send];
}

- (void)dealloc
{
    AFFRemoveAllEvents();
    
    [super dealloc];
}

@end