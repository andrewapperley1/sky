//
//  WeatherObject.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherObject : NSObject

#define CITY_NAME @"name"

#define CONDITION_CODE @"conditionCode"
#define MAIN_CONDITION @"mainCondition"
#define DESCRIPTION @"description"

#define TEMP_MIN @"temp_min"
#define TEMP_MAX @"temp_max"
#define TEMPERATURE @"temp"


@property(nonatomic, retain)NSString *name;
@property(nonatomic, retain)NSDictionary *weather;
@property(nonatomic, retain)NSDictionary *temp;
@property(nonatomic, retain)NSString *image;
@property(nonatomic, retain)NSString *date;
@property(nonatomic, retain)NSMutableArray *times;

- (id)initWithName:(NSString *)lname andWeather:(NSDictionary *)lweather andTemp:(NSDictionary *)ltemp andImage:(NSString *)limage andDate:(NSString *)ldate;
@end