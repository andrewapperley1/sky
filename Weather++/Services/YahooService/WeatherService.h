//
//  WeatherService.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#define API_KEY  @"8442d6229229896c8a473b9b14d44738"
@class SimpleWeatherService;
@interface WeatherService : NSObject<NSURLConnectionDataDelegate>
{
    NSMutableData *responseData;
    NSURLConnection *queryConnection;
    SimpleWeatherService *simpleService;
    NSJSONSerialization *json;
}

@property(nonatomic,assign)BOOL notReady;

- (void)queryWeather;
- (void)initalStart;

AFFEventCreate(AFFEventInstance, evtWeatherUpdated);
AFFEventCreate(AFFEventInstance, evtQueryStarted);
AFFEventCreate(AFFEventInstance, evtQueryCompleted);
AFFEventCreate(AFFEventInstance, evtQueryFailed);

@end