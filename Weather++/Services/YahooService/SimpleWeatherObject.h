//
//  SimpleWeatherObject.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleWeatherObject : NSObject


@property (nonatomic,retain) NSString *time;
@property (nonatomic,retain) NSString *temp;
@property (nonatomic,retain) NSString *desc;
@property (nonatomic,retain) NSString *image_url;

- (id)initWithTime:(NSString *)ltime andTemp:(NSString *)ltemp andImage:(NSString *)limage andDesc:(NSString *)ldesc;
@end
