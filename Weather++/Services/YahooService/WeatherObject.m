//
//  WeatherObject.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "WeatherObject.h"
#import "AFFCleanup.h"
@implementation WeatherObject 
@synthesize name = _name,temp = _temp,image = _image,weather = _weather, date = _date, times = _times;

- (id)initWithName:(NSString *)lname andWeather:(NSDictionary *)lweather andTemp:(NSDictionary *)ltemp andImage:(NSString *)limage andDate:(NSString *)ldate
{
    self = [super init];
    if(self)
    {
        _name = [lname retain];
        _weather = [lweather retain];
        _temp = [ltemp retain];
        _image = [limage retain];
        _date = [ldate retain];
        _times = [NSMutableArray new];
    }
    
    return self;
}


- (void)dealloc
{
    destroy(_name);
    destroy(_weather);
    destroy(_temp);
    destroy(_image);
    destroy(_date);
    destroy(_times);
    
    [super dealloc];
}
@end