//
//  WeatherDatabase.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-15.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "BaseDatabase.h"
@class WeatherObject, SimpleWeatherObject;

@interface WeatherDatabase : BaseDatabase

- (NSArray *)getWeather;
- (void)updateWeatherDatabase:(NSArray *)weatherData;
- (WeatherObject *)createWeatherObjectWithData:(NSDictionary *)data;
- (SimpleWeatherObject *)createSimpleWeatherObjectWithData:(NSDictionary *)data;
@end
