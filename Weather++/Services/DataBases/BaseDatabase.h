//
//  BaseDatabase.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-15.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseConstants.h"
#import <sqlite3.h>

@interface BaseDatabase : NSObject

@property(nonatomic, readonly)NSString *databaseName;
@property(nonatomic, readonly)NSString *path;

- (id)initWithDatabase:(NSString*)ldatabase;
- (NSString *)databaseName;

//Database exceptions
+ (void)failedToOpen;
+ (void)failedToRunQuery:(const char *)query;

@end