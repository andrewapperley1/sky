//
//  WeatherDatabase.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-15.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "WeatherDatabase.h"
#import "WeatherObject.h"
#import "SimpleWeatherObject.h"

@implementation WeatherDatabase

- (id)init
{
    return [self initWithDatabase:DATABASE_WEATHER_DATABASE];
}

- (void)updateWeatherDatabase:(NSArray *)weatherData
{
    for (int rowId = 0; rowId < weatherData.count; rowId++) {
        
        //Query
        NSString *query = [NSString stringWithFormat:@"UPDATE WeatherData SET name = '%@', temp = '%@', temp_min = '%@', temp_max = '%@', image_url = '%@', condition_code = %d, main_condition = '%@', description  = '%@', date = '%@', precipitation = '%f', humidity = '%d' WHERE id = %d",((WeatherObject *)[weatherData objectAtIndex:rowId]).name, [((WeatherObject *)[weatherData objectAtIndex:rowId]).temp objectForKey:TEMPERATURE],[((WeatherObject *)[weatherData objectAtIndex:rowId]).temp objectForKey:TEMP_MIN],[((WeatherObject *)[weatherData objectAtIndex:rowId]).temp objectForKey:TEMP_MAX],((WeatherObject *)[weatherData objectAtIndex:rowId]).image,(int)[[((WeatherObject *)[weatherData objectAtIndex:rowId]).weather objectForKey:CONDITION_CODE] intValue],[((WeatherObject *)[weatherData objectAtIndex:rowId]).weather objectForKey:MAIN_CONDITION],[((WeatherObject *)[weatherData objectAtIndex:rowId]).weather objectForKey:DESCRIPTION],((WeatherObject *)[weatherData objectAtIndex:rowId]).date,[[((WeatherObject *)[weatherData objectAtIndex:rowId]).weather objectForKey:@"precipitation"] floatValue],[[((WeatherObject *)[weatherData objectAtIndex:rowId]).weather objectForKey:@"humidity"] intValue], (rowId + 1)];
        sqlite3_stmt *statement;
        sqlite3 *database;
        
        if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
            {
                if(sqlite3_step(statement) == SQLITE_RANGE)
                {
                    [BaseDatabase failedToRunQuery:[query UTF8String]];
                    [self updateDBStructure:weatherData andNewCallBack:@selector(updateWeatherDatabase:)];
                    return;
                }
                
                sqlite3_finalize(statement);
            } else {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
                [self updateDBStructure:weatherData andNewCallBack:@selector(updateWeatherDatabase:)];
                return;
            }
            sqlite3_close(database);
        } else {
            [BaseDatabase failedToOpen];
            [self updateDBStructure:weatherData andNewCallBack:@selector(updateWeatherDatabase:)];
            return;
        }
        
        [self updateDaysDatabase:rowId andData:((WeatherObject *)[weatherData objectAtIndex:rowId]).times];
        
    }

}

- (void)updateDBStructure:(id)object andNewCallBack:(SEL)lselec
{
    NSString *query = @"CREATE TABLE 'main'.'WeatherData2' ('id' INTEGER PRIMARY KEY  NOT NULL ,'name' TEXT,'temp' TEXT,'temp_min' TEXT,'temp_max' TEXT,'image_url' TEXT,'condition_code' INTEGER,'main_condition' TEXT,'description' TEXT,'date' TEXT DEFAULT (null) ,'precipitation' FLOAT DEFAULT (null) , 'humidity' INTEGER)";
    sqlite3_stmt *statement;
    sqlite3 *database;
    
    if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_RANGE)
            {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            sqlite3_finalize(statement);
        } else {
            [BaseDatabase failedToRunQuery:[query UTF8String]];
        }
        sqlite3_close(database);
    } else {
        [BaseDatabase failedToOpen];
    }
    
    query = @"INSERT INTO 'main'.'WeatherData2' SELECT * FROM 'main'.'WeatherData'";
    
    if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_RANGE)
            {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            sqlite3_finalize(statement);
        } else {
            [BaseDatabase failedToRunQuery:[query UTF8String]];
        }
        sqlite3_close(database);
    } else {
        [BaseDatabase failedToOpen];
    }

    
    query = @"DROP TABLE 'main'.'WeatherData'";
    
    if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_RANGE)
            {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            sqlite3_finalize(statement);
        } else {
            [BaseDatabase failedToRunQuery:[query UTF8String]];
        }
        sqlite3_close(database);
    } else {
        [BaseDatabase failedToOpen];
    }
    
    query = @"ALTER TABLE 'main'.'WeatherData2' RENAME TO 'WeatherData'";
    
    if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_RANGE)
            {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            sqlite3_finalize(statement);
        } else {
            [BaseDatabase failedToRunQuery:[query UTF8String]];
        }
        sqlite3_close(database);
    } else {
        [BaseDatabase failedToOpen];
    }
    
    if(lselec == @selector(updateWeatherDatabase))
       [self updateWeatherDatabase:object];
    
    
}

- (void)updateDaysDatabase:(int)day andData:(NSArray *)data
{
    NSString *query = [NSString stringWithFormat:@"DELETE FROM '_%d' ", (day+1)];
    sqlite3_stmt *statement;
    sqlite3 *database;
    
    if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_RANGE)
            {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            
            sqlite3_finalize(statement);
        } else {
            [BaseDatabase failedToRunQuery:[query UTF8String]];
        }
        sqlite3_close(database);
    } else {
        [BaseDatabase failedToOpen];
    }
    
    for (int rowId = 0; rowId < data.count; rowId++) {
        
        //Query
        NSString *query = [NSString stringWithFormat:@"INSERT INTO _%d(id, time, temp, image_url, desc) VALUES(%d, '%@', '%@', '%@', '%@')", (day + 1), (rowId + 1), ((SimpleWeatherObject *)[data objectAtIndex:rowId]).time, ((SimpleWeatherObject *)[data objectAtIndex:rowId]).temp, ((SimpleWeatherObject *)[data objectAtIndex:rowId]).image_url, ((SimpleWeatherObject *)[data objectAtIndex:rowId]).desc ];
        sqlite3_stmt *statement;
        sqlite3 *database;
        
        if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
            {
                if(sqlite3_step(statement) == SQLITE_RANGE)
                {
                    [BaseDatabase failedToRunQuery:[query UTF8String]];
                }
                
                sqlite3_finalize(statement);
            } else {
                [BaseDatabase failedToRunQuery:[query UTF8String]];
            }
            sqlite3_close(database);
        } else {
            [BaseDatabase failedToOpen];
        }
        
    }

}

- (NSArray *)getWeather
{
    NSMutableArray *weatherData = [NSMutableArray array];
    
    for (int rowId = 1; rowId < 8; rowId++) {
        
        //Query
        const char *query = [[NSString stringWithFormat:@"SELECT name, temp, temp_min, temp_max, image_url, condition_code, main_condition, description, date, precipitation, humidity FROM WeatherData WHERE id = %d",rowId] UTF8String];
        
        sqlite3_stmt *statement;
        sqlite3 *database;
        
        if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(statement) == SQLITE_ROW)
                {
                    
                    WeatherObject *weatherObject = [self createWeatherObjectWithData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                     //City Name
                                                                                     [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 0)], CITY_NAME,
                                                                                     //Weather Object
                                                                                     [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:(int)sqlite3_column_int(statement, 5)],CONDITION_CODE, [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 6)],MAIN_CONDITION, [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 7)], DESCRIPTION,[NSNumber numberWithFloat:(float)sqlite3_column_double(statement, 9)], @"precipitation",[NSNumber numberWithInt:(int)sqlite3_column_int(statement, 10)], @"humidity", nil], @"weather",
                                                                                     //Temperature Object
                                                                                     [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)],TEMPERATURE,
                                                                                      [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)],TEMP_MIN,
                                                                                      [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 3)],TEMP_MAX,nil], @"temp",
                                                                                     //Image Object
                                                                                     [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 4)],@"image",
                                                                                      //Date
                                                                                      [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 8)], @"date",
                                                                                     nil]];
                    
                    
                    [weatherData addObject:weatherObject];
                    [((WeatherObject *)[weatherData lastObject]).times addObjectsFromArray:[self getDaysLaterWeather:rowId]];
                } else {
                    [BaseDatabase failedToRunQuery:query];
                    [self updateDBStructure:weatherData andNewCallBack:@selector(getWeather)];
                    break;
                }
                sqlite3_finalize(statement);
            } else {
                [BaseDatabase failedToRunQuery:query];
                [self updateDBStructure:weatherData andNewCallBack:@selector(getWeather)];
                break;
            }
            sqlite3_close(database);
        } else {
            [BaseDatabase failedToOpen];
            [self updateDBStructure:weatherData andNewCallBack:@selector(getWeather)];
            break;
        }

    }
    
    return weatherData;
}

- (NSArray *)getDaysLaterWeather:(int)day
{
    NSMutableArray *weatherData = [NSMutableArray array];
    
    for (int rowId = 1; rowId < 10; rowId++) {
        
        //Query
        const char *query = [[NSString stringWithFormat:@"SELECT time, temp, image_url, desc FROM _%d WHERE id = %d",day, rowId] UTF8String];
        
        sqlite3_stmt *statement;
        sqlite3 *database;
        
        if(sqlite3_open([self.path UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
            {
                if(sqlite3_step(statement) == SQLITE_ROW)
                {
                    
                    SimpleWeatherObject *weatherObject = [self createSimpleWeatherObjectWithData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                    //Time
                                                                                    [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 0)], @"time",
                                                                                    //Temp
                                                                                    [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)], @"temp",
                                                                                    //Image
                                                                                    [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)], @"image",
                                                                                                  
                                                                                    //Image
                                                                                    [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 3)], @"desc",
                                                                                                  
                                                                                      nil]];                    
                    
                    [weatherData addObject:weatherObject];
                }
                sqlite3_finalize(statement);
            } else {
                [BaseDatabase failedToRunQuery:query];
            }
            sqlite3_close(database);
        } else {
            [BaseDatabase failedToOpen];
        }
        
        
        
    }
    
    return weatherData;

}

- (WeatherObject *)createWeatherObjectWithData:(NSDictionary *)data
{
    WeatherObject *object = [[[WeatherObject alloc] initWithName:[data objectForKey:@"name"] andWeather:[data objectForKey:@"weather"] andTemp:[data objectForKey:@"temp"] andImage:[data objectForKey:@"image"] andDate:[data objectForKey:@"date"]] autorelease];
    
    return object;
}

- (SimpleWeatherObject *)createSimpleWeatherObjectWithData:(NSDictionary *)data
{
    SimpleWeatherObject *object = [[[SimpleWeatherObject alloc] initWithTime:[data objectForKey:@"time"] andTemp:[data objectForKey:@"temp"] andImage:[data objectForKey:@"image"] andDesc:[data objectForKey:@"desc"]] autorelease];
    
    return object;
}

@end