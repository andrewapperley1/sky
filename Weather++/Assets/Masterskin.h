//
//  Masterskin.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-15.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

//Weather min max image
#define WEATHER_MIN_MAX_IMAGE           @"weatherMinMax.png"

#define SETTINGS_CELCIUS_ON             @"settingsButton_on.png"
#define SETTINGS_CELCIUS_OFF            @"settingsButton_off.png"

#define SETTINGS_BLANK_ON               @"blank_Button_on.png"
#define SETTINGS_BLANK_OFF              @"blank_Button.png"