//
//  Constants.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-15.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Shell.h"

//Dismiss Message Macro
#define DISMISS_MESSAGE()         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)); \
                                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){ \
                                        [[Shell.shell evtDismissMessage] send]; \
                                  });

//Logo Image

#define LOGO @"Icon-Small"

/*
 * View animation positions
 */

#define ORIENTATION [[UIDevice currentDevice] orientation]
#define isLandscape ((ORIENTATION) == UIDeviceOrientationLandscapeLeft || (ORIENTATION) == UIDeviceOrientationLandscapeRight)

//Portrait
#define VIEW_PORTRAIT_POSITION_TOP          CGPointMake( 0, - ScreenHeight )
#define VIEW_PORTRAIT_POSITION_BOTTOM       CGPointMake( 0, ScreenHeight )
#define VIEW_PORTRAIT_POSITION_LEFT         CGPointMake( - ScreenWidth, 0 )
#define VIEW_PORTRAIT_POSITION_RIGHT        CGPointMake( ScreenWidth, 0 )

//Landscape
#define VIEW_LANDSCAPE_POSITION_TOP         CGPointMake( 0, - ScreenWidth )
#define VIEW_LANDSCAPE_POSITION_BOTTOM      CGPointMake( 0, ScreenWidth )
#define VIEW_LANDSCAPE_POSITION_LEFT        CGPointMake( - ScreenHeight, 0 )
#define VIEW_LANDSCAPE_POSITION_RIGHT       CGPointMake( ScreenHeight, 0 )

#define VIEW_POSITION_CENTER                CGPointMake( 0, 0 )

//Last view animation offset
#define VIEW_INDICATOR_LAST_SIZE    10

/*
 * View animation times
 */
#define TUTORIAL_ANIMATION_OUT_TIME 0.5f
#define VIEW_ANIMATION_TIME         0.35f

/*
 * Font
 */
#define FONT_REGULAR            @"Ubuntu"

/*
 * Base app color
 */
#define APP_COLOR_MAIN RGB(102, 199, 196) 
#define APP_COLOR_SECONDARY RGB(50, 50, 50)
#define APP_COLOR_SAVED [UserDefaults floatForKey:THEME_RED] == 0.00f && [UserDefaults floatForKey:THEME_GREEN] == 0.00f && [UserDefaults floatForKey:THEME_BLUE] == 0.00f ? APP_COLOR_MAIN : [UIColor colorWithRed:[UserDefaults floatForKey:THEME_RED] green:[UserDefaults floatForKey:THEME_GREEN] blue:[UserDefaults floatForKey:THEME_BLUE] alpha:1]

/*
 * Weather constants
 */
#define WEATHER_MODE            @"weatherMode"
#define UNIT [UserDefaults boolForKey:WEATHER_MODE] ? @"imperial" : @"metric"


/*
 * Theme settings
 */
#define THEME_RED                          @"red"
#define THEME_GREEN                        @"green"
#define THEME_BLUE                         @"blue"
#define THEME_INDEX                        @"themeIndex"

/*
 * Temperature
 */
#define TEMP_MODE_TYPE [UserDefaults boolForKey:WEATHER_MODE] ? @"F" : @"C"
#define DEGREE_SYMBOL @"\u00B0"
#define TEMP_MODE [NSString stringWithFormat:@"%@%@",DEGREE_SYMBOL ,TEMP_MODE_TYPE]

/*
 * Hourly time seperation
 */
#define HOURLY_TIME_SEPERATION 3
#define CURRENT_DATE    @"currentDate"
/*
 * String constants
 */
//
//Supported languages
//
//English - en,                         0
//Russian - ru,                         1
//Italian - it,                         2
//Spanish - sp,                         3
//Ukrainian - ua,                       4
//German - de,                          5
//Portuguese - pt,                      6
//Romanian - ro,                        7
//Polish - pl,                          8
//Finnish - fi,                         9
//Dutch - nl,                           10
//French - fr,                          11
//Bulgarian - bg,                       12
//Swedish - se,                         13
//Chinese Traditional - zh_tw,          14
//Chinese Simplified - zh_cn            15

#define KEY_DEFAULT_LANGUAGE    @"defaultAppLanguage"
#define SET_CURRENT_LANGUAGE_INDEX(x) [UserDefaults setInteger:x forKey:KEY_DEFAULT_LANGUAGE]
#define CURRENT_LANGUAGE [UserDefaults integerForKey:KEY_DEFAULT_LANGUAGE]

//Languages
#define LANGUAGES               [NSArray arrayWithObjects:@"en", @"ru", @"it", @"sp", @"ua", @"de", @"pt", @"ro", @"pl", @"fi", @"nl", @"fr", @"bg", @"se", @"zh_tw", @"zh_cn", nil]
#define LANGUAGE_BUTTONS        [NSArray arrayWithObjects:@"sky", @"погода", @"cielo", @"clima", @"небо", @"himmel", @"clima", @"cer", @"klimat", @"taivas", @"lucht", @"ciel", @"небе", @"himmel", @"天空", @"天空", nil]

//Dates
#define __TODAY                 [NSArray arrayWithObjects:@"Today", @"сегодня", @"oggi", @"hoy", @"сьогодні", @"heute", @"hoje", @"azi", @"dzisiaj", @"tänään", @"vandaag", @"aujourd hui", @"днес", @"I dag", @"今天", @"今天", nil]
#define TODAY                   [__TODAY objectAtIndex:CURRENT_LANGUAGE]

#define __TOMORROW              [NSArray arrayWithObjects:@"Tomorrow", @"завтра", @"domani", @"mañana", @"завтра", @"morgen", @"amanhã", @"mâine", @"jutro", @"huomenna", @"morgen", @"demain",  @"утре", @"I morgon", @"明天", @"明天", nil]
#define TOMORROW                [__TOMORROW objectAtIndex:CURRENT_LANGUAGE]

//Dismiss
#define __DISMISS              [NSArray arrayWithObjects:@"Tap to dismiss", @"Нажмите, чтобы закрыть", @"Toccare per chiudere", @"Toque para cerrar", @"Натисніть, щоб закрити", @"Tippen entlassen", @"Toque para demitir", @"Atingeți pentru a înlătura", @"Stuknij, aby zamknąć", @"Kosketa erottaa", @"Kosketa erottaa", @"Appuyez sur pour rejeter", @"Докоснете, за да отхвърли", @"Tryck här för att avfärda", @"點擊解僱", @"点击解雇", nil]
#define DISMISS                 [__DISMISS objectAtIndex:CURRENT_LANGUAGE]

 //Error/Loading strings
#define __LOADING               [NSArray arrayWithObjects:@"Retrieving Weather", @"Получение Погода", @"Recupero Meteo", @"Recuperando tiempo", @"отримання Погода", @"Suchen Wetter", @"Recuperando Tempo", @"preluarea meteo", @"Pobieranie Pogoda", @"Haetaan Sää", @"Ophalen Weer", @"Récupération Météo", @"Извличане Времето", @"Hämtar Väder", @"檢索天氣", @"检索天气", nil]
#define LOADING                 [__LOADING objectAtIndex:CURRENT_LANGUAGE]

#define __CONNECTION_ERROR      [NSArray arrayWithObjects:@"Connection Error", @"Ошибка соединения", @"Errore di connessione", @"Error de conexión", @"Помилка з'єднання", @"Verbindungsfehler", @"Erro de conexão", @"preluarea meteo", @"Błąd połączenia", @"Yhteysvirhe", @"Aansluiting Fout", @"Erreur de connexion", @"грешка във връзката", @"anslutningsfel visas", @"連接錯誤", @"连接错误", nil]
#define CONNECTION_ERROR        [__CONNECTION_ERROR objectAtIndex:CURRENT_LANGUAGE]

#define __DATA_UP_TO_DATE       [NSArray arrayWithObjects:@"Current Weather is Up To Date", @"Погода в курсе", @"Previsioni del tempo corrente sia aggiornato", @"El tiempo actual está actualizado", @"Погода в курсі", @"Aktuelles Wetter auf dem neuesten Stand", @"O tempo atual é até à data", @"Vremea este de până la data", @"Aktualna pogoda jest na bieżąco", @"Säätila on ajan tasalla", @"Actueel weer is up-to-date", @"Météo actuelle est à jour", @"Времето в момента е актуален", @"Aktuellt väder är aktuell", @"當前天氣是最新的", @"当前天气是最新的", nil]
#define DATA_UP_TO_DATE         [__DATA_UP_TO_DATE objectAtIndex:CURRENT_LANGUAGE]

#define __TIMEOUT_ERROR         [NSArray arrayWithObjects:@"Weather Update Timed Out", @"Погода обновление по тайм-ауту", @"Meteo aggiornamento scaduta", @"Update Tiempo Tiempo de espera agotado", @"Погода оновлення по тайм-ауту", @"Wetter aktualisieren Zeitüberschreitung", @"Tempo de atualização expirou", @"Actualizare vreme expirat", @"Upłynął limit czasu aktualizacji pogody", @"Sää Update aikakatkaisu", @"Weer-update verlopen", @"Mise à jour météo expiré", @"Времето обновяване изтече", @"Väderuppdateringen gått ut", @"天氣更新超時", @"天气更新超时", nil]
#define TIMEOUT_ERROR           [__TIMEOUT_ERROR objectAtIndex:CURRENT_LANGUAGE]

#define __LOCATION_ERROR        [NSArray arrayWithObjects:@"Failed to retrieve Location", @"Не удалось получить расположение", @"Impossibile recuperare Località", @"No se pudo recuperar Ubicación", @"Не вдалося розташування", @"Fehler beim Standort abrufen", @"Falha ao recuperar Localização", @"Nu pentru a prelua Locație", @"Nie można pobrać Lokalizacja", @"Noutaminen epäonnistui Sijainti", @"Mislukt locatie ophalen", @"Impossible de récupérer Lieu", @"Неуспешно извличане Местоположение", @"Det gick inte att hämta Plats", @"檢索位置失敗", nil]
#define LOCATION_ERROR          [__LOCATION_ERROR objectAtIndex:CURRENT_LANGUAGE]

#define __LOCATION_DENIED       [NSArray arrayWithObjects:@"Location Updates Denied", @"Расположение Обновление Denied", @"Località Aggiornamenti Denied", @"Ubicación Actualizaciones Denied", @"Розміщення Оновлення Denied", @"Ortsaktualisierungen verweigert", @"Localização Updates negado", @"Locație Actualizări negat", @"Aktualizacje Odmowa Lokalizacja", @"Sijainti päivitykset kielsi", @"Locatie-updates ontkend", @"Mises à jour de l'emplacement refuser", @"Местоположение Updates Отказан", @"Platsuppdateringar nekad", @"位置更新拒絕", @"检索位置失败", nil]
#define LOCATION_DENIED         [__LOCATION_DENIED objectAtIndex:CURRENT_LANGUAGE]

#define __LOCATION_UNKNOWN      [NSArray arrayWithObjects:@"Unknown Location Failure", @"Неизвестная ошибка Расположение", @"Posizione sconosciuta Fallimento", @"Desconocido Ubicación fracaso", @"Невідома помилка Розміщення", @"unbekannten Ort Ausfall", @"Desconhecido Localização falha", @"Necunoscut Locație Nerespectarea", @"Nieznana awaria lokalizacja", @"Tuntematon Paikkakunta jättäminen", @"Onbekende locatie falen", @"Situation inconnue échec", @"Непознат файл Местоположение Неизпълнение", @"Okänd Plats Misslyckande", @"未知位置失敗", @"未知位置失败", nil]
#define LOCATION_UNKNOWN        [__LOCATION_UNKNOWN objectAtIndex:CURRENT_LANGUAGE]

//Settings panel strings
#define COMPANY_TITLE           @"\u00A9 AF Apps 2013"

#define __UNIT_TITLE            [NSArray arrayWithObjects:@"Weather Unit", @"Метеорологический блок", @"Meteo Unità", @"Unidad de tiempo", @"метеорологічний блок", @"Wetter Einheit", @"Unidade de Tempo", @"Unitatea meteo", @"Jednostka pogody", @"Sää yksikkö", @"weer eenheid", @"Unité météo", @"Времето звено", @"Väder enhet", @"氣象單位", @"气象单位", nil]
#define UNIT_TITLE              [__UNIT_TITLE objectAtIndex:CURRENT_LANGUAGE]

#define __THEME_TITLE           [NSArray arrayWithObjects:@"Theme", @"тема", @"argomento", @"tema", @"Тема", @"Thema", @"tema", @"temă", @"temat", @"teema", @"thema", @"thème", @"тема", @"Tema", @"主題", @"主题", nil]
#define THEME_TITLE             [__THEME_TITLE objectAtIndex:CURRENT_LANGUAGE]

#define __LANGUAGE_TITLE        [NSArray arrayWithObjects:@"Language", @"язык", @"lingua", @"idioma", @"Мова", @"Sprache", @"linguagem", @"limbă", @"język", @"kieli", @"taal", @"langue", @"език", @"Språk", @"語", @"语", nil]
#define LANGUAGE_TITLE          [__LANGUAGE_TITLE objectAtIndex:CURRENT_LANGUAGE]

#define __MAPDATA_TITLE         [NSArray arrayWithObjects:@"Map data by OpenWeatherMap", @"Карта данных OpenWeatherMap", @"Mappa dati da parte OpenWeatherMap", @"Datos de mapa de OpenWeatherMap", @"Карта даних OpenWeatherMap", @"Karte Daten durch OpenWeatherMap", @"Mapa de dados por OpenWeatherMap", @"Date cartografice de OpenWeatherMap", @"Mapa przez OpenWeatherMap", @"Kartta tietojen OpenWeatherMap", @"Kaartgegevens door OpenWeatherMap", @"Carte de données par OpenWeatherMap", @"Карта данни от OpenWeatherMap", @"Karta uppgifter OpenWeatherMap", @"地圖數據OpenWeatherMap提供", @"地图数据OpenWeatherMap提供", nil]
#define MAPDATA_TITLE           [__MAPDATA_TITLE objectAtIndex:CURRENT_LANGUAGE]

//Weather panel strings
#define __NOWEATHER             [NSArray arrayWithObjects:@"Extended forecast unavailable for this date", @"Расширенный прогноз недоступны для этой даты", @"Meteo non disponibili per questa data", @"Pronóstico extendido no está disponible para esta fecha", @"Розширений прогноз недоступні для цієї дати", @"Erweiterte Vorhersage nicht verfügbar für dieses Datum", @"Previsão estendida disponível para esta data", @"Previziuni extinse indisponibil pentru această dată", @"Rozszerzona Prognoza niedostępna dla tej daty", @"Laajennettu ennuste saatavilla tälle päivälle", @"Meerdaagse verwachting niet beschikbaar voor deze datum", @"Prévisions à long terme disponible pour cette date actuellement", @"Разширена направи прогноза за тази дата", @"Utökad prognos otillgänglig för detta datum", @"擴展預測此日期不可用", @"扩展预测此日期不可用", nil]
#define NOWEATHER               [__NOWEATHER objectAtIndex:CURRENT_LANGUAGE]

#define __WEATHER_PANEL_FOR     [NSArray arrayWithObjects:@"for", @"для", @"per", @"para", @"для", @"für", @"para", @"pentru", @"dla", @"varten", @"voor", @"pour", @"за", @"för", @"為", @"为", nil]
#define WEATHER_PANEL_FOR       [__WEATHER_PANEL_FOR objectAtIndex:CURRENT_LANGUAGE]

//Weather overview strings
#define __WEATHER_OVERVIEW_ON   [NSArray arrayWithObjects:@"on", @"на", @"su", @"en", @"на", @"auf", @"em", @"pe", @"na", @"päälle", @"op", @"sur", @"на", @"på", @"上", @"上", nil]
#define WEATHER_OVERVIEW_ON     [__WEATHER_OVERVIEW_ON objectAtIndex:CURRENT_LANGUAGE]

//Getting Started

#define __Started_TITLE             [NSArray arrayWithObjects:@"Getting Started", @"Приступая к работе", @"Guida introduttiva", @"Introducción", @"Приступаючи до роботи", @"Erste Schritte", @"Introdução", @"Noțiuni de bază", @"Pierwsze kroki", @"Aloittaminen", @"Aan de slag", @"Mise en route", @"Първи стъпки", @"Komma igång", @"入門", @"入门", nil]
#define Started_TITLE               [__Started_TITLE objectAtIndex:CURRENT_LANGUAGE]

#define __Started_1             [NSArray arrayWithObjects:@"Swipe to change day", @"Размах изменить день", @"Passare il dito per cambiare giorno", @"Pase para cambiar el días", @"Розмах змінити день", @"Prankenhieb zu Tag ändern", @"Passe para mudar de dia", @"Bețivan pentru a schimba zi", @"Przesuń palcem zmienić dzień", @"Pyyhkäise muuttaa päivä", @"Uithaal tot dag veranderen", @"Glissez pour changer le jour", @"Неточен удар да променят деня", @"Dra för att ändra dag", @"刷卡改變一天", @"刷卡改变一天", nil]
#define Started_1               [__Started_1 objectAtIndex:CURRENT_LANGUAGE]

#define __Started_2             [NSArray arrayWithObjects:@"Swipe up for weekly view", @"Проведите вверх по неделям", @"Scorrere verso l'alto per la vista settimanale", @"Flagelo para vista semanal", @"Проведіть вгору по тижнях", @"Prankenhieb up für wöchentliche Blick", @"Passe para exibição semanal", @"Treceți pentru vizualizare săptămânală", @"Przesuń się na widok tygodniowy", @"Pyyhkäise ylöspäin viikoittain näkymä", @"Veeg omhoog voor weekweergave", @"Glissez vers le haut pour la vue hebdomadaire", @"Неточен удар за Седмичен преглед", @"Svep upp för vecko vy", @"向上滑動每週視圖", @"向上滑动每周视图", nil]
#define Started_2               [__Started_2 objectAtIndex:CURRENT_LANGUAGE]

#define __Started_3             [NSArray arrayWithObjects:@"Swipe down for settings view", @"Проведите вниз для настройки просмотра", @"Passare il dito verso il basso per le impostazioni di visualizzazione", @"Flagelo hacia abajo para ver los ajustes", @"Проведіть вниз для налаштування перегляду", @"Prankenhieb nach unten für Einstellungen Blick", @"Deslize para ajustes vista", @"Deplasați degetul în jos pentru setările de vedere", @"Przesuń w dół do ustawienia widoku", @"Pyyhkäise alas asetusnäkymässä", @"Veeg naar beneden voor instellingen view", @"Balayez vers le bas pour afficher les paramètres", @"Плъзнете надолу за настройки оглед", @"Svep nedåt för inställningar vy", @"向下滑動設置視圖", @"向下滑动设置视图", nil]
#define Started_3               [__Started_3 objectAtIndex:CURRENT_LANGUAGE]

#define __Started_4             [NSArray arrayWithObjects:@"Tap for weather details", @"Нажмите на информацию о погоде", @"Toccare per i dettagli meteo", @"Pulse para obtener más detalles meteorológicos", @"Натисніть на інформацію про погоду", @"Tippen Sie für Wetterdaten", @"Toque para obter mais informações meteorológicas", @"Atingeți pentru detalii meteo", @"Dotknij szczegóły atmosferycznych", @"Hana sääyksityiskohtia", @"Tik voor weer informatie", @"Appuyez sur pour plus de détails météorologiques", @"Натиснете за метеорологични данни", @"Knacka för väderinformation", @"輕觸天氣詳情", @"轻触天气详情", nil]
#define Started_4               [__Started_4 objectAtIndex:CURRENT_LANGUAGE]

#define __HUMIDITY             [NSArray arrayWithObjects:@"Humidity", @"влажность", @"Umidità", @"humedad", @"Вологість", @"Luftfeuchtigkeit", @"umidade", @"umiditate", @"wilgotność", @"kosteus", @"vochtigheid", @"Humidité", @"влажност", @"Fuktighet", @"濕度", @"湿度", nil]
#define HUMIDITY               [__HUMIDITY objectAtIndex:CURRENT_LANGUAGE]
