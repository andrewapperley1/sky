//
//  WeatherPanelObject.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-05-02.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "SimpleWeatherObject.h"
#import "WeatherPanelObject.h"

#define TEMP_LABEL_SIZE IS_IPAD() ? 28 : 15
#define DESC_LABEL_SIZE IS_IPAD() ? 22 : 13
#define TIME_LABEL_SIZE IS_IPAD() ? 24 : 14

@implementation WeatherPanelObject
@synthesize data = _data;
@synthesize isCurrentTime;

-(void)setData:(SimpleWeatherObject *)data
{
    if(_data)
        destroy(_data);
    _data = [data retain];
    [self updateText];
}

- (id)initWithFrame:(CGRect)frame andData:(SimpleWeatherObject *)ldata
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _data = [ldata retain];
        [self createUI];
        [self layoutSubviews];
    }
    return self;
}

- (void)createUI
{    
    //Condition
    weatherImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, [UIImage imageNamed:_data.image_url].size.width, [UIImage imageNamed:_data.image_url].size.height)];
    weatherImage.backgroundColor = [UIColor clearColor];
    weatherImage.image = [UIImage imageNamed:_data.image_url];
    
    //Temp
    tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, IS_IPAD() ? 85 : 50, self.affHeight)];
    tempLabel.textAlignment = UITextAlignmentRight;
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.font = [UIFont fontWithName:FONT_REGULAR size:TEMP_LABEL_SIZE];
    tempLabel.textColor = RGB(254, 254, 254);
    tempLabel.text = [NSString stringWithFormat:@"%@%@", _data.temp,TEMP_MODE];
    
    //Desc
    desc = [[UILabel alloc] initWithFrame:CGRectMake(tempLabel.affX + tempLabel.affWidth + 15, 0, 0, self.affHeight)];
    desc.backgroundColor = [UIColor clearColor];
    desc.font = [UIFont fontWithName:FONT_REGULAR size:DESC_LABEL_SIZE];
    desc.textColor = RGB(254, 254, 254);
    
    //Time
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(desc.affX + desc.affWidth - 5, 0, IS_IPAD() ? 140 : 100, self.affHeight)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.font = [UIFont fontWithName:FONT_REGULAR size:TIME_LABEL_SIZE];
    timeLabel.textColor = RGB(254, 254, 254);
    timeLabel.text = _data.time;
    
    [self addSubview:desc];
    [self addSubview:timeLabel];
    [self addSubview:tempLabel];
    [self addSubview:weatherImage];
}

- (void)updateText
{
    weatherImage.image = [UIImage imageNamed:_data.image_url];
    weatherImage.affWidth = [UIImage imageNamed:_data.image_url].size.width + 10;
    weatherImage.affHeight = [UIImage imageNamed:_data.image_url].size.height + 10;
    
    tempLabel.text = [NSString stringWithFormat:@"%@%@", _data.temp,TEMP_MODE];
    timeLabel.text = _data.time;
    [self layoutSubviews];
}

- (void)layoutSubviews
{    
    CENTER_OBJECT_Y(weatherImage, self);
    CENTER_OBJECT_Y(tempLabel, self);
    CENTER_OBJECT_Y(desc, self);
    CENTER_OBJECT_Y(timeLabel, self);

    if(IS_IPAD())
    {
        NSString *descText = [NSString stringWithFormat:@"%@ %@  ", _data.desc, WEATHER_PANEL_FOR];
        desc.text = [descText stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[descText substringToIndex:1] capitalizedString]];
        [desc sizeToFit];
        
        //Landscape
        if(isLandscape)
        {
            tempLabel.affX = 55;
            desc.affX = 165;
            timeLabel.affX = desc.affX + desc.affWidth - 2;
        }
        //Portrait
        else
        {
            tempLabel.affX = 60;
            desc.affX = 160;
            timeLabel.affX = desc.affX + desc.affWidth - 5;
        }
    } else {
        //Landscape
        if(isLandscape)
        {
            desc.text = [NSString stringWithFormat:@" %@  ", WEATHER_PANEL_FOR];
            [desc sizeToFit];
            
            tempLabel.affX = 40;
            desc.affX = 95;
            timeLabel.affX = desc.affX + desc.affWidth - 2;
        }
        //Portrait
        else
        {
            NSString *descText = [NSString stringWithFormat:@"%@ %@  ", _data.desc, WEATHER_PANEL_FOR];
            desc.text = [descText stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[descText substringToIndex:1] capitalizedString]];
            [desc sizeToFit];
            
            tempLabel.affX = 45;
            desc.affX = 105;
            timeLabel.affX = desc.affX + desc.affWidth - 5;
        }
    }
}

/*
 * Change background for current time
 */
- (void)setIsCurrentTime:(BOOL)_isCurrentTime
{
    if(_isCurrentTime)
        self.backgroundColor = APP_COLOR_SAVED;
    else
        self.backgroundColor = [UIColor clearColor];
}

- (void)dealloc
{
    destroy(timeLabel);
    destroy(tempLabel);
    destroy(weatherImage);
    destroy(desc);
    destroy(_data);
    
    [super dealloc];
}

@end
