//
//  WeatherPanelObject.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-05-02.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SimpleWeatherObject;

@interface WeatherPanelObject : UIView
{
    @private    
    UIImageView *weatherImage;
    UILabel *tempLabel;
    UILabel *desc;
    UILabel *timeLabel;
}

@property (nonatomic, assign) BOOL isCurrentTime;
@property (nonatomic, assign) SimpleWeatherObject *data;

- (id)initWithFrame:(CGRect)frame andData:(SimpleWeatherObject *)ldata;

@end
