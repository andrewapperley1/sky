//
//  SwipeUpScrollViewObject.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "SwipeUpScrollViewObject.h"

@implementation SwipeUpScrollViewObject
@synthesize isSelected;

AFFEventSynthesize(AFFEventInstance, evtSelected);

- (id)initWithFrame:(CGRect)frame withIndex:(uint)lindex
{
    self = [super initWithFrame:frame];
    if (self)
    {
        index = lindex;
        [self createButton];
    }
    return self;
}

- (void)createButton
{
    backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.affWidth, self.affHeight)];
    [backgroundButton addTarget:self action:@selector(onSelected) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:backgroundButton];
}

- (void)onSelected
{
    [[self evtSelected] send:[NSNumber numberWithUnsignedInt:index]];
    self.isSelected = TRUE;
}

- (void)onOver
{
    self.isSelected = TRUE;
}

- (void)onOut
{
    self.isSelected = FALSE;
}

- (void)dealloc
{
    destroy(backgroundButton);

    AFFRemoveAllEvents();
    [super dealloc];
}

@end
