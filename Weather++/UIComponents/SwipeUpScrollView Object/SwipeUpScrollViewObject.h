//
//  SwipeUpScrollViewObject.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SwipeUpScrollViewObject : UIView
{
    @private
    uint index;
    UIButton *backgroundButton;
}

@property (nonatomic, assign) BOOL isSelected;

- (id)initWithFrame:(CGRect)frame withIndex:(uint)lindex;

AFFEventCreate(AFFEventInstance, evtSelected);

@end
