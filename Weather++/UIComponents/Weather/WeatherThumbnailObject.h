//
//  WeatherThumbnailObject.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "SwipeUpScrollViewObject.h"

@class WeatherObject;

@interface WeatherThumbnailObject : SwipeUpScrollViewObject
{
    @private
    WeatherObject *data;
    
    UIImageView *conditionImageView;
    UILabel *tempLabel;
    UILabel *descLabel;
}

- (id)initWithFrame:(CGRect)frame withIndex:(uint)lindex andData:(WeatherObject *)ldata;

@end
