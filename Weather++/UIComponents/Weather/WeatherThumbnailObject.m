//
//  WeatherThumbnailObject.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "WeatherObject.h"
#import "WeatherThumbnailObject.h"

#define IMAGE_SIZE IS_IPAD() ? 48 : 24
#define TEMP_LABEL_SIZE IS_IPAD() ? 30 : 15
#define DESC_LABEL_SIZE IS_IPAD() ? 24 : 12

@implementation WeatherThumbnailObject

- (id)initWithFrame:(CGRect)frame withIndex:(uint)lindex andData:(WeatherObject *)ldata
{
    self = [super initWithFrame:frame withIndex:lindex];
    if (self)
    {
        data = [ldata retain];
        [self createUI];
        [self updateUI];
    }
    return self;
}

- (void)createUI
{    
    //Create Icon
    conditionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, IMAGE_SIZE, IMAGE_SIZE)];
    conditionImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:conditionImageView];
    
    //Create temp
    tempLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    tempLabel.font = [UIFont fontWithName:FONT_REGULAR size:TEMP_LABEL_SIZE];
    tempLabel.numberOfLines = 1;
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.textColor = RGB(254, 254, 254);
    [self addSubview:tempLabel];
    
    //Create description
    descLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    descLabel.font = [UIFont fontWithName:FONT_REGULAR size:DESC_LABEL_SIZE];
    descLabel.numberOfLines = 2;
    descLabel.lineBreakMode = UILineBreakModeWordWrap;
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.textColor = RGB(254, 254, 254);
    [self addSubview:descLabel];
}

- (void)setIsSelected:(BOOL)isSelected
{
    [super setIsSelected:isSelected];
    
    if(isSelected)
        self.backgroundColor = APP_COLOR_SAVED;
    else
        self.backgroundColor = [UIColor clearColor];
}

- (void)updateUI
{
    //stupid fix
    NSString *desc = [[((WeatherObject *)data).weather objectForKey:@"description"] isEqualToString:@"sky is clear" ]? @"clear skies" : [((WeatherObject *)data).weather objectForKey:@"description"];
    //
    
    conditionImageView.image = [UIImage imageNamed:[(WeatherObject *)data image]];        
    tempLabel.text = [NSString stringWithFormat:@"%@%@",[[(WeatherObject *)data temp] objectForKey:@"temp"],TEMP_MODE];
    
    //Description label
    NSMutableString *descText = nil;
    
    //Date
    if([[(WeatherObject *)data.date description] isEqualToString:TOMORROW] || [[(WeatherObject *)data.date description] isEqualToString:TODAY])
    {
        descText = [NSMutableString stringWithFormat:@"%@ ", desc];
        [descText appendString:[[(WeatherObject *)data.date description] lowercaseString]];
     } else {
        descText = [NSMutableString stringWithFormat:@"%@ %@ ", desc, WEATHER_OVERVIEW_ON];

        [descText appendString:[(WeatherObject *)data.date description]];
    }
    
    descLabel.text = [descText stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[descText substringToIndex:1] capitalizedString]];
}

- (void)layoutSubviews
{
    [conditionImageView setFrame:CGRectMake(10, 0, conditionImageView.image.size.width, conditionImageView.image.size.height)];
    
    if(IS_IPAD())
        conditionImageView.affScaleX = conditionImageView.affScaleY = 0.5f;
    else
        conditionImageView.affScaleX = conditionImageView.affScaleY = 0.25f;        
    
    CENTER_OBJECT_Y(conditionImageView, self);
    
    [tempLabel sizeToFit];
    CENTER_OBJECT_Y(tempLabel, self);
        
    //Landscape
    if(isLandscape)
    {
        conditionImageView.affX = 60;
        tempLabel.affX = IS_IPAD() ? 200 : 130;
        descLabel.affX = IS_IPAD() ? 300 : 190;
        descLabel.frame = CGRectMake( IS_IPAD() ? 300 : 190, 0, ScreenHeight - 190 - conditionImageView.affX, self.affHeight);
    }
    //Portrait
    else
    {
        conditionImageView.affX = IS_IPAD() ? 40 : 0;
        tempLabel.affX = IS_IPAD() ? 160 : 60;
        descLabel.frame = CGRectMake(IS_IPAD() ? 245 : 105, 0, ScreenWidth - ( IS_IPAD() ? 235 : 105) - conditionImageView.affX, self.affHeight);
    }
        
    CENTER_OBJECT_Y(descLabel, self);
}

- (void)dealloc
{
    destroy(data);
    
    destroy(conditionImageView);
    destroy(tempLabel);
    destroy(descLabel);
    
    [super dealloc];
}

@end
