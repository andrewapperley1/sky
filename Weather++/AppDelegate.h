//
//  AppDelegate.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
AFFEventCreate(AFFEventClass, evtCheckTimer);
@end
