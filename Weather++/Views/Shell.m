//
//  Shell.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "WeatherService.h"
#import "WeatherViewController.h"
#import "Database.h"
#import "WeatherView.h"
#import "WeatherDatabase.h"
#import "SettingsColourGridItem.h"
#import "Reachability.h"

const short VIEW_CONTROLLER_INDEX_WEATHER   = 0;
@implementation Shell
@synthesize weatherViewController = _weatherViewController;
@synthesize service;
@synthesize errorLabel;
@synthesize _error;
@synthesize offline = _offline;
@synthesize _isLandscape = __isLandscape;
@synthesize _firstBoot;

AFFEventSynthesize(AFFEventInstance, evtLocationChanged);
AFFEventSynthesize(AFFEventInstance, evtRotationChanged);
AFFEventSynthesize(AFFEventInstance, evtWentOffline);
AFFEventSynthesize(AFFEventInstance, evtDismissMessage);

static Shell *_shell = nil;

+ (Shell *)shell
{
    return _shell;
}

- (void)set_isLandscape:(BOOL)_isLandscape
{
    __isLandscape = _isLandscape;
    [[self evtRotationChanged] send];
}

- (BOOL)_isLandscape
{
    return __isLandscape;
}

- (void)setOffline:(BOOL)offline
{
    _offline = offline;
    if(!_weatherViewController)
        return;
    if(_offline == true && self.weatherViewController.view != nil && [self.weatherViewController.view isKindOfClass:[WeatherView class]])
        [[self evtWentOffline] send];
}

- (BOOL)offline
{
    return _offline;
}

- (id)initWithFrame:(CGRect)frame andViewController:(AFFViewController *)viewController
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _shell = self;
                
        _shell.backgroundColor = APP_COLOR_MAIN;
        
        _firstBoot = FALSE;
        
        _offline = false;
                
        self.parentViewController = viewController;
        
        [self createUI];
        
        [self createDatabase];
        [self startStandardUpdates];
    }
    return self;
}

/*
 * Set default language
 */
+ (BOOL)firstBoot
{
    if(![UserDefaults objectForKey:KEY_DEFAULT_LANGUAGE])
    {        
        //Set default language from device
        NSString *defaultLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
        uint languageCount = [LANGUAGES count];
        uint i = 0;
        while (i <= languageCount)
        {
            if(i == languageCount)
            {
                //If no default language is found after cycling through available languages then change the default language to 'en'
                SET_CURRENT_LANGUAGE_INDEX(0);
                break;
            }
            
            if([defaultLanguage isEqualToString:[LANGUAGES objectAtIndex:i]])
            {
                SET_CURRENT_LANGUAGE_INDEX(i);
                break;
            }
            i++;
        }
        SyncDefaults;
        
        return TRUE;
    }
    return FALSE;
}

/*
 * Querying Api UI
 */
- (void)createUI
{
    blocker = [[UIButton alloc] initWithFrame:self.frame];
    blocker.backgroundColor = APP_COLOR_MAIN;
    blocker.userInteractionEnabled = FALSE;
    blocker.alpha = 0;
    [blocker addTarget:self action:@selector(blockerDismissed) forControlEvents:UIControlEventTouchUpInside];
    [[self evtDismissMessage] addHandler:AFFHandler(@selector(blockerDismissed))];
    [self addSubview:blocker];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    CENTER_OBJECT(spinner, blocker);
    spinner.alpha = 0;
    [self addSubview:spinner];
    
    errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, spinner.affY + spinner.affHeight + 20, self.affWidth, 50)];
    errorLabel.textColor = RGB(254, 254, 254);
    errorLabel.numberOfLines = 0;
    errorLabel.backgroundColor = [UIColor clearColor];
    errorLabel.font = [UIFont fontWithName:FONT_REGULAR size:IS_IPAD() ? 32 : 20];
    errorLabel.textAlignment = UITextAlignmentCenter;
    CENTER_OBJECT_X(errorLabel, self);
    [blocker addSubview:errorLabel];
}

- (void)blockerDismissed
{
    if(!_weatherViewController)
        return;
    if(([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable))
        self.offline = true;
   
    [self queryingApi];
    

}

- (void)queryingApi
{

    [self bringSubviewToFront:blocker];
    [self bringSubviewToFront:spinner];

    
    
    if(blocker.userInteractionEnabled){
        blocker.userInteractionEnabled = FALSE;
        [UIView animateWithDuration:0.5f animations:^{
            if(!blocker.userInteractionEnabled)
                blocker.alpha = spinner.alpha = 0.0f;
        } completion:^(BOOL done){
            if(!blocker.userInteractionEnabled)
                [spinner stopAnimating];
        }];
    } else {
        blocker.userInteractionEnabled = TRUE;
        blocker.backgroundColor = APP_COLOR_SAVED;
        [self bringSubviewToFront:blocker];
        [self bringSubviewToFront:spinner];
        [UIView animateWithDuration:0.5f animations:^{
            blocker.alpha = 0.9f;
            spinner.alpha = 1.0f;
            [spinner startAnimating];
        }];
    }
}

/*
 * Database
 */
- (void)createDatabase
{
    database = [Database new];
}

/*
 * View controllers
 */

- (void)createViewControllers
{   
    _weatherViewController = [WeatherViewController new];
    
    [self.parentViewController addChildViewController:_weatherViewController];
        
    [[self evtReady] send];
    
    [self openWeatherViewFrom:VIEW_POSITION_CENTER withClose:VIEW_POSITION_CENTER andFadeIn:TRUE];
}

- (WeatherViewController *)weatherViewController
{
    return (WeatherViewController *)[[self.parentViewController childViewControllers] objectAtIndex:VIEW_CONTROLLER_INDEX_WEATHER];
}

/*
 * Create weather service
 */
- (void)createWeatherService
{
    if(service)
        return;
    
    if((id)[UserDefaults boolForKey:WEATHER_MODE] == nil)
        [UserDefaults setBool:FALSE forKey:WEATHER_MODE];
    service = [WeatherService new];
    [service initalStart];
    [[service evtWeatherUpdated] addHandlerOneTime:AFFHandler(@selector(finishBoot))];
    
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    
    if (locationManager){
        locationManager.delegate = nil;
        destroy(locationManager);
    }
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500;
    
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{

    if((newLocation.coordinate.longitude - [UserDefaults doubleForKey:@"locationLong"] >= 0.10f || newLocation.coordinate.longitude - [UserDefaults doubleForKey:@"locationLong"] <= -0.10f) || (newLocation.coordinate.latitude - [UserDefaults doubleForKey:@"locationLat"] >= 0.10f || newLocation.coordinate.latitude - [UserDefaults doubleForKey:@"locationLat"] <= -0.10f))
    {
        [UserDefaults setDouble:newLocation.coordinate.longitude forKey:@"locationLong"];
        [UserDefaults setDouble:newLocation.coordinate.latitude forKey:@"locationLat"];
        _error = FALSE;
        self.offline = false;
        [[self evtLocationChanged] send];
    }
    
    if(!_firstBoot)
    {
        [self createWeatherService];
        
    } else {
        [[self service] queryWeather];
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    switch([error code])
    {
        case kCLErrorNetwork:
            errorLabel.text = LOCATION_ERROR;
            break;
        case kCLErrorDenied:
            errorLabel.text = LOCATION_DENIED;
            break;
        default:
             errorLabel.text = LOCATION_ERROR;
            break;
    }
    
    if(!_firstBoot || !_weatherViewController)
    {
        _firstBoot = TRUE;
        _error = true;
        [self createViewControllers];
        [self createWeatherService];
        return;
    } else {
        _error = true;
        _offline = true;
    }
    
    [self queryingApi];
    
}
- (void)finishBoot
{
    _firstBoot = TRUE;
    [self createViewControllers];
    [self setNeedsLayout];
}

/*
 * Views
 */
- (void)openWeatherViewFrom:(CGPoint)fromPoint withClose:(CGPoint)closePoint
{
    [self openWeatherViewFrom:fromPoint withClose:closePoint andFadeIn:FALSE];
}

- (void)openWeatherViewFrom:(CGPoint)fromPoint withClose:(CGPoint)closePoint andFadeIn:(BOOL)fadeIn
{
    [self.weatherViewController openView:fromPoint andClosePosition:closePoint withDayDifference:0 andFadeIn:fadeIn];
    [self addSubview:self.weatherViewController.view];
}

- (BOOL)hasScrollViewOpen
{
    return self.weatherViewController.isScrollViewOpened;
}

- (BOOL)viewAnimating
{
    return self.weatherViewController.isOpening || self.weatherViewController.isClosing;
}

/*
 * View controller swipes
 */
- (void)createViewControllerSwipesHandlers
{
    [[self.weatherViewController evtSwipedLeft] addHandler:AFFHandler(@selector(onWeatherSwipeLeft))];
}

- (void)layoutSubviews
{
    if(self.parentViewController.childViewControllers.count > 0)
    {
        self.weatherViewController.view.backgroundColor = self.backgroundColor = APP_COLOR_SAVED;
        [self.weatherViewController layoutSubviews];
    }
    
    //Landscape
    if(isLandscape)
    {        
        blocker.frame = CGRectMake(0, 0,  ScreenHeight, ScreenWidth);
        CENTER_OBJECT(spinner, blocker);
        errorLabel.affY = spinner.affY + spinner.affHeight + 20;
        CENTER_OBJECT_X(errorLabel, blocker);
        
    //Portrait
    } else {
        blocker.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        CENTER_OBJECT(spinner, blocker);
        errorLabel.affY = spinner.affY + spinner.affHeight + 20;
        CENTER_OBJECT_X(errorLabel, blocker);
    }
}

- (void)dealloc
{
    destroy(_weatherViewController);
    destroy(service);
    destroy(locationManager);
    locationManager.delegate = nil;
    
    destroyAndRemove(blocker);
    destroyAndRemove(spinner);
    destroyAndRemove(errorLabel);
    
    AFFRemoveAllEvents();
    
    [super dealloc];
}
@end