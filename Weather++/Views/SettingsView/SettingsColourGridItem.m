//
//  SettingsColourGridItem.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-01.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "SettingsColourGridItem.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation SettingsColourGridItem

@synthesize colour, index;
AFFEventSynthesize(AFFEventInstance, evtItemSelected);
- (id)initWithFrame:(CGRect)frame andColour:(UIColor *)lcolour andIndex:(int)lindex
{
    self = [super initWithFrame:frame];
    if (self) {
        colour = [lcolour retain];
        self.backgroundColor = colour;
        index = lindex;
//        [[SettingsColourGridItem evtItemSelected] addHandlerOneTime:AFFHandler(@selector(itemDeselected))];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemSelected)];
        tap.numberOfTapsRequired = 1;
        
        [self addGestureRecognizer:tap];
        
        destroy(tap);
        
    }
    return self;
}

- (void)itemDeselected
{
    self.userInteractionEnabled = false;
    self.layer.borderColor = [UIColor clearColor].CGColor;
    self.layer.borderWidth = 0;
}

- (void)itemSelected
{
    const CGFloat *components = CGColorGetComponents(colour.CGColor);
    
    [UserDefaults setFloat:components[0]  forKey:THEME_RED];
    [UserDefaults setFloat:components[1]  forKey:THEME_GREEN];
    [UserDefaults setFloat:components[2]  forKey:THEME_BLUE];
    [UserDefaults setInteger:index forKey:THEME_INDEX];
    
    [[self evtItemSelected] send];
    
}

- (void)setBorder
{
    self.layer.borderColor = RGB(254, 254, 254).CGColor;
    self.layer.borderWidth = 1;
}

- (void)dealloc
{
    AFFRemoveAllEvents();
    destroy(colour);
    [super dealloc];
}

@end