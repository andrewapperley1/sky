//
//  SettingsView.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-05-01.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFView.h"

@interface SettingsView : AFFView
{
    UILabel *unitTitle;
    UILabel *languageTitle;
    UILabel *themeTitle;
    UILabel *companyTitle;
    UILabel *mapDataTitle;
    NSArray *colours;
    UIButton *celButton;
    UIButton *ferButton;
    UIButton *lastClicked;
    UIButton *lastLangButton;
    UIScrollView *themeContainer;
    UIScrollView *languageContainer;
}
- (void)showIn;
- (void)showOut;

@property (nonatomic,assign)BOOL reQuery;

AFFEventCreate(AFFEventInstance, evtClose);

@end
