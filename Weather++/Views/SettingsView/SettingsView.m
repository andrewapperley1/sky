//
//  SettingsView.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-05-01.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "Masterskin.h"
#import "SettingsView.h"
#import "SettingsColourGridItem.h"

@implementation SettingsView
@synthesize reQuery;
AFFEventSynthesize(AFFEventInstance, evtClose);

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)

const int gridSize = 12;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        if([UserDefaults objectForKey:THEME_INDEX] == nil)
            [UserDefaults setInteger:4 forKey:THEME_INDEX];
        
        self.backgroundColor = APP_COLOR_SAVED;
        colours = [[NSArray alloc] initWithObjects:
                   //greens
                   RGB(164, 196, 1),
                   RGB(96, 170, 23),
                   RGB(1, 139, 0),
                   RGB(1, 171, 170),
                   //blues
                   RGB(101, 199, 195),
                   RGB(64, 110, 163),
                   RGB(90, 56, 125),
                   RGB(119, 56, 125),
                   //reds
                   RGB(244, 115, 208),
                   RGB(161, 0, 36),
                   RGB(129, 7, 7),
                   RGB(250, 105, 0),
                   nil];
                
        [self createView];
    }
    return self;
}

- (void)createView
{

    languageTitle = [[self createLabel:LANGUAGE_TITLE andSize:20 andAlignment:UITextAlignmentLeft andColour:RGB(254, 254, 254) andWidth:ScreenWidth] retain];
    languageTitle.affX = 20;
    languageTitle.affY = 15;
    
    [self createLanguageScrollView];
    
    int unitTitleOffset = IS_IPHONE_5 ? 40 : 60;
    unitTitle = [[self createLabel:UNIT_TITLE andSize:20 andAlignment:UITextAlignmentLeft andColour:RGB(254, 254, 254) andWidth:ScreenWidth] retain];
    unitTitle.affX = 20;
    unitTitle.affY = 33 + unitTitleOffset;
    
    celButton = [[self createButtonWithLabel:@"\u00B0C" andTag:0] retain];
    celButton.affX = 20;
    ferButton = [[self createButtonWithLabel:@"\u00B0F" andTag:1] retain];
    ferButton.affX = celButton.affX + celButton.affWidth + 10;
    celButton.affY = ferButton.affY = unitTitle.affY + unitTitle.affHeight + 12;
    
    int themeTitleOffset = IS_IPHONE_5 ? 92 : 60;
    themeTitle = [[self createLabel:THEME_TITLE andSize:20 andAlignment:UITextAlignmentLeft andColour:RGB(254, 254, 254) andWidth:ScreenWidth] retain];
    themeTitle.affX = 20;
    themeTitle.affY = unitTitle.affY + unitTitle.affHeight + themeTitleOffset;
    
    [self createThemeGrid];
    
    companyTitle = [[self createLabel:COMPANY_TITLE andSize:IS_IPAD() ? 15 : 9 andAlignment:UITextAlignmentLeft andColour:RGB(254, 254, 254) andWidth:IS_IPAD() ? 200 : 80] retain];
    companyTitle.affX = 10;
    companyTitle.affY = self.affHeight - companyTitle.affHeight - 30;
    
    mapDataTitle = [[self createLabel:MAPDATA_TITLE andSize:IS_IPAD() ? 15 : 9 andAlignment:UITextAlignmentRight andColour:RGB(254, 254, 254) andWidth: IS_IPAD() ? 400 : 200] retain];
    mapDataTitle.affX = self.affWidth - mapDataTitle.affWidth - 10;
    mapDataTitle.affY = self.affHeight - mapDataTitle.affHeight - 30;
    
    [self addSubview:unitTitle];
    [self addSubview:languageTitle];
    [self addSubview:celButton];
    [self addSubview:ferButton];
    [self addSubview:themeTitle];
    [self addSubview:companyTitle];
    [self addSubview:mapDataTitle];
    
    [[self evtReady] send];
    
}

- (UIButton *)createButtonWithLabel:(NSString *)ltitle andTag:(NSInteger)ltag
{
    UIImage *_on = [UIImage imageNamed:SETTINGS_CELCIUS_ON];
    UIImage *_off = [UIImage imageNamed:SETTINGS_CELCIUS_OFF];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _on.size.width, _on.size.height)];
    [button setBackgroundImage:_on forState:UIControlStateSelected];
    [button setBackgroundImage:_off forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onUnitButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = ltag;
    
    if((BOOL)button.tag == [UserDefaults boolForKey:WEATHER_MODE]) {
        [button setSelected:TRUE];
        lastClicked = button;
    }
    UILabel *label = [self createLabel:ltitle andSize:20 andAlignment:UITextAlignmentCenter andColour:RGB(254, 254, 254) andWidth:button.affWidth];
    CENTER_OBJECT(label, button);
    
    [button addSubview:label];
    
    return [button autorelease];
}

- (UIButton *)createLanguageButtonWithLabel:(NSString *)ltitle andTag:(NSInteger)ltag
{
    UIImage *_on = [UIImage imageNamed:SETTINGS_CELCIUS_ON];
    UIImage *_off = [UIImage imageNamed:SETTINGS_CELCIUS_OFF];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _on.size.width, _on.size.height)];
    [button setBackgroundImage:_on forState:UIControlStateSelected];
    [button setBackgroundImage:_off forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onLangButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = ltag;
    
    if((int)button.tag == CURRENT_LANGUAGE) {
        [button setSelected:TRUE];
        lastLangButton = button;
    }
    UILabel *label = [self createLabel:[ltitle capitalizedString] andSize:14 andAlignment:UITextAlignmentCenter andColour:RGB(254, 254, 254) andWidth:button.affWidth];
    CENTER_OBJECT(label, button);
    
    [button addSubview:label];
    
    return [button autorelease];
}

- (void)onUnitButtonClicked:(UIButton *)sender
{
    if(sender == lastClicked)
        return;
    reQuery = TRUE;
    
    [UserDefaults setBool:(BOOL)sender.tag forKey:WEATHER_MODE];
    
    [sender setSelected:TRUE];
    
    [lastClicked setSelected:FALSE];
    lastClicked = sender;
    
}

- (void)onLangButtonClicked:(UIButton *)sender
{
    if(sender == lastClicked)
        return;
    reQuery = TRUE;
    
    SET_CURRENT_LANGUAGE_INDEX((int)sender.tag);
    
    [sender setSelected:TRUE];
    
    [lastLangButton setSelected:FALSE];
    lastLangButton = sender;
    
    [self refreshLanguage];
}

- (void)createLanguageScrollView
{
    int column = 0;
    languageContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, 280, 29)];
    languageContainer.showsHorizontalScrollIndicator = FALSE;
    languageContainer.showsVerticalScrollIndicator = FALSE;
    languageContainer.directionalLockEnabled = TRUE;

    for (int i = 0; i < [LANGUAGES count]; i++) {
        if(i > [LANGUAGES count] - 1)
            break;
        UIButton *item = [self createLanguageButtonWithLabel:[(NSString *)[LANGUAGE_BUTTONS objectAtIndex:i] capitalizedString] andTag:i];
        
        item.affY = 0;
        item.affX = ((item.affWidth + 5) * column ) + 25;
        [languageContainer addSubview:item];
        languageContainer.contentSize = CGSizeMake(((item.affWidth + 5) * (column + 1) ) + 25, item.affHeight);
        ++column;
    }
    CENTER_OBJECT_X(languageContainer, self);
    languageContainer.affY = languageTitle.affY + languageTitle.affHeight + 7;
    [self addSubview:languageContainer];

}

- (void)createThemeGrid
{
    int row = 0;
    int column = 0;
    themeContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, IS_IPAD() ? 768 : 320, IS_IPAD() ? 554 : 231)];
    themeContainer.showsHorizontalScrollIndicator = FALSE;
    themeContainer.showsVerticalScrollIndicator = FALSE;
    themeContainer.directionalLockEnabled = TRUE;
    for (int i = 0; colours.count - 1; i++) {
        if(i > colours.count - 1)
            break;
        SettingsColourGridItem *item = [[SettingsColourGridItem alloc] initWithFrame:CGRectMake(0, 0, IS_IPAD() ? 179 :  73.5, IS_IPAD() ? 179 : 73.5) andColour:[colours objectAtIndex:i] andIndex:i];
        
        if(i == [UserDefaults integerForKey:THEME_INDEX])
            [item setBorder];
        
        [[item evtItemSelected] addHandlerOneTime:AFFHandler(@selector(close))];
        
        item.affX = ((item.affWidth + 5) * column) + (IS_IPAD() ? 15 : 5);
        item.affY = ((item.affHeight + 5) * row) + (IS_IPAD() ? 4 : 0);
        [themeContainer addSubview:item];
        
        column++;
        if(column == 4) {
            column = 0;
            row++;
    }
        destroy(item);
    }
    
    CENTER_OBJECT_X(themeContainer, self);
    themeContainer.affY = themeTitle.affY + themeTitle.affHeight + 7;
    [self addSubview:themeContainer];
    
    destroy(colours);
}

- (void)close
{
    SyncDefaults;
    [[self evtClose] send];
}

- (UILabel *)createLabel:(NSString *)ltext andSize:(int)lsize andAlignment:(UITextAlignment)lalign andColour:(UIColor *)lcolour andWidth:(float)lwidth
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, lwidth, lsize + 3)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:FONT_REGULAR size:lsize];
    label.textAlignment = lalign;
    label.text = [ltext capitalizedString];
    label.textColor = lcolour;
    
    return [label autorelease];
}

- (void)showIn
{
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        self.alpha = 1;
        self.backgroundColor = APP_COLOR_SECONDARY;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

- (void)showOut
{
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        self.alpha = 0;
        self.backgroundColor = APP_COLOR_SAVED;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

- (void)layoutSubviews
{
    int column = 0;
    int row = 0;
    int unitTitleOffset = IS_IPHONE_5 ? 40 : 35;

    //portrait mode
    if(!isLandscape)
    {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        unitTitle.affY = 33 + unitTitleOffset + 20;
        themeTitle.affY = IS_IPHONE_5 ? unitTitle.affY + unitTitle.affHeight + 85 : unitTitle.affY + unitTitle.affHeight + 52;
        themeContainer.frame = CGRectMake(0, 0, IS_IPAD() ? 768 : 320, IS_IPAD() ? 554 : 231);
        celButton.affY = ferButton.affY = unitTitle.affY + unitTitle.affHeight + 12;
        themeContainer.contentOffset = CGPointZero;
        themeContainer.contentSize = CGSizeMake(themeContainer.affWidth, themeContainer.affHeight);
        for (SettingsColourGridItem *item in themeContainer.subviews) {
            if(item.index >gridSize)
                break;
            
            if(IS_IPAD())
                item.affWidth = item.affHeight = 179;
            
            item.affX = ((item.affWidth + 5) * column) + (IS_IPAD() ? 15 : 5);
            item.affY = ((item.affHeight + 5) * row) + (IS_IPAD() ? 4 : 0);
            
            
            column++;
            if(column == 4) {
                column = 0;
                row++;
            }
        }

        CENTER_OBJECT_X(themeContainer, self);
        themeContainer.affY = themeTitle.affY + themeTitle.affHeight + 7;
        
        languageContainer.frame = CGRectMake(20, languageContainer.affY, self.affWidth - 40, 29);
        column = 0;
        for (UIButton *item in languageContainer.subviews) {
            item.affY = 0;
            item.affX = ((item.affWidth + 5) * column ) + 5;
            languageContainer.contentSize = CGSizeMake(((item.affWidth + 5) * (column + 1) ) + 5, item.affHeight);
            ++column;
            languageContainer.contentOffset = CGPointMake(((item.affWidth + 5) * MIN(12,CURRENT_LANGUAGE) ) + 5, 0);
        }
    
    } else {
        
        self.frame = CGRectMake(0, 0, ScreenHeight, ScreenWidth);
        
        unitTitle.affY = 46 + unitTitleOffset;
        themeTitle.affY = unitTitle.affY + unitTitle.affHeight + 50;
        themeContainer.frame = IS_IPAD() ? themeContainer.frame : CGRectMake(0, 0, self.affWidth - 10, 73.5);
        celButton.affY = ferButton.affY = unitTitle.affY + unitTitle.affHeight + 12;
        CENTER_OBJECT_X(themeContainer, self);
        themeContainer.affY = themeTitle.affY + themeTitle.affHeight + 7;
        
        for (SettingsColourGridItem *item in themeContainer.subviews) {
            if(!IS_IPAD()){
                item.affY = 0;
                item.affX = ((item.affWidth + 5) * column ) + 5;
                themeContainer.contentSize = CGSizeMake(((item.affWidth + 5) * (column + 1) ) + 5, item.affHeight);
                ++column;
                themeContainer.contentOffset = CGPointMake(((item.affWidth + 5) * MIN(IS_IPHONE_5 ? 6 : 7,[UserDefaults integerForKey:THEME_INDEX]) ) + 5, 0);
            } else {
                item.affWidth = item.affHeight = 170;
                
                item.affX = ((item.affWidth + 5) * column) + (IS_IPAD() ? 15 : 5);
                item.affY = ((item.affHeight + 5) * row) + (IS_IPAD() ? 4 : 0);
                
                column++;
                if(column == 4) {
                    column = 0;
                    row++;
                }

            }
        } 
        languageContainer.frame = CGRectMake(20, languageContainer.affY, self.affWidth - 40, 29);
        column = 0;
        for (UIButton *item in languageContainer.subviews) {
            item.affY = 0;
            item.affX = ((item.affWidth + 5) * column ) + 5;
            languageContainer.contentSize = CGSizeMake(((item.affWidth + 5) * (column + 1) ) + 5, item.affHeight);
            ++column;
            languageContainer.contentOffset = CGPointMake(((item.affWidth + 5) * MIN(10,CURRENT_LANGUAGE) ) + 5, 0);
        }
        
    }
    companyTitle.affY = self.affHeight - companyTitle.affHeight - 30;
    mapDataTitle.affY = self.affHeight - mapDataTitle.affHeight - 30;
    mapDataTitle.affX = self.affWidth - mapDataTitle.affWidth - 10;
    
}

- (void)refreshLanguage
{
    //Set text
    unitTitle.text = [UNIT_TITLE capitalizedString];
    languageTitle.text = [LANGUAGE_TITLE capitalizedString];
    themeTitle.text = [THEME_TITLE capitalizedString];
    companyTitle.text = [COMPANY_TITLE capitalizedString];
    mapDataTitle.text = [MAPDATA_TITLE capitalizedString];
    
    //Reload labels
    [self setNeedsLayout];
}

- (void)dealloc
{
    AFFRemoveAllEvents();
    destroyAndRemove(unitTitle);
    destroyAndRemove(languageTitle);
    destroyAndRemove(themeTitle);
    destroyAndRemove(companyTitle);
    destroyAndRemove(mapDataTitle);
    destroyAndRemove(themeContainer);
    destroyAndRemove(celButton);
    destroyAndRemove(ferButton);
    
    for (SettingsColourGridItem *item in self.subviews) {
        destroyAndRemove(item);
    }
    
    [super dealloc];
}

@end
