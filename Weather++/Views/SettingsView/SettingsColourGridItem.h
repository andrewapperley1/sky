//
//  SettingsColourGridItem.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-01.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsColourGridItem : UIView

@property (nonatomic, retain)UIColor *colour;
@property (nonatomic, assign)int index;
- (id)initWithFrame:(CGRect)frame andColour:(UIColor *)lcolour andIndex:(int)lindex;
- (void)setBorder;
AFFEventCreate(AFFEventInstance, evtItemSelected);

@end