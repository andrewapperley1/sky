//
//  GettingStartedView.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "GettingStartedView.h"

@implementation GettingStartedView

AFFEventSynthesize(AFFEventInstance, evtClosed);

- (id)initWithFrame:(CGRect)frame andText:(NSString *)text andImage:(TutorialImage)limageType
{
    self = [super initWithFrame:frame];
    if (self)
    {
        pageText = text;
        imageType = limageType;
        
        [self createBackground];
        [self createLabels];
        [self createObjects];
        [self setNeedsLayout];
    }
    return self;
}

- (void)createObjects
{
    switch (imageType) {
        case ArrowLeftRight:
            
            actionIconArrowLeft = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ArrowLeft.png"]];
            [self addSubview:actionIconArrowLeft];
            
            actionIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ArrowRight.png"]];
            [self addSubview:actionIcon];
            break;
            
        case ArrowUp:
            
            actionIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ArrowUp.png"]];            
            [self addSubview:actionIcon];
            break;
        
        case ArrowDown:
            actionIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ArrowDown.png"]];
            [self addSubview:actionIcon];
            break;
        
        case Circle:
            actionIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Dot.png"]];
            [self addSubview:actionIcon];
            break;
            
    }    
    [[self evtReady] send];
}

- (void)createBackground
{
    self.backgroundColor = APP_COLOR_MAIN;
}

- (void)createLabels
{
    bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, ScreenHeight - 80 - StatusBarHeight, self.affWidth, 80)];
    bottomLabel.backgroundColor = [UIColor clearColor];
    bottomLabel.font = [UIFont fontWithName:FONT_REGULAR size: IS_IPAD() ? 32 : 20];
    bottomLabel.textAlignment = UITextAlignmentCenter;
    bottomLabel.text = pageText;
    bottomLabel.textColor = RGB(254, 254, 254);
    bottomLabel.numberOfLines = 0;
    
    titleText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.affWidth, 40)];
    titleText.backgroundColor = [UIColor clearColor];
    titleText.font = [UIFont fontWithName:FONT_REGULAR size:IS_IPAD() ? 32 : 20];
    titleText.textAlignment = UITextAlignmentCenter;
    titleText.text = Started_TITLE;
    titleText.textColor = RGB(254, 254, 254);
    
    logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:LOGO]];
    
    [self addSubview:titleText];
    [self addSubview:logo];
    [self addSubview:bottomLabel];
}

- (void)layoutSubviews
{
    if(isLandscape)
    {
        self.frame = CGRectMake(0, 0, ScreenHeight, ScreenWidth);
        
        //Logo
        logo.affX = 100;
        logo.affY = 5;
        
        //Labels
        titleText.frame = CGRectMake(0, IS_IPAD() ? 10 : 0, self.affWidth, 40);
        titleText.textAlignment = UITextAlignmentCenter;
        
        bottomLabel.frame = CGRectMake(0, self.affHeight - (IS_IPAD() ? 100 : 80), self.affWidth, 80);
        bottomLabel.textAlignment = UITextAlignmentCenter;
        
        //Images
        switch (imageType) {
            case ArrowLeftRight:
                
                actionIconArrowLeft.affX = IS_IPAD() ? 15 : 5;
                CENTER_OBJECT_Y(actionIconArrowLeft, self);

                actionIcon.affX = self.affWidth - actionIcon.affWidth - (IS_IPAD() ? 15 : 5);
                CENTER_OBJECT_Y(actionIcon, self);
                break;
                
            case ArrowUp:
                CENTER_OBJECT_X(actionIcon, self);
                actionIcon.affY = IS_IPAD() ? 300 : 78;
                break;
                
            case ArrowDown:
                CENTER_OBJECT_X(actionIcon, self);
                actionIcon.affY = IS_IPAD() ? 320 : 78;
                break;
                
            case Circle:
                CENTER_OBJECT(actionIcon, self);
                actionIcon.affY -= 10;
                break;
        }
    } else {
        
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);

        //Logo
        logo.affX = 55;
        logo.affY = 5;
        
        //Labels
        titleText.frame = CGRectMake(0, IS_IPAD() ? 10 : 0, self.affWidth, 40);
        titleText.textAlignment = UITextAlignmentCenter;
        
        bottomLabel.frame = CGRectMake(0, self.affHeight - (IS_IPAD() ? 100 : 80 - StatusBarHeight), self.affWidth, 80);
        bottomLabel.textAlignment = UITextAlignmentCenter;
        
        //Images
        switch (imageType) {
            case ArrowLeftRight:
                
                actionIconArrowLeft.affX = IS_IPAD() ? 15 : 5;
                actionIconArrowLeft.affY = IS_IPAD() ? 450 : 150;
                                
                actionIcon.affX = ScreenWidth - actionIcon.affWidth - ( IS_IPAD() ? 15 : 5);
                actionIcon.affY = IS_IPAD() ? 522 : 222;
                break;
                
            case ArrowUp:
                CENTER_OBJECT_X(actionIcon, self);
                actionIcon.affY = IS_IPAD() ? 400 : 150;
                break;
                
            case ArrowDown:
                CENTER_OBJECT_X(actionIcon, self);
                actionIcon.affY = IS_IPAD() ? 420 : 150;
                break;
                
            case Circle:
                CENTER_OBJECT(actionIcon, self);
                actionIcon.affY -= 10;
                break;
        }    
    }
}

- (void)dealloc
{
    pageText = nil;
    
    destroy(bottomLabel);
    destroy(titleText);
    destroy(actionIcon);
    destroy(logo);
    
    if(actionIconArrowLeft)
        destroy(actionIconArrowLeft);
    
    [super dealloc];
}

@end