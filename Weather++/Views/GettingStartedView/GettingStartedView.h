//
//  GettingStartedView.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-05-24.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFView.h"

enum TutorialImage
{
    ArrowLeftRight,
    ArrowUp,
    ArrowDown,
    Circle
};

typedef NSUInteger TutorialImage;

@interface GettingStartedView : AFFView
{
    @private
    uint tutorialViewIndex;
    UILabel *bottomLabel;
    NSString *pageText;
    UILabel *titleText;
    UIImageView *logo;
    UIImageView *actionIcon;
    UIImageView *actionIconArrowLeft;
    TutorialImage imageType;
}

- (id)initWithFrame:(CGRect)frame andText:(NSString *)text andImage:(TutorialImage)limageType;

@end
