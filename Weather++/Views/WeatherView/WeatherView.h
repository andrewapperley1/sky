//
//  WeatherView.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-14.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFView.h"

@class WeatherObject;

@interface WeatherView : AFFView
{
    UILabel *nameLabel;
    UILabel *dateLabel;
    UILabel *tempLabel;
    UILabel *descLabel;
    UILabel *minLabel;
    UILabel *maxLabel;
    UILabel *noWeatherTimes;
    UIImageView *conditionImageView;
    UIImageView *minMaxImageView;
    UIView *panel;
    BOOL mode;
    
    @private
    NSMutableArray *panelObjects;
    UIActivityIndicatorView *spinner;
}

@property (nonatomic, readonly) BOOL panelOpen;
@property (nonatomic, assign) WeatherObject *data;
@property (nonatomic, assign) BOOL isCurrentDay;

- (id)initWithFrame:(CGRect)frame andData:(WeatherObject *)ldata;
- (id)initWithFrame:(CGRect)frame andData:(WeatherObject *)ldata andIsCurrentDay:(BOOL)lisCurrentDay;
- (void)showExtraTimes;

//Animations
- (void)showIn;
- (void)showOut;

AFFEventCreate(AFFEventInstance, evtPanelClosed);

@end