//
//  WeatherView.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-14.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFCleanup.h"
#import "Database.h"
#import "Constants.h"
#import "Masterskin.h"
#import "WeatherPanelObject.h"
#import "WeatherView.h"
#import "WeatherService.h"
#import "WeatherObject.h"
#import "Reachability.h"
#import "SimpleWeatherObject.h"
#import "SettingsColourGridItem.h"

@implementation WeatherView

#define IPHONE_5 (isLandscape ? ScreenWidth : ScreenHeight) == 568 ? TRUE : FALSE

#define PANEL_HEIGHT (IS_IPAD() ? (isLandscape ? 390 : 540) : (isLandscape ? 230 : 350))
#define PANEL_WIDTH (isLandscape ? ScreenHeight : ScreenWidth) + (VIEW_INDICATOR_LAST_SIZE * 2)
#define PRECIP_SUFFIX ![UserDefaults boolForKey:WEATHER_MODE] ? @"mm" : @"in"
#define DECIMAL_PLACES ![UserDefaults boolForKey:WEATHER_MODE] ? @"%@ - %.02f%@" : @"%@ - %.02f%@"

@synthesize data;
@synthesize panelOpen = _panelOpen;
@synthesize isCurrentDay;

AFFEventSynthesize(AFFEventInstance, evtPanelClosed);

- (void)setData:(WeatherObject *)ldata
{
    if(data)
        destroy(data);
    data = [ldata retain];
    if(spinner)
        [self showSpinner];
    [self updateUI];
}

- (id)initWithFrame:(CGRect)frame andData:(WeatherObject *)ldata
{
    return [self initWithFrame:frame andData:ldata andIsCurrentDay:FALSE];
}

- (id)initWithFrame:(CGRect)frame andData:(WeatherObject *)ldata andIsCurrentDay:(BOOL)lisCurrentDay
{
    self = [super initWithFrame:frame];
    if(self)
    {        
        mode = [UserDefaults boolForKey:WEATHER_MODE];
        
        self.isCurrentDay = lisCurrentDay;
        [self initUI];
        [self setData:ldata];
        [self createExtraInfoPanel];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showExtraTimes)];
        tapGesture.numberOfTapsRequired = 1;
       
        [self addGestureRecognizer:tapGesture];
        
        destroy(tapGesture);
        
        
        [[self evtReady] send];
        [[Shell.shell evtWentOffline] addHandler:AFFHandler(@selector(showSpinner))];
    }
    return self;
}

- (void)refreshData
{
    [Shell.shell.service queryWeather];
    [self showSpinner];
}


- (void)showSpinner
{
    if((([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable)) && spinner.isAnimating) {
        [spinner stopAnimating];
        return;
    } else if(([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)){
        [spinner startAnimating];
        return;
    }

}

- (void)initUI
{
    uint nameLabelSize = IS_IPAD() ? 48 : 31;
    uint dateLabelSize = IS_IPAD() ? 38 : 21;
    uint tempLabelSize = IS_IPAD() ? 90 : 60;
    uint descLabelSize = IS_IPAD() ? 32 : 17;
    uint minLabelSize = IS_IPAD() ? 42 : 23;
    uint maxLabelSize = IS_IPAD() ? 42 : 23;
    uint conditionSize = IS_IPAD() ? 125 : 50;
    
    self.backgroundColor = APP_COLOR_SAVED;
    
    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    nameLabel.font = [UIFont fontWithName:FONT_REGULAR size:nameLabelSize];
    nameLabel.textAlignment = UITextAlignmentCenter;
    nameLabel.numberOfLines = 0;
    nameLabel.lineBreakMode = UILineBreakModeWordWrap;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = RGB(254, 254, 254);
    
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10 + nameLabel.affY + nameLabel.affHeight, self.affWidth, 24)];
    dateLabel.font = [UIFont fontWithName:FONT_REGULAR size:dateLabelSize];
    dateLabel.textAlignment = UITextAlignmentCenter;
    dateLabel.numberOfLines = 0;
    dateLabel.lineBreakMode = UILineBreakModeWordWrap;
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.textColor = RGB(254, 254, 254);
    
    conditionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, dateLabel.affHeight + dateLabel.affY + 50, conditionSize, conditionSize)];
    CENTER_OBJECT_X(conditionImageView, self);
    conditionImageView.backgroundColor = [UIColor clearColor];
    
    tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, conditionImageView.affHeight + conditionImageView.affY + 50, self.affWidth, 50)];
    tempLabel.font = [UIFont fontWithName:FONT_REGULAR size:tempLabelSize];
    tempLabel.textAlignment = UITextAlignmentCenter;
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.textColor = RGB(254, 254, 254);
    
    descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10 + tempLabel.affY + tempLabel.affHeight + 30, self.affWidth, 50)];
    descLabel.font = [UIFont fontWithName:FONT_REGULAR size:descLabelSize];
    descLabel.textAlignment = UITextAlignmentCenter;
    descLabel.numberOfLines = 2;
    descLabel.lineBreakMode = UILineBreakModeWordWrap;
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.textColor = RGB(254, 254, 254);
    descLabel.clipsToBounds = FALSE;
    
    minMaxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.affHeight - 26, 33, 26)];
    minMaxImageView.image = [UIImage imageNamed:WEATHER_MIN_MAX_IMAGE];
    CENTER_OBJECT_X(minMaxImageView, self);
    minMaxImageView.backgroundColor = [UIColor clearColor];
    
    minLabel = [[UILabel alloc] initWithFrame:CGRectMake(minMaxImageView.affX - 102, self.affHeight - 35 - 20, 45, 23)];
    minLabel.font = [UIFont fontWithName:FONT_REGULAR size:minLabelSize];
    minLabel.textAlignment = UITextAlignmentRight;
    minLabel.numberOfLines = 0;
    minLabel.lineBreakMode = UILineBreakModeWordWrap;
    minLabel.backgroundColor = [UIColor clearColor];
    minLabel.textColor = RGB(254, 254, 254);
    
    maxLabel = [[UILabel alloc] initWithFrame:CGRectMake(minMaxImageView.affX + minMaxImageView.affWidth + 12, self.affHeight - 55, 45, 23)];
    maxLabel.font = [UIFont fontWithName:FONT_REGULAR size:maxLabelSize];
    maxLabel.textAlignment = UITextAlignmentLeft;
    maxLabel.numberOfLines = 0;
    maxLabel.lineBreakMode = UILineBreakModeWordWrap;
    maxLabel.backgroundColor = [UIColor clearColor];
    maxLabel.textColor = RGB(254, 254, 254);
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.affX = self.affWidth - spinner.affWidth - 5;
    spinner.affY = self.affHeight - spinner.affHeight - 50 - StatusBarHeight;
    [spinner startAnimating];
    UITapGestureRecognizer *refreshGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refreshData)];
    refreshGesture.numberOfTapsRequired = 1;
    
    [spinner addGestureRecognizer:refreshGesture];
    destroy(refreshGesture);
    
    
    if(Shell.shell.offline)
        [self showSpinner];
    
    [self addSubview:nameLabel];
    [self addSubview:dateLabel];
    [self addSubview:tempLabel];
    [self addSubview:descLabel];
    [self addSubview:conditionImageView];
    [self addSubview:minMaxImageView];
    [self addSubview:minLabel];
    [self addSubview:maxLabel];
    [self addSubview:spinner];
       
}

- (void)showExtraTimes
{
    self.userInteractionEnabled = FALSE;
    
    if(_panelOpen)
    {
        panel.alpha = 1.0f;
        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^(){
            panel.affY = isLandscape ? ScreenWidth : ScreenHeight;
        } completion:^(BOOL done){
            _panelOpen = FALSE;
            panel.alpha = 0.0f;
            self.userInteractionEnabled = TRUE;
            
            [[self evtPanelClosed] send];
        }];
        
    } else {
        panel.alpha = 1.0f;
        [self bringSubviewToFront:panel];
        _panelOpen = TRUE;
        
        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^(){
            panel.affY = (isLandscape ? ScreenWidth : ScreenHeight) - PANEL_HEIGHT;
        } completion:^(BOOL done){

            if(data.times.count == 0)
                [self performSelector:@selector(showExtraTimes) withObject:nil afterDelay:1.0f];
            else
                self.userInteractionEnabled = TRUE;
        }];
    }
}

- (void)createExtraInfoPanel
{
    panelObjects = [[NSMutableArray alloc] init];
    
    panel = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight, ScreenWidth, PANEL_HEIGHT)];
    panel.alpha = 0.0f;
    panel.backgroundColor = APP_COLOR_SECONDARY;

    
    if(data.times.count == 0)
    {
        noWeatherTimes = [[UILabel alloc] initWithFrame:CGRectZero];
        noWeatherTimes.text = NOWEATHER;
        noWeatherTimes.numberOfLines = 2;
        noWeatherTimes.backgroundColor = [UIColor clearColor];
        noWeatherTimes.font = [UIFont fontWithName:FONT_REGULAR size: IS_IPAD() ? 24 : 14];
        noWeatherTimes.textColor = RGB(254, 254, 254);
        noWeatherTimes.textAlignment = UITextAlignmentCenter;
        [panel addSubview:noWeatherTimes];
        
    } else {
                
        float rowHeight = isLandscape ? PANEL_HEIGHT / 6 : PANEL_HEIGHT / 8;
        
        for (int i = 0; i < [[data times] count]; i++) {
            
            WeatherPanelObject *panelObject = [[WeatherPanelObject alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, rowHeight) andData:[[data times] objectAtIndex:i]];
            
            [panel addSubview:panelObject];
            [panelObjects addObject:panelObject];
            destroy(panelObject);
        }
    }
    [self addSubview:panel];
}

- (void)updateUI
{
    //stupid fix
    NSString *desc = [[((WeatherObject *)data).weather objectForKey:@"description"] isEqualToString:@"sky is clear" ]? @"clear skies" : [((WeatherObject *)data).weather objectForKey:@"description"];
    //
    
    NSMutableString *mainDesc = [[NSMutableString alloc] initWithString:([[((WeatherObject *)data).weather objectForKey:@"precipitation"] floatValue] != 0.00) ? [NSString stringWithFormat:DECIMAL_PLACES,[ desc capitalizedString],[[((WeatherObject *)data).weather objectForKey:@"precipitation"] floatValue],PRECIP_SUFFIX] : [desc capitalizedString]];
    if([[((WeatherObject *)data).weather objectForKey:@"humidity"] intValue] > 0)
        [mainDesc insertString:[NSString stringWithFormat:@"\n%@ %d%%",[HUMIDITY capitalizedString],[[((WeatherObject *)data).weather objectForKey:@"humidity"] intValue]] atIndex:mainDesc.length];
    
    nameLabel.text = ((WeatherObject *)data).name;
    dateLabel.text = [((WeatherObject *)data).date description];
    tempLabel.text = [NSString stringWithFormat:@"%@%@",[((WeatherObject *)data).temp objectForKey:@"temp"],TEMP_MODE];
    descLabel.text = mainDesc;
    minLabel.text = [NSString stringWithFormat:@"%@%@",[((WeatherObject *)data).temp objectForKey:@"temp_min"],TEMP_MODE];
    maxLabel.text = [NSString stringWithFormat:@"%@%@",[((WeatherObject *)data).temp objectForKey:@"temp_max"],TEMP_MODE];
    conditionImageView.image = [UIImage imageNamed:((WeatherObject *)data).image];
    
    destroy(mainDesc);
    
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    
    //portrait mode
    if(!isLandscape)
    {
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        
        if(_panelOpen)
            panel.frame = CGRectMake(- VIEW_INDICATOR_LAST_SIZE, ScreenHeight - PANEL_HEIGHT, PANEL_WIDTH, MAX(ScreenWidth, ScreenHeight));
        else
            panel.frame = CGRectMake(- VIEW_INDICATOR_LAST_SIZE, ScreenHeight, PANEL_WIDTH, MAX(ScreenWidth, ScreenHeight));
        
        nameLabel.frame = CGRectMake(0, 10, self.affWidth, IS_IPAD() ? 80 : 35);
        dateLabel.frame = CGRectMake(0, 10 + nameLabel.affY + nameLabel.affHeight, self.affWidth, IS_IPAD() ? 80 : 24);
    
        if(IS_IPAD())
        {
            conditionImageView.frame = CGRectMake(0, dateLabel.affHeight + dateLabel.affY + 85 , conditionImageView.image.size.width * 2, conditionImageView.image.size.height * 2);
            tempLabel.frame = CGRectMake(0, 650, self.affWidth, 150);
        } else {
            if(IPHONE_5){
                conditionImageView.frame = CGRectMake(0, dateLabel.affHeight + dateLabel.affY + 50 , conditionImageView.image.size.width, conditionImageView.image.size.height);
                tempLabel.frame = CGRectMake(0, 330, self.affWidth, 50);
            } else {
                conditionImageView.frame = CGRectMake(0, dateLabel.affHeight + dateLabel.affY + 50, conditionImageView.image.size.width * 0.90f, conditionImageView.image.size.height * 0.90f);
                tempLabel.frame = CGRectMake(0, 288, self.affWidth, 50);
            }
        }

        CENTER_OBJECT_X(conditionImageView, self);
        descLabel.frame = CGRectMake(0, tempLabel.affY + tempLabel.affHeight + 15, self.affWidth, 50);
        minMaxImageView.frame = CGRectMake(0, IS_IPAD() ? self.affHeight - 100 : self.affHeight - 50 - 13, 33, 26);
        CENTER_OBJECT_X(minMaxImageView, self);
        minLabel.frame = CGRectMake(minMaxImageView.affX - (IS_IPAD() ? 115 : 65), self.affHeight - (IS_IPAD() ? 115 : 60), IS_IPAD() ? 100 : 50, IS_IPAD() ? 50 : 23);
        maxLabel.frame = CGRectMake(minMaxImageView.affX + minMaxImageView.affWidth + 12, minLabel.affY, IS_IPAD() ? 100 : 50, minLabel.affHeight);
        
    } else {
        //landscape
        self.frame = CGRectMake(0, 0, ScreenHeight, ScreenWidth);
        
        if(_panelOpen)
            panel.frame = CGRectMake(- VIEW_INDICATOR_LAST_SIZE, ScreenWidth - PANEL_HEIGHT + (IS_IPAD() ? 10 :0), MAX(ScreenWidth, ScreenHeight) + (VIEW_INDICATOR_LAST_SIZE * 2), MAX(ScreenWidth, ScreenHeight));
        else
            panel.frame = CGRectMake(- VIEW_INDICATOR_LAST_SIZE, ScreenWidth, MAX(ScreenWidth, ScreenHeight) +(VIEW_INDICATOR_LAST_SIZE * 2), MAX(ScreenWidth, ScreenHeight));
        
        nameLabel.frame = CGRectMake(0, IS_IPAD() ? 20 : 10, self.affWidth, 35);
        dateLabel.frame = CGRectMake(0, 10 + nameLabel.affY + nameLabel.affHeight, self.affWidth, IS_IPAD() ? 80 : 24);
        
        if(IS_IPAD())
        {
            conditionImageView.frame = CGRectMake(0, dateLabel.affHeight + dateLabel.affY + 50 , conditionImageView.image.size.width * 2, conditionImageView.image.size.height * 2);
            tempLabel.frame = CGRectMake(0, 330, self.affWidth, 150);
        } else {
            if(IPHONE_5){
                conditionImageView.frame = CGRectMake(65, 0 , conditionImageView.image.size.width, conditionImageView.image.size.height);
            } else {
                conditionImageView.frame = CGRectMake(65, 0, conditionImageView.image.size.width * 0.90f, conditionImageView.image.size.height * 0.90f);
            }
        }
        conditionImageView.affX = (self.affWidth / 2 - conditionImageView.affWidth) / 2 + 10;
        CENTER_OBJECT_Y(conditionImageView, self);
        minMaxImageView.frame = CGRectMake(self.affWidth - minMaxImageView.affWidth - (IS_IPAD() ? 140 : 100), IS_IPAD() ? self.affHeight - 110 : self.affHeight - 50 - 13, 33, 26);
        minLabel.frame = CGRectMake(minMaxImageView.affX - (IS_IPAD() ? 115 : 65), self.affHeight - (IS_IPAD() ? 125 :  60), IS_IPAD() ? 100 : 50, IS_IPAD() ? 50 : 23);
        maxLabel.frame = CGRectMake(minMaxImageView.affX + minMaxImageView.affWidth + 12, minLabel.affY, IS_IPAD() ? 100 : 50, IS_IPAD() ? 50 : 23);
        descLabel.frame = CGRectMake(10, minLabel.affY - (IS_IPAD() ? 5 : 15), self.affWidth / 2/*conditionImageView.affWidth + 80*/, 50);
        
        if(IS_IPAD())
        {
            tempLabel.frame = CGRectMake(minLabel.affX, 0, 200, 100);
        } else {
            tempLabel.frame = CGRectMake(minLabel.affX, 0, 150, 50);
        }
        CENTER_OBJECT_Y(tempLabel, self);
    }
    
    //Panel objects
    float rowHeight = isLandscape ? PANEL_HEIGHT / 5 : PANEL_HEIGHT / 8;
    float rowX = 0;
    float rowY = 0;
    float rowYOffset = 0;
    
    //Adjust current day if needed
    uint currentHour = 0;
    
    if(self.isCurrentDay)
    {        
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"H"];
        
        currentHour = [[formatter stringFromDate:[NSDate date]] intValue];
        currentHour /= HOURLY_TIME_SEPERATION;
        destroy(formatter);
    }
    
    for (int i = 0; i < [data.times count]; i++) {
                
        WeatherPanelObject *panelObject = [panelObjects objectAtIndex:i];
        panelObject.data = [data.times objectAtIndex:i];
        //Check for current matching time
        if(self.isCurrentDay)
        {
//            if(i == 0)
//                panelObject.isCurrentTime = TRUE; // :)
            
            NSMutableString *panelObjectTimeBaseString = [NSMutableString stringWithString:[panelObject.data time]];
            //Check if the panel time is AM or PM.
            BOOL timeIsPM = [panelObjectTimeBaseString rangeOfString:@"AM"].location == NSNotFound;
            
            //String hour from base panel time string
            NSUInteger colonIndex = [panelObjectTimeBaseString rangeOfString:@":"].location;
            NSString *timeString = [panelObjectTimeBaseString stringByReplacingCharactersInRange:NSMakeRange(colonIndex, panelObjectTimeBaseString.length - colonIndex) withString:@""];
            
            //Create usable panel hour
            uint panelHour = [timeString intValue];
            
            if(panelHour == 12 && !timeIsPM)
                panelHour -= 12;
            
            //If PM then add 12 hours
            uint additionalHours = timeIsPM && (panelHour != 12) ? 12 : 0;
            panelHour += additionalHours;
            
            if(panelHour > 0)
                panelHour /= HOURLY_TIME_SEPERATION;
                        
            if(currentHour == panelHour)
                panelObject.isCurrentTime = TRUE;
            else
                panelObject.isCurrentTime = FALSE;
        }
        
        rowX = VIEW_INDICATOR_LAST_SIZE;

        if(isLandscape)
        {            
            rowYOffset = IS_IPAD() ? 25 : 10;
            
            if( i > 3)
            {
                rowX = (IS_IPAD() ? 490 : 240) + VIEW_INDICATOR_LAST_SIZE;
                rowY = rowHeight * (i - 4) + rowYOffset;
            } else {
                rowY = rowHeight * i + rowYOffset;
            }
            
            panelObject.frame = CGRectMake(rowX, rowY, IS_IPAD() ? ScreenHeight / 2 : ScreenHeight / 2.5, rowHeight);
        } else {
            
            rowYOffset = IS_IPAD() ? 29 : 12;
            rowY = rowHeight * i + rowYOffset;
        
            panelObject.frame = CGRectMake(rowX, rowY, PANEL_WIDTH, rowHeight);
        }
    }
    
    if(noWeatherTimes)
    {
        [noWeatherTimes sizeToFit];
        noWeatherTimes.frame = CGRectMake(20, (PANEL_HEIGHT - noWeatherTimes.affHeight) / 2, self.affWidth - 30, 60);
        noWeatherTimes.affY = (PANEL_HEIGHT - noWeatherTimes.affHeight ) / 2;
    }
    
    uint spinnerSize = IS_IPAD() ? 50 : 20;
    spinner.frame = CGRectMake(5, self.affHeight - spinner.affHeight - 5 - spinnerSize, spinnerSize, spinnerSize);
    
}

- (void)showIn
{    
    self.backgroundColor = APP_COLOR_SECONDARY;
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        self.backgroundColor = APP_COLOR_SAVED;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

- (void)showOut
{
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME - 0.5 animations:^{
        self.backgroundColor = APP_COLOR_SECONDARY;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

- (void)dealloc
{
    destroy(data);
    destroy(panelObjects);
    destroyAndRemove(noWeatherTimes);
    destroyAndRemove(nameLabel);
    destroyAndRemove(dateLabel);
    destroyAndRemove(tempLabel);
    destroyAndRemove(descLabel);
    destroyAndRemove(conditionImageView);
    destroyAndRemove(panel);
    destroyAndRemove(minLabel);
    destroyAndRemove(maxLabel);
    destroyAndRemove(minMaxImageView);
    if(spinner)
        destroyAndRemove(spinner);
 
    AFFRemoveAllEvents();
    
    [super dealloc];
}

@end