//
//  SwipeUpScrollView.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "SwipeUpScrollView.h"
#import "SwipeUpScrollViewObject.h"

@implementation SwipeUpScrollView

AFFEventSynthesize(AFFEventInstance, evtShowOut);

- (id)initWithFrame:(CGRect)frame andDataObjects:(NSArray *)ldata
{
    self = [super initWithFrame:frame];
    if(self)
    {
        selectedDate = 0;
        
        if(isLandscape)
            self.affY = VIEW_LANDSCAPE_POSITION_BOTTOM.y;
        else
            self.affY = VIEW_PORTRAIT_POSITION_BOTTOM.y;
        
        self.backgroundColor = APP_COLOR_SAVED;
        
        data = [[NSMutableArray alloc] initWithArray:ldata];
        
        [self createGesture];
        [self createScrollView];
        [self layoutSubviews];
    }
    return self;
}

- (void)createGesture
{
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipeUp)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    swipeGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:swipeGesture];
    
    [swipeGesture release];
    swipeGesture = nil;
}

/*
 * Scroll view and positioning
 */
- (void)createScrollView
{
    float scrollViewHeight = 0;
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.affWidth, self.affHeight)];
    scrollView.showsHorizontalScrollIndicator = FALSE;
    scrollView.showsVerticalScrollIndicator = FALSE;
    
    for(uint i = 0; i < data.count; i ++)
    {
        SwipeUpScrollViewObject *object = [data objectAtIndex:i];
        
        if(object.isSelected)
            selectedDate = i;
        
        [[object evtSelected] addHandlerOneTime:AFFHandler(@selector(onItemSelected:))];
        object.affY = scrollViewHeight;
        
        scrollViewHeight += object.affHeight;
        [scrollView addSubview:object];
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.affWidth, scrollViewHeight);
    [self addSubview:scrollView];
    
    [[self evtReady] send];
}

/*
 * Item selection
 */
- (void)onItemSelected:(AFFEvent *)event
{
    [[self evtShowOut] send:event.data];
    [self showOut];
}

/*
 * Swipe Up
 */
- (void)onSwipeUp
{
    [[self evtShowOut] send:[NSNumber numberWithUnsignedInt:selectedDate]];
    [self showOut];
}

- (void)showIn
{
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        self.backgroundColor = APP_COLOR_SECONDARY;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

- (void)showOut
{
    self.userInteractionEnabled = FALSE;
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        self.backgroundColor = APP_COLOR_SAVED;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = TRUE;
    }];
}

/*
 * Layout subviews
 */
- (void)layoutSubviews
{
    //Scroll view
    if(isLandscape)
        self.frame = CGRectMake(self.affX, self.affY, ScreenHeight, ScreenWidth);
    else
        self.frame = CGRectMake(self.affX, self.affY, ScreenWidth, ScreenHeight);
        
    //Scroll view items
    float scrollViewHeight = 0;
    
    scrollView.frame = CGRectMake(0, 0, self.affWidth, self.affHeight);
    
    
    float thumbnailWidth = self.affWidth;
    float thumbnailHeight = IS_IPAD() ? (isLandscape ? ((self.affHeight - StatusBarHeight) / 2.40) : ((self.affHeight - StatusBarHeight) / 7))
    : (isLandscape ? ((self.affHeight - StatusBarHeight) / 3.75) : ((self.affHeight - StatusBarHeight) / 7));
    
    for(uint i = 0; i < data.count; i ++)
    {
        SwipeUpScrollViewObject *object = [data objectAtIndex:i];
        object.frame = CGRectMake(0, 0, thumbnailWidth, thumbnailHeight);
        object.affY = scrollViewHeight;
        
        scrollViewHeight += object.affHeight;
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.affWidth, scrollViewHeight + 20);
}

- (void)dealloc
{
    destroy(data);
    destroy(scrollView);
    
    AFFRemoveAllEvents();
    [super dealloc];
}

@end
