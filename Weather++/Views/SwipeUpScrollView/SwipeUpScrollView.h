//
//  SwipeUpScrollView.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-23.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFView.h"

@interface SwipeUpScrollView : AFFView
{
    @private
    NSMutableArray *data;
    uint selectedDate;
    UIScrollView *scrollView;
}

- (id)initWithFrame:(CGRect)frame andDataObjects:(NSArray *)ldata;

//Animations
- (void)showIn;
- (void)showOut;

AFFEventCreate(AFFEventInstance, evtShowOut);

@end
