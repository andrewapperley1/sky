//
//  Shell.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFView.h"
#import <CoreLocation/CoreLocation.h>

@class AlarmViewController;
@class Database;
@class NoteViewController;
@class WeatherService;
@class WeatherViewController;

@interface Shell : AFFView<CLLocationManagerDelegate>
{
    Database *database;

    CLLocationManager *locationManager;
    
    UIButton *blocker;
    UIActivityIndicatorView *spinner;

}

@property (nonatomic, readonly) WeatherViewController *weatherViewController;
@property (nonatomic, readonly) WeatherService *service;
@property (nonatomic, assign) BOOL _isLandscape;
@property (nonatomic, assign) BOOL _error;
@property (nonatomic, assign) BOOL offline;
@property (nonatomic, retain) UILabel *errorLabel;
@property (nonatomic, assign) BOOL _firstBoot;
- (id)initWithFrame:(CGRect)frame andViewController:(AFFViewController *)viewController;

+ (Shell *)shell;

//Views
- (void)openWeatherViewFrom:(CGPoint)fromPoint withClose:(CGPoint)closePoint;

- (void)queryingApi;

+ (BOOL)firstBoot;

- (void)finishBoot;

AFFEventCreate(AFFEventInstance, evtLocationChanged);
AFFEventCreate(AFFEventInstance, evtRotationChanged);
AFFEventCreate(AFFEventInstance, evtWentOffline);
AFFEventCreate(AFFEventInstance, evtDismissMessage);

@end