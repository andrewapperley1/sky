//
//  WeatherViewController.m
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-14.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "Database.h"
#import "SettingsView.h"
#import "SwipeUpScrollView.h"
#import "WeatherDatabase.h"
#import "WeatherView.h"
#import "WeatherViewController.h"
#import "WeatherService.h"
#import "WeatherThumbnailObject.h"

@implementation WeatherViewController
@synthesize isAnimating = _isAnimating;
@synthesize isScrollViewOpened = _isScrollViewOpened;
@synthesize isSettingsViewOpened = _isSettingsViewOpened;
@synthesize isPanelOpened;

#define isCurrentDay (currentDayDifference == 0)

AFFEventSynthesize(AFFEventInstance, evtPanelClosed);

static int currentDayDifference = 0;

- (id)init
{
    self = [super initWithDirections:SwipeAll];
    if(self)
    {        
        _isAnimating = FALSE;
        _isScrollViewOpened = FALSE;
        
        [self updateWeather];
        [[[[Shell shell] service] evtWeatherUpdated] addHandler:AFFHandler(@selector(updateWeather))];
                
        [[self evtSwipedLeft] addHandler:AFFHandler(@selector(goToNextDay))];
        [[self evtSwipedRight] addHandler:AFFHandler(@selector(goToPreviousDay))];
        [[self evtSwipedUp] addHandler:AFFHandler(@selector(openScrollView))];
        [[self evtSwipedUp] addHandler:AFFHandler(@selector(closeSettingsView))];
        [[self evtSwipedDown] addHandler:AFFHandler(@selector(openSettingsView))];
    }
    return self;
}

/*
 * Weater updates
 */
- (void)updateWeather
{
    if(data)
        destroy(data);
    data = [[weatherDatabase getWeather] retain];
    if(data.count < 1 || !data)
        data = [[weatherDatabase getWeather] retain];
    
    ((WeatherView *)self.currentView).data = (WeatherObject *)[data objectAtIndex:currentDayDifference];
}

/*
 * Data changing
 */
- (void)goToNextDay
{
    if(currentDayDifference + 1 < [data count])
    {
        if(!_isScrollViewOpened && !_isSettingsViewOpened)
        {
            currentDayDifference += 1;
            
            CGPoint openPos = isLandscape ? VIEW_LANDSCAPE_POSITION_RIGHT : VIEW_PORTRAIT_POSITION_RIGHT;
            CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_LEFT : VIEW_PORTRAIT_POSITION_LEFT;
            
            [self openView:openPos andClosePosition:closePos withDayDifference:currentDayDifference];
        }
    } else {
        [self indicateLastViewFromPoint:CGPointMake( -VIEW_INDICATOR_LAST_SIZE, 0)];
    }
}

- (void)goToPreviousDay
{
    if(currentDayDifference > 0)
    {
        if(!_isScrollViewOpened && !_isSettingsViewOpened)
        {
            currentDayDifference -= 1;
            
            CGPoint openPos = isLandscape ? VIEW_LANDSCAPE_POSITION_LEFT : VIEW_PORTRAIT_POSITION_LEFT;
            CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_RIGHT : VIEW_PORTRAIT_POSITION_RIGHT;
            
            [self openView:openPos andClosePosition:closePos withDayDifference:currentDayDifference];
        }
    } else {
        [self indicateLastViewFromPoint:CGPointMake( VIEW_INDICATOR_LAST_SIZE, 0)];
    }
}

- (void)openView:(CGPoint)startPosition andClosePosition:(CGPoint)closePosition withDayDifference:(int)dayDifference
{
    if(self.isPanelOpened)
    {
        [[(WeatherView *)self.currentView evtPanelClosed] addHandlerOneTime:AFFHandlerWithArgs(@selector(onPanelClosed:andStartPosition:andClosePosition:withDayDifference:), [NSValue valueWithCGPoint:startPosition], [NSValue valueWithCGPoint:closePosition], [NSNumber numberWithInt:dayDifference])];
        [(WeatherView *)self.currentView showExtraTimes];
    } else {
        float width = IS_IPAD() ? (isLandscape ? ScreenHeight : ScreenWidth) : (isLandscape ? ScreenHeight : ScreenWidth);
        float height = IS_IPAD() ? (isLandscape ? ScreenWidth : ScreenHeight) : (isLandscape ? ScreenWidth : ScreenHeight);
        
        WeatherView *view = [[WeatherView alloc] initWithFrame:CGRectMake(0, 0, width, height) andData:(WeatherObject *)[data objectAtIndex:dayDifference] andIsCurrentDay:isCurrentDay];
        
        [self open:view from:startPosition to:VIEW_POSITION_CENTER andCloseCurrentViewTo:closePosition];
        destroy(view);
    }
}

- (void)onPanelClosed:(AFFEvent *)event andStartPosition:(NSValue *)startPosition andClosePosition:(NSValue *)closePosition withDayDifference:(NSNumber *)dayDifference
{
    float width = isLandscape ? ScreenHeight : ScreenWidth;
    float height = isLandscape ? ScreenWidth : ScreenHeight;
    
    WeatherView *view = [[WeatherView alloc] initWithFrame:CGRectMake(0, 0, width, height) andData:(WeatherObject *)[data objectAtIndex:[dayDifference intValue]] andIsCurrentDay:isCurrentDay];
    
    [self open:view from:[startPosition CGPointValue] to:VIEW_POSITION_CENTER andCloseCurrentViewTo:[closePosition CGPointValue]];
    destroy(view);
}

/*
 * Fade in views
 */
- (void)openView:(CGPoint)startPosition andClosePosition:(CGPoint)closePosition withDayDifference:(int)dayDifference andFadeIn:(BOOL)fadeIn
{
    if(!fadeIn)
    {
        [self openView:startPosition andClosePosition:closePosition withDayDifference:dayDifference];
        return;
    }
    
    if(self.isPanelOpened)
    {
        [[(WeatherView *)self.currentView evtPanelClosed] addHandlerOneTime:AFFHandlerWithArgs(@selector(onPanelClosedWithFade:andStartPosition:andClosePosition:withDayDifference:), [NSValue valueWithCGPoint:startPosition], [NSValue valueWithCGPoint:closePosition], [NSNumber numberWithInt:dayDifference])];
        [(WeatherView *)self.currentView showExtraTimes];
    } else {
        float width = isLandscape ? ScreenHeight : ScreenWidth;
        float height = isLandscape ? ScreenWidth : ScreenHeight;
        
        WeatherView *view = [[WeatherView alloc] initWithFrame:CGRectMake(0, 0, width, height) andData:(WeatherObject *)[data objectAtIndex:dayDifference] andIsCurrentDay:isCurrentDay];
        view.alpha = 0;
        [[self evtOpened] addHandlerOneTime:AFFHandlerWithArgs(@selector(onFadeInViewOpened:withView:), view)];
        [self open:view from:startPosition to:VIEW_POSITION_CENTER andCloseCurrentViewTo:closePosition];
        destroy(view);
    }
}

- (void)onPanelClosedWithFade:(AFFEvent *)event andStartPosition:(NSValue *)startPosition andClosePosition:(NSValue *)closePosition withDayDifference:(NSNumber *)dayDifference
{
    float width = isLandscape ? ScreenHeight : ScreenWidth;
    float height = isLandscape ? ScreenWidth : ScreenHeight;
    
    WeatherView *view = [[WeatherView alloc] initWithFrame:CGRectMake(0, 0, width, height) andData:(WeatherObject *)[data objectAtIndex:[dayDifference intValue]] andIsCurrentDay:isCurrentDay];
    
    [[self evtOpened] addHandlerOneTime:AFFHandlerWithArgs(@selector(onFadeInViewOpened:withView:), view)];
    [self open:view from:[startPosition CGPointValue] to:VIEW_POSITION_CENTER andCloseCurrentViewTo:[closePosition CGPointValue]];
    destroy(view);
}

- (void)onFadeInViewOpened:(AFFEvent *)event withView:(UIView *)view
{
    [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
        view.alpha = 1;
    }];
}

/*
 * Check for weather view panel
 */
- (BOOL)isPanelOpened
{
    return [self.currentView isKindOfClass:[WeatherView class]] && [(WeatherView *)self.currentView panelOpen];
}

- (void)closeWeatherPanel
{
    [[(WeatherView *)self.currentView evtPanelClosed] addHandlerOneTime:AFFHandler(@selector(onWeatherPanelClosed))];
    [(WeatherView *)self.currentView showExtraTimes];
}

- (void)onWeatherPanelClosed
{
    [[self evtPanelClosed] send];
}

/*
 * Scoll view
 */
- (void)openScrollView
{
    if(self.isPanelOpened)
    {
        [[self evtPanelClosed] addHandlerOneTime:AFFHandler(@selector(openScrollView))];
        [self closeWeatherPanel];
        return;
    }
    
    if(!_isScrollViewOpened && !_isSettingsViewOpened && !_isAnimating)
    {
        _isScrollViewOpened = TRUE;
        
        CGRect frame = CGRectZero;
        if(isLandscape)
            frame = CGRectMake(0, 0, ScreenHeight, ScreenWidth);
        else
            frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        
        float thumbnailWidth = frame.size.width;
        float thumbnailHeight = isLandscape ? (frame.size.height / 3.75) : (frame.size.height / 7);
        
        //Create usable objects from data
        NSMutableArray *dataObjects = [[NSMutableArray alloc] initWithCapacity:data.count];
        for(uint i = 0; i < data.count; i++)
        {
            WeatherThumbnailObject *thumbnail = [[WeatherThumbnailObject alloc] initWithFrame:CGRectMake(0, 0, thumbnailWidth, thumbnailHeight) withIndex:i andData:(WeatherObject*)[data objectAtIndex:i]];
            
            if(i == currentDayDifference)
                thumbnail.isSelected = TRUE;
            
            [dataObjects addObject:thumbnail];
            destroy(thumbnail);
        }
        
        scrollView = [[SwipeUpScrollView alloc] initWithFrame:frame andDataObjects:dataObjects];
        [self.view addSubview:scrollView];

        [[scrollView evtShowOut] addHandlerOneTime:AFFHandler(@selector(scrollViewClosing:))];
        
        if(scrollView.isReady)
            [self showScrollViewIn];
        else 
            [[scrollView evtReady] addHandlerOneTime:AFFHandler(@selector(showScrollViewIn))];

        destroy(dataObjects);
    }
}

- (void)showScrollViewIn
{
    //Animate views in
    if(!_isAnimating)
    {
        _isAnimating = TRUE;
        self.view.userInteractionEnabled = FALSE;
        
        [scrollView showIn];
        [(WeatherView *)self.currentView showOut];
        
        CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_TOP : VIEW_PORTRAIT_POSITION_TOP;
        
        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
            [self closeTo:closePos];
            scrollView.affY = VIEW_POSITION_CENTER.y;
        } completion:^(BOOL finished) {
            _isAnimating = FALSE;
            self.view.userInteractionEnabled = TRUE;
        }];
    }
}

- (void)showScrollViewOut
{
    //Animate views out
    if(!_isAnimating)
    {
        _isAnimating = TRUE;
        self.view.userInteractionEnabled = FALSE;
        
        CGPoint openPos = isLandscape ? VIEW_LANDSCAPE_POSITION_TOP : VIEW_PORTRAIT_POSITION_TOP;
        CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_BOTTOM : VIEW_PORTRAIT_POSITION_BOTTOM;
        
        [self openView:openPos andClosePosition:closePos withDayDifference:currentDayDifference];
        [(WeatherView *)self.currentView showIn];

        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
            scrollView.affY = closePos.y;
        } completion:^(BOOL finished) {
            _isAnimating = FALSE;
            _isScrollViewOpened = FALSE;
            self.view.userInteractionEnabled = TRUE;
            destroyAndRemove(scrollView);
        }];
    }
}

- (void)scrollViewClosing:(AFFEvent *)event
{
    int selectedIndex = event.data ? [(NSNumber *)event.data intValue] : currentDayDifference;
    
    currentDayDifference = selectedIndex;

    [self showScrollViewOut];
}

/*
 * Settings view
 */
- (void)openSettingsView
{
    if(self.isPanelOpened)
    {
        [[self evtPanelClosed] addHandlerOneTime:AFFHandler(@selector(openSettingsView))];
        [self closeWeatherPanel];
        return;
    }
    
    if(!_isScrollViewOpened && !_isSettingsViewOpened && !_isAnimating)
    {        
        _isSettingsViewOpened = TRUE;
        
        CGRect frame = CGRectZero;
        if(isLandscape)
            frame = CGRectMake(0, - ScreenWidth, ScreenHeight, ScreenWidth);
        else
            frame = CGRectMake(0, - ScreenHeight, ScreenWidth, ScreenHeight);
                
        settingsView = [[SettingsView alloc] initWithFrame:frame];
        [[settingsView evtClose] addHandlerOneTime:AFFHandler(@selector(closeSettingsView))];
        [self.view addSubview:settingsView];
        
        if(settingsView.isReady)
            [self showSettingsViewIn];
        else
            [[settingsView evtReady] addHandlerOneTime:AFFHandler(@selector(showSettingsViewIn))];
    }
}

- (void)showSettingsViewIn
{
    //Animate views in
    if(!_isAnimating)
    {
        _isAnimating = TRUE;
        self.view.userInteractionEnabled = FALSE;
        
        [settingsView showIn];
        [(WeatherView *)self.currentView showOut];
        
        CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_BOTTOM : VIEW_PORTRAIT_POSITION_BOTTOM;
        
        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
            [self closeTo:closePos];
            settingsView.affY = VIEW_POSITION_CENTER.y;
        } completion:^(BOOL finished) {
            _isAnimating = FALSE;
            self.view.userInteractionEnabled = TRUE;
        }];
    }
}

- (void)closeSettingsView
{
    //Animate views out
    if(_isSettingsViewOpened && !_isAnimating)
    {
        _isAnimating = TRUE;
        self.view.userInteractionEnabled = FALSE;
        
        CGPoint openPos = isLandscape ? VIEW_LANDSCAPE_POSITION_BOTTOM : VIEW_PORTRAIT_POSITION_BOTTOM;
        CGPoint closePos = isLandscape ? VIEW_LANDSCAPE_POSITION_TOP : VIEW_PORTRAIT_POSITION_TOP;
        
        [self openView:openPos andClosePosition:closePos withDayDifference:currentDayDifference];
        [(WeatherView *)self.currentView showIn];
        
        [UIView animateWithDuration:VIEW_ANIMATION_TIME animations:^{
            settingsView.affY = closePos.y;
        } completion:^(BOOL finished) {
            _isAnimating = FALSE;
            _isSettingsViewOpened = FALSE;
            self.view.userInteractionEnabled = TRUE;
            self.view.backgroundColor = APP_COLOR_SAVED;
            if(settingsView.reQuery)
                [[Shell.shell evtLocationChanged] send];
            if(settingsView)
            destroyAndRemove(settingsView);
        }];
    }
}

/*
 * Layout subviews
 */
- (void)layoutSubviews
{
    //Landscape
    if(isLandscape)
        self.view.frame = CGRectMake(0, 0,  ScreenHeight, ScreenWidth);
    else 
        self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    
    if(scrollView)
        [scrollView setNeedsLayout];
    
    if(settingsView)
        [settingsView setNeedsLayout];
    
    if(self.currentView)
        [self.currentView setNeedsLayout];
}

/*
 * Last view animation
 */
- (void)indicateLastViewFromPoint:(CGPoint)animateToPoint
{
    float startX = self.currentView.affX;
    float startY = self.currentView.affY;
    
    self.view.userInteractionEnabled = FALSE;
    
    [UIView animateWithDuration:VIEW_ANIMATION_TIME / 2 animations:^{
        self.currentView.affX = animateToPoint.x;
        self.currentView.affY = animateToPoint.y;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:VIEW_ANIMATION_TIME / 2 animations:^{
            self.currentView.affX = startX;
            self.currentView.affY = startY;            
        } completion:^(BOOL finished) {
            self.view.userInteractionEnabled = TRUE;
        }];
    }];
}

- (void)dealloc
{
    destroy(data);
    destroyAndRemove(scrollView);
    destroyAndRemove(settingsView);
    
    AFFRemoveAllEvents();
    
    [super dealloc];
}

@end
