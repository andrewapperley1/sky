//
//  WeatherViewController.h
//  Weather++
//
//  Created by Jeremy Fuellert on 2013-04-14.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFSwipeViewController.h"

@class SettingsView;
@class SwipeUpScrollView;

@interface WeatherViewController : AFFSwipeViewController
{
    @private
    NSArray *data;
    SwipeUpScrollView *scrollView;
    SettingsView *settingsView;
}

@property (nonatomic, readonly) BOOL isAnimating;
@property (nonatomic, readonly) BOOL isScrollViewOpened;
@property (nonatomic, readonly) BOOL isPanelOpened;
@property (nonatomic, readonly) BOOL isSettingsViewOpened;

- (void)openView:(CGPoint)startPosition andClosePosition:(CGPoint)closePosition withDayDifference:(int)dayDifference;
- (void)openView:(CGPoint)startPosition andClosePosition:(CGPoint)closePosition withDayDifference:(int)dayDifference andFadeIn:(BOOL)fadeIn;

- (void)closeWeatherPanel;

- (void)openSettingsView;
- (void)closeSettingsView;

- (void)layoutSubviews;

AFFEventCreate(AFFEventInstance, evtPanelClosed);

@end
