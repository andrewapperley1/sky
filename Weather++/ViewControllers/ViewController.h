//
//  ViewController.h
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "AFFGesturedViewController.h"

enum TutorialGesture
{
    kTapGesture,
    kSwipeUp,
    kSwipeDown,
    kSwipeLeft,
    kSwipeRight
};

@interface ViewController : AFFGesturedViewController
{
    @private
    uint tutorialIndex;
    NSArray *tutorialStrings;
    NSArray *tutorialImages;
}

@end
