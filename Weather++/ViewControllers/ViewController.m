//
//  ViewController.m
//  Weather++
//
//  Created by Andrew Apperley on 2013-04-11.
//  Copyright (c) 2013 AF Apps. All rights reserved.
//

#import "Constants.h"
#import "ViewController.h"
#import "GettingStartedView.h"

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithDirections:SwipeAll];
    if(self)
    {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(deviceOrientationDidChange) name: UIDeviceOrientationDidChangeNotification object:nil];
        
        if([Shell firstBoot])
        {
            tutorialIndex = 0;
            tutorialStrings = [[NSArray alloc] initWithObjects:Started_1,Started_2,Started_3,Started_4, nil];
            tutorialImages = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:ArrowLeftRight], [NSNumber numberWithInt:ArrowUp], [NSNumber numberWithInt:ArrowDown], [NSNumber numberWithInt:Circle], nil];
        
            [self createGestureHandlers];

            CGPoint top = isLandscape ? VIEW_LANDSCAPE_POSITION_TOP : VIEW_PORTRAIT_POSITION_TOP;
            CGPoint bottom = isLandscape ? VIEW_LANDSCAPE_POSITION_BOTTOM : VIEW_PORTRAIT_POSITION_BOTTOM;

            [self showTutorialIndex:0 andOpenPoint:top andClosePoint:bottom];
        } else {
            [self startLoadSequence];
        }
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return TRUE;
}

- (void)deviceOrientationDidChange
{
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
    
    if(self.currentView)
        [self.currentView setNeedsLayout];
    
    if(currentOrientation == UIDeviceOrientationUnknown || currentOrientation == UIDeviceOrientationFaceUp || currentOrientation == UIDeviceOrientationFaceDown)
    {
        if([self.view isKindOfClass:[Shell class]])
            [(Shell *)self.view set_isLandscape:[(Shell *)self.view _isLandscape]];
        return;
    }
    
    if ( isLandscape && [self.view isKindOfClass:[Shell class]] )
        [(Shell *)self.view set_isLandscape:TRUE];
    else if ([self.view isKindOfClass:[Shell class]])
       [(Shell *)self.view set_isLandscape:FALSE];
}

- (void)startLoadSequence
{
    float width = isLandscape ? ScreenHeight : ScreenWidth;
    float height = isLandscape ? ScreenWidth : ScreenHeight;
    
    Shell *shell = [[Shell alloc] initWithFrame:CGRectMake(0, 0, width, height) andViewController:self];
    [self setView:shell];
    destroy(shell);
    
    [self deviceOrientationDidChange];
}

/*
 * Gesture Handling
 */
- (void)createGestureHandlers
{
    [[self evtTapped] addHandler:AFFHandlerWithArgs(@selector(onGestureHandler:andGesture:), [NSNumber numberWithInt:kTapGesture])];
    [[self evtSwipedUp] addHandler:AFFHandlerWithArgs(@selector(onGestureHandler:andGesture:), [NSNumber numberWithInt:kSwipeUp])];
    [[self evtSwipedDown] addHandler:AFFHandlerWithArgs(@selector(onGestureHandler:andGesture:), [NSNumber numberWithInt:kSwipeDown])];
    [[self evtSwipedLeft] addHandler:AFFHandlerWithArgs(@selector(onGestureHandler:andGesture:), [NSNumber numberWithInt:kSwipeLeft])];
    [[self evtSwipedRight] addHandler:AFFHandlerWithArgs(@selector(onGestureHandler:andGesture:), [NSNumber numberWithInt:kSwipeRight])];
}

- (void)onGestureHandler:(AFFEvent *)event andGesture:(NSNumber *)gesture
{
    CGPoint top = isLandscape ? VIEW_LANDSCAPE_POSITION_TOP : VIEW_PORTRAIT_POSITION_TOP;
    CGPoint bottom = isLandscape ? VIEW_LANDSCAPE_POSITION_BOTTOM : VIEW_PORTRAIT_POSITION_BOTTOM;
    CGPoint left = isLandscape ? VIEW_LANDSCAPE_POSITION_LEFT : VIEW_PORTRAIT_POSITION_LEFT;
    CGPoint right = isLandscape ? VIEW_LANDSCAPE_POSITION_RIGHT : VIEW_PORTRAIT_POSITION_RIGHT;
    
    switch ([gesture intValue])
    {
        case kTapGesture:
            if(tutorialIndex == 3)
                [self onTutorialOpenFrom:right andCloseTo:left];
            break;
        case kSwipeUp:
            if(tutorialIndex == 1)
                [self onTutorialOpenFrom:bottom andCloseTo:top];
            break;
        case kSwipeDown:
            if(tutorialIndex == 2)
                [self onTutorialOpenFrom:top andCloseTo:bottom];
            break;
        case kSwipeLeft:
            if(tutorialIndex == 0)
                [self onTutorialOpenFrom:right andCloseTo:left];
            break;
        case kSwipeRight:
            if(tutorialIndex == 0)
                [self onTutorialOpenFrom:left andCloseTo:right];
            break;
            
        default:
            break;
    }
}

/*
 * Tutorial Handling
 */
- (void)showTutorialIndex:(uint)index andOpenPoint:(CGPoint)openPoint andClosePoint:(CGPoint)closePoint
{
    float width = isLandscape ? ScreenHeight : ScreenWidth;
    float height = isLandscape ? ScreenWidth : ScreenHeight;
    
    GettingStartedView *view = [[GettingStartedView alloc] initWithFrame:CGRectMake(0, 0, width, height) andText:[tutorialStrings objectAtIndex:index] andImage:[[tutorialImages objectAtIndex:index] intValue]];
    [self open:view from:openPoint to:VIEW_POSITION_CENTER andCloseCurrentViewTo:closePoint];
    destroy(view);
}

- (void)onTutorialOpenFrom:(CGPoint)openPoint andCloseTo:(CGPoint)closePoint
{
    tutorialIndex ++;
    
    if(tutorialIndex == tutorialStrings.count)
        [self showOutTutorial];
    else
        [self showTutorialIndex:tutorialIndex andOpenPoint:openPoint andClosePoint:closePoint];
}

- (void)showOutTutorial
{
    [UIView animateWithDuration:TUTORIAL_ANIMATION_OUT_TIME animations:^{
        self.currentView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self startLoadSequence];
        [self destroyTutorialObjects];
    }];
}

- (void)destroyTutorialObjects
{
    destroy(tutorialStrings);
    destroy(tutorialImages);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self deviceOrientationDidChange];
}

/*
 * IOS 6+
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

/*
 * IOS 5 ONLY
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return TRUE;
}

- (BOOL)shouldAutorotate
{
    return TRUE;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

@end
